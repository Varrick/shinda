# --------- SET PLATFORM -----------
set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR arm)

# --------- SET PATHS -----------
set(base_path $ENV{HOME})
# Toolchain Paths
set(toolchain_path ${base_path}/arm_dev_env_64/toolchain_arm/gcc-linaro-7.4.1-2019.02-x86_64_aarch64-linux-gnu)
set(sysroot_target ${toolchain_path}/aarch64-linux-gnu/libc)
set(tools ${toolchain_path}/bin)

# --------- CONFIG COMPILER -----------
set(CMAKE_C_COMPILER ${tools}/aarch64-linux-gnu-gcc)
set(CMAKE_CXX_COMPILER ${tools}/aarch64-linux-gnu-g++)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_SYSROOT ${sysroot_target})
SET(CMAKE_CXX_FLAGS " -march=armv8-a --sysroot=${sysroot_target} -lpthread -lm -lrt -fPIC -fexceptions")
SET(CMAKE_C_FLAGS ${CMAKE_CXX_FLAGS})
SET(CMAKE_EXE_LINKER_FLAGS "--sysroot=${sysroot_target}")
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)