# - Find libmosquitto
# Find the native libshinda includes and libraries
#
#  SHINDA_INCLUDE_DIRS - where to find mosquitto.h, etc.
#  SHINDA_LIBRARIES    - List of libraries when using libmosquitto.
#  SHINDA_FOUND        - True if libmosquitto found.

if (NOT SHINDA_INCLUDE_DIR)
  find_path(SHINDA_INCLUDE_DIR shinda/pubsub/Client.hpp)
endif()

if (NOT SHINDA_LIBRARY)
  find_library(
    SHINDA_LIBRARY
    NAMES libShinda libshinda shinda Shinda
    PATH_SUFFIXES shinda)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
  SHINDA DEFAULT_MSG
  SHINDA_LIBRARY SHINDA_INCLUDE_DIR)

set(SHINDA_LIBRARIES ${SHINDA_LIBRARY})
set(SHINDA_INCLUDE_DIRS ${SHINDA_INCLUDE_DIR})

message(STATUS "libshinda include dir: ${SHINDA_INCLUDE_DIRS}")
message(STATUS "libshinda: ${SHINDA_LIBRARIES}")
