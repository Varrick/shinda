# - Find libmosquitto
# Find the native libmosquitto includes and libraries
#
#  MOSQUITTO_INCLUDE_DIRS - where to find mosquitto.h, etc.
#  MOSQUITTO_LIBRARIES    - List of libraries when using libmosquitto.
#  MOSQUITTO_FOUND        - True if libmosquitto found.

if (NOT MOSQUITTO_INCLUDE_DIR)
  find_path(MOSQUITTO_INCLUDE_DIR mosquitto.h)
endif()

if (NOT MOSQUITTO_LIBRARY)
  find_library(
    MOSQUITTO_LIBRARY
    NAMES mosquitto libmosquitto
    PATH_SUFFIXES)
endif()

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(
  MOSQUITTO DEFAULT_MSG
  MOSQUITTO_LIBRARY MOSQUITTO_INCLUDE_DIR)

set(MOSQUITTO_LIBRARIES ${MOSQUITTO_LIBRARY})
set(MOSQUITTO_INCLUDE_DIRS ${MOSQUITTO_INCLUDE_DIR})

message(STATUS "libmosquitto include dir: ${MOSQUITTO_INCLUDE_DIRS}")
message(STATUS "libmosquitto: ${MOSQUITTO_LIBRARIES}")