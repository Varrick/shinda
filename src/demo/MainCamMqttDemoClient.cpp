// StdLib Imports
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "mosquitto.h"
// Shinda Imports
#include "utils/logging.hpp"
#include "macros.hpp"
#include "utils/timing.hpp"

// WIDTH x HEIGHT | COLS x ROWS
#define CHANNELS        3
#define TYPE            16

using namespace std;
using namespace cv;
namespace po = boost::program_options;

// Constants
const std::string TOPIC = "EXAMPLE_TOPIC";
const std::string PUB_ID = "Client_PUB";
const std::string SUB_ID = "Client_SUB";
const std::string HOST = "localhost";
const int PORT = 1883;

// Other Variables
unsigned int logLevel, deviceId, cols, rows, imgSize, avgFps = 0, fpsDataPoints= 0, i = 0, framesCaptured = 0, startTime = 0, timeNow = 0, fps = 0, tick = 0;
bool pub, shindaActive, show;
Mat frame;

void sigIntRoutine(int s) {
    LOG_APP(info, "SignalHandler", "Average FPS: " << avgFps / fpsDataPoints);
    exit(1); 
}

unsigned int fpsToMillies(unsigned int fps) {
    return 1000 / fps;
}

void onConnect(struct mosquitto* mosq, void* obj, int rc) {   
    if(rc) {
        LOG_APP(info, "Mosquitto-Subscriber", "Error with result Code: " << rc);
        throw;
    }
    mosquitto_subscribe(mosq, NULL, TOPIC.c_str(), 0);
}

void onMessage(struct mosquitto* mosq, void *obj, const struct mosquitto_message* msg) {

    //LOG_APP(info, "Mosquitto-Subscriber", "New message with topic " << msg->topic << ": " << msg->payloadlen << " bytes.");
    //LOG_APP(info, "CamSubscriber", "Received #" << ++i); 

    // Put received image data into OpenCV Mat 
    frame.data = (uchar*)(msg->payload);
    
    // Update FPS
    framesCaptured++;
    timeNow = (shinda::utils::timing::steady_microseconds() - startTime)*1e-6;
    if(timeNow - tick >= 1)
    {
        tick++;
        fps = framesCaptured;
        framesCaptured = 0;
        fpsDataPoints++;
        avgFps += fps;
    }

    //LOG_APP(info, "CamSubscriber", "FPS = " << fps);

    // Show received image data in live window
    if(show) {
        cv::putText(frame, cv::format("FPS: %u",fps), cv::Point(30, 30), cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0,0,255), 2);
        imshow("Live", frame);
        if (waitKey(fpsToMillies(30)) >= 0) {
            ;
        }
    }
}

auto main(int ac, char* av[]) -> int
{
    ////////////////////7//////////////////// IGNORE THE FOLLOWING ////////////////////////////////////////////

    // CATCH CTRL-C
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = sigIntRoutine;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    #pragma region SET_CUSTOM_LOGGIN_FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&shinda::utils::logging::coloring_formatter);
    #pragma endregion
    #pragma region READ_SUPPLIED_COMMAND_LINE_ARGS

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("pub", po::value<bool>(), "are you the publisher process?")
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5")
        ("shindaActive", po::value<bool>()->default_value(1), "Activate Shinda 0/1")
        ("show", po::value<bool>()->default_value(1), "Activate Live View 0/1")
        ("deviceId", po::value<unsigned int>()->default_value(0), "Select Device (default 0)")
        ("height", po::value<unsigned int>()->default_value(480), "Height of images to be transferred")
        ("width", po::value<unsigned int>()->default_value(640), "Width of images to be transferred");
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "ClientMain", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        /* -------------------- SET UP LOGGING FILTER -------------------- */
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );
        
        LOG_APP(info, "ClientMain", "logLevel = " << logLevel);
    }
    
    cols = vm["width"].as<unsigned int>();
    rows = vm["height"].as<unsigned int>();
    deviceId = vm["deviceId"].as<unsigned int>();
    pub = vm["pub"].as<bool>();
    show = vm["show"].as<bool>();
    shindaActive = vm["shindaActive"].as<bool>();
    LOG_APP(info, "ClientMain", "Publisher = " << pub);
    #pragma endregion

    ////////////////////7//////////////////// STOP IGNORING THE CODE -> THE INTERESTING PART STARTS NOW ////////////////////////////////////////////

    // Initialize Image Container
    imgSize = rows*cols*CHANNELS;
    frame = Mat::zeros(rows,cols,TYPE);

    // Initialize Mosquitto Client
    int rc;
    struct mosquitto* mosq;

    mosquitto_lib_init();

    if(pub)    // IF APP WAS STARTED AS PUBLISHER
    {
        // Configure mosquitto Client
        mosq = mosquitto_new(PUB_ID.c_str(), true, NULL);
        
        // Connect to mosquitto Broker
        rc = mosquitto_connect(mosq, HOST.c_str(), PORT, 60);
        if(rc != 0){
            LOG_APP(info, "Mosquitto-Publisher", "Could not connect to broker! Error Code: " << rc);
            mosquitto_destroy(mosq);
            throw;
        }
        LOG_APP(info, "Mosquitto-Publisher", "Successfully connected to broker!");

        // INITIALIZE VIDEOCAPTURE
        VideoCapture cap;

        // Select API backend
        int apiID = cv::CAP_ANY;      // 0 = autodetect default API

        // Open selected camera using selected API
        cap.open(deviceId, apiID);
        if (!cap.isOpened()) {
            LOG_APP(error, "CamPublisher", "Unable to open camera\n");
            return -1;
        }

        // Configure Camera
        auto codec = cv::VideoWriter::fourcc('M','J','P','G');

        // Configure Camera
        cap.set(cv::CAP_PROP_FOURCC, codec);
        cap.set(cv::CAP_PROP_FRAME_WIDTH, cols);
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, rows);

        LOG_APP(info, "CamPublisher", "Start grabbing");
        if(show) {
            LOG_APP(info, "CamPublisher", "Press any key to terminate");
        }

        startTime = shinda::utils::timing::steady_microseconds();

        for (;;)
        { 
            // Capture another frame from camera
            cap.read(frame);
            if (frame.empty()) {
                LOG_APP(error, "CamPublisher", "Blank frame grabbed");
                break;
            }

            // Publish image data via mosquitto
            if(mosquitto_publish(mosq, NULL, TOPIC.c_str(), imgSize, frame.data, 0, false) != MOSQ_ERR_SUCCESS)
                LOG_APP(info, "CamPublisher", "Failed to publish data.");
            //else
                //LOG_APP(info, "CamPublisher", "Sent #" << ++i);
            

            // Update FPS
            framesCaptured++;
            timeNow = (shinda::utils::timing::steady_microseconds() - startTime)*1e-6;
            if(timeNow - tick >= 1)
            {
                tick++;
                fps = framesCaptured;
                framesCaptured = 0;
                fpsDataPoints++;
                avgFps += fps;
            }

            //LOG_APP(info, "CamPublisher", "FPS = " << fps);

            // Show captured frame in live window
            if(show) {
                cv::putText(frame, cv::format("FPS: %u",fps), cv::Point(30, 30), cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0,0,255), 2);
                imshow("Live", frame);
                if (waitKey(fpsToMillies(30)) >= 0) {
                    break;
                }
            }
        } 

        // Cleanup
        mosquitto_disconnect(mosq);
        mosquitto_destroy(mosq);
    }
    else
    {
        // Configure Mosquitto Client
        mosq = mosquitto_new(SUB_ID.c_str(), true, 0);
        mosquitto_connect_callback_set(mosq, onConnect);
        mosquitto_message_callback_set(mosq, onMessage);

        // Connect to Mosquitto Broker
        rc = mosquitto_connect(mosq, HOST.c_str(), PORT, 10);
        if(rc) {
            LOG_APP(info, "Mosquitto-Subscriber", "Could not connect to broker! Error Code: " << rc);
            mosquitto_destroy(mosq);
            return -1;
        }
        LOG_APP(info, "Mosquitto-Subscriber", "Successfully connected to broker!");
        
        startTime = shinda::utils::timing::steady_microseconds();

        // Start Capturing MsgReceived Events
        mosquitto_loop_start(mosq);
        LOG_APP(info, "Mosquitto-Subscriber", "Press Enter to quit...");
        getchar();

        // Cleanup
        mosquitto_loop_stop(mosq, true);
        mosquitto_disconnect(mosq);
        mosquitto_destroy(mosq);
    }

    // Cleanup
    mosquitto_lib_cleanup();

    return 0;
}