// StdLib Imports
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
// Shinda Imports
#include "pubsub/ShindaClient.hpp"
#include "utils/logging.hpp"
#include "macros.hpp"

using namespace std;
using namespace shinda;
namespace po = boost::program_options;

auto main(int ac, char* av[]) -> int
{
    ///////////////////////////////////////// IGNORE THE FOLLOWING -> ONLY FOR READING CMD PARAMS ////////////////////////////////////////////

    #pragma region SET_CUSTOM_LOGGIN_FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&shinda::utils::logging::coloring_formatter);
    #pragma endregion
    #pragma region READ_SUPPLIED_COMMAND_LINE_ARGS
    unsigned int updateRate, logLevel;
    bool pub, shinda, show;

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("pub", po::value<bool>(), "are you the publisher process?")
        ("updateRate", po::value<unsigned int>()->default_value(0), "reading/writing rate [milliseconds] >= 0")
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5");
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["updateRate"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "ClientMain", "Invalid Update Rate. Try running the script with --updateRate <[milliseconds] >= 1>");
        return 0;
    }
    else
    {
        updateRate = vm["updateRate"].as<unsigned int>();
        LOG_APP(info, "ClientMain", "updateRate = " << updateRate << "ms");
    }

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "ClientMain", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        /* -------------------- SET UP LOGGING FILTER -------------------- */
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );
        
        LOG_APP(info, "ClientMain", "logLevel = " << logLevel);
    }
        
    pub = vm["pub"].as<bool>();
    LOG_APP(info, "ClientMain", "Publisher = " << pub);
    #pragma endregion

    ////////////////////7//////////////////// STOP IGNORING THE CODE -> THE INTERESTING PART STARTS NOW ////////////////////////////////////////////


    if(pub)    // IF APP WAS STARTED AS PUBLISHER
    {
        // Create Client Object, which offers methods for publishsing
        // and subscribing data to a Topic with a specific name. 
        // The Topic gets created automatically on trying to publish or subscribe
        // if it doesn't already exist.
        pubsub::ShindaClient<int> client("ClientNumberDemo", sizeof(int));
        if(client.connect() != status::StatusCode::SUCCESS) {
            LOG_APP(error, "NumberPublisher", "Could not connect to Shinda Broker");
            exit(1);
        }

        for (int numToSend = 0;;)
        { 
            // Publish a message to a sample Topic called "ClientDemo"
            // As a subscriber you would need to call client.spin()
            // on this Topic to read the data from the shared memory
            if(client.publish(&numToSend, sizeof(int)) != status::StatusCode::SUCCESS)
                LOG_APP(info, "NumberPublisher", "Failed to publish data.");
            else {
                LOG_APP(info, "NumberPublisher", "Published: " << numToSend);
                numToSend++;
            }

            // If an "updateRate" was supplied via the command line, 
            // wait certain amount of time until publishing data again.
            std::this_thread::sleep_for(std::chrono::milliseconds(updateRate)); 
        }
    }
    else    // IF APP WAS STARTED AS SUBSCRIBER (pub = 0/false)
    { 
        // Create Client Object, which offers methods for publishsing
        // and subscribing data to a Topic with a certain name. 
        // The Topic gets created automatically on trying to publish or subscribe
        // if it doesn't already exist.
        int* data;
        pubsub::ShindaClient<int> client("ClientNumberDemo", sizeof(int));
        if(client.connect() != status::StatusCode::SUCCESS) {
            LOG_APP(error, "NumberPublisher", "Could not connect to Shinda Broker");
            exit(1);
        }

        for (;;)
        {
            // Get the data which was published from shared memory by calling spin(). 
            // As a return value you will receive a pointer to the data read.
            if(client.spin(data) == status::StatusCode::SUCCESS) 
                LOG_APP(info, "NumberSubscriber", "Received: " << *data);
            else
                LOG_APP(info, "NumberSubscriber", "Failed to read data.");

            // If an "updateRate" was supplied via the command line, 
            // wait certain amount of time until reading data again.
            std::this_thread::sleep_for(std::chrono::milliseconds(updateRate)); 
        }
    }

    return 0;
}
