// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
// Shinda Imports
#include "pubsub/ShindaClient.hpp"
#include "utils/logging.hpp"
#include "macros.hpp"
#include "utils/random.hpp"

using namespace std;
using namespace shinda;
namespace po = boost::program_options;


auto main(int ac, char* av[]) -> int
{
    ////////////////////7//////////////////// IGNORE THE FOLLOWING -> ONLY FOR READING CMD PARAMS ////////////////////////////////////////////

    #pragma region SET_CUSTOM_LOGGIN_FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&utils::logging::coloring_formatter);
    #pragma endregion
    #pragma region READ_SUPPLIED_COMMAND_LINE_ARGS
    unsigned int iters, logLevel, updateRate, maxDataSize;
    bool pub;

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("pub", po::value<bool>(), "are you the publisher process?")
        ("updateRate", po::value<unsigned int>()->default_value(0), "reading/writing rate [milliseconds] >= 0")
        ("iters", po::value<unsigned int>()->default_value(10), "reading/writing iterations >= 1")
        ("maxDataSize", po::value<unsigned int>()->default_value(sizeof(memory::SharedMsgHeader) + 1), "maxDataSize > " + sizeof(memory::SharedMsgHeader))
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5");
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "ClientMain", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        /* -------------------- SET UP LOGGING FILTER -------------------- */
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );
        
        LOG_APP(info, "ClientMain", "logLevel = " << logLevel);
    }
        
    if(vm["updateRate"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "ClientMain", "Invalid Update Rate. Try running the script with --updateRate <[milliseconds] >= 1>");
        return 0;
    }
    else
    {
        updateRate = vm["updateRate"].as<unsigned int>();
        LOG_APP(info, "ClientMain", "updateRate = " << updateRate << "ms");
    }

    if(vm["maxDataSize"].as<unsigned int>() <= sizeof(memory::SharedMsgHeader))
    {
        LOG_APP(error, "ClientMain", "Invalid Message Size. Try running the script with --maxDataSize <number >= " << sizeof(memory::SharedMsgHeader) + 1 << ">");
        return 0;
    }
    else
    {
        maxDataSize = vm["maxDataSize"].as<unsigned int>();
        LOG_APP(info, "ClientMain", "maxDataSize = " << vm["maxDataSize"].as<unsigned int>());
    }
        
    if(vm["iters"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "ClientMain", "Invalid #Iterations. Try running the script with --iters <number >= 1>");
        return 0;
    }
    else
    {
        iters = vm["iters"].as<unsigned int>();
        LOG_APP(info, "ClientMain", "iters = " << iters);
    }

    pub = vm["pub"].as<bool>();
    LOG_APP(info, "ClientMain", "Publisher = " << pub);
    #pragma endregion

    ////////////////////7//////////////////// STOP IGNORING THE CODE -> THE INTERESTING PART STARTS NOW ////////////////////////////////////////////

    if(pub)    // IF APP WAS STARTED AS PUBLISHER (--pub=1/true)
    {
        // Generate dummy data which will be put through shared memory to subscribers
        // You can pass any primitive data types to the client.publishMsg(...) method but NEVER EVER
        // pass a data structure that contains a pointer to it (e.g. a struct containing a pointer
        // or a std object which most likely contains pointers internally). Pointers cannot be stored in 
        // shared memory without special treatment. In this example, I'm sending a simple string 
        // which is internally copied from the data char pointer.
        //const char* data = std::string(maxDataSize, '*').c_str();//utils::random::random_string(maxDataSize).c_str();
        std::string data = utils::random::random_string(maxDataSize);

        // Create Client Object, which offers methods for publishsing
        // and subscribing data to a Topic with a specific name. 
        // The Topic gets created automatically on trying to publish or subscribe
        // if it doesn't already exist.
        pubsub::ShindaClient<char> client("ClientDemo", maxDataSize);
        if(client.connect() != status::StatusCode::SUCCESS)
            exit(1);

        // Publish dummy data "iters" - times (supplied through command line args)
        for(unsigned int i = 0; i < iters; i++)
        {
            LOG_APP(debug, "Publisher", "______________________________________________");

            // Publish a message to a sample Topic called "ClientDemo"
            // As a subscriber you would need to call client.spin()
            // on this Topic to read the data from the shared memory
            if(client.publish(data.c_str(), maxDataSize) == status::StatusCode::SUCCESS)
                LOG_APP(info, "DemoClient", "Message Published: " << data);

            // If an "updateRate" was supplied via the command line, 
            // wait certain amount of time until publishing data again.
            std::this_thread::sleep_for(std::chrono::milliseconds(updateRate)); 
        }
    }
    else    // IF APP WAS STARTED AS SUBSCRIBER (--pub = 0/false)
    { 
        char* data;

        // Create Client Object, which offers methods for publishsing
        // and subscribing data to a Topic with a certain name. 
        // The Topic gets created automatically on trying to publish or subscribe
        // if it doesn't already exist.
        pubsub::ShindaClient<char> client("ClientDemo", maxDataSize);
        if(client.connect() != status::StatusCode::SUCCESS)
            exit(1);

        // Read dummy data "iters" - times (supplied through command line args)
        for(unsigned int i = 0; i < iters; i++)
        {
            LOG_APP(debug, "Subscriber", "______________________________________________");

            // Get the data which was published from shared memory by calling spin(). 
            // As a return value you will receive a pointer to the data read.
            if(client.spin(data) == status::StatusCode::SUCCESS)
                LOG_APP(info, "DemoClient", "Message Read: " << data); // DO SOMETHING WITH THE DATA READ

            // If an "updateRate" was supplied via the command line, 
            // wait certain amount of time until reading data again.
            std::this_thread::sleep_for(std::chrono::milliseconds(updateRate));
        }
    }

    return 0;
}
