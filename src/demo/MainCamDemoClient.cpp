// StdLib Imports
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
// Shinda Imports
#include "pubsub/ShindaClient.hpp"
#include "utils/logging.hpp"
#include "macros.hpp"
#include "utils/timing.hpp"

// WIDTH x HEIGHT | COLS x ROWS
#define CHANNELS        3
#define TYPE            16

using namespace std;
using namespace cv;
using namespace shinda;
namespace po = boost::program_options;

uint64_t avgFps = 0, fpsDataPoints= 0;

void sigIntRoutine(int s) {
    LOG_APP(info, "SignalHandler", "Average FPS: " << avgFps / fpsDataPoints);
    exit(1); 
}

unsigned int fpsToMillies(unsigned int fps) {
    return 1000 / fps;
}

auto main(int ac, char* av[]) -> int
{
    ////////////////////7//////////////////// IGNORE THE FOLLOWING ////////////////////////////////////////////

    // CATCH CTRL-C
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = sigIntRoutine;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    #pragma region SET_CUSTOM_LOGGIN_FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&shinda::utils::logging::coloring_formatter);
    #pragma endregion
    #pragma region READ_SUPPLIED_COMMAND_LINE_ARGS
    unsigned int logLevel, deviceId, cols, rows;
    bool pub, shinda, show;

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("pub", po::value<bool>(), "are you the publisher process?")
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5")
        ("shinda", po::value<bool>()->default_value(1), "Activate Shinda 0/1")
        ("show", po::value<bool>()->default_value(1), "Activate Live View 0/1")
        ("deviceId", po::value<unsigned int>()->default_value(0), "Select Device (default 0)")
        ("height", po::value<unsigned int>()->default_value(480), "Height of images to be transferred")
        ("width", po::value<unsigned int>()->default_value(640), "Width of images to be transferred");
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "ClientMain", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        /* -------------------- SET UP LOGGING FILTER -------------------- */
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );
        
        LOG_APP(info, "ClientMain", "logLevel = " << logLevel);
    }
    
    cols = vm["width"].as<unsigned int>();
    rows = vm["height"].as<unsigned int>();
    deviceId = vm["deviceId"].as<unsigned int>();
    pub = vm["pub"].as<bool>();
    show = vm["show"].as<bool>();
    shinda = vm["shinda"].as<bool>();
    LOG_APP(info, "ClientMain", "Publisher = " << pub);
    #pragma endregion

    ////////////////////7//////////////////// STOP IGNORING THE CODE -> THE INTERESTING PART STARTS NOW ////////////////////////////////////////////

    uint64_t i = 0, framesCaptured = 0, startTime = 0, tick = 0, timeNow = 0, fps = 0, imgSize = rows*cols*CHANNELS;
    Mat frame = Mat::zeros(rows,cols,TYPE);
    pubsub::ShindaClient<uchar> client("ClientDemo", imgSize);
    if(client.connect() != status::StatusCode::SUCCESS)
        exit(1);
    //VideoWriter video("/home/pi/001.avi", cv::VideoWriter::fourcc('M','J','P','G'), 24, Size(fwidth, fheight));

    if(pub)    // IF APP WAS STARTED AS PUBLISHER
    {     
        // Initialize Videocamera
        VideoCapture cap;

        // Select API backend
        int apiID = cv::CAP_ANY;      // 0 = autodetect default API

        // Open selected camera using selected API
        cap.open(deviceId, apiID);
        if (!cap.isOpened()) {
            LOG_APP(error, "CamPublisher", "Unable to open camera\n");
            return -1;
        }

        // Configure Camera
        // CAMERA SETUP & SELECTED CODEC CAN CAUSE SEVERE FPS LIMITS
        cap.set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M','J','P','G'));
        cap.set(cv::CAP_PROP_FRAME_WIDTH, cols);
        cap.set(cv::CAP_PROP_FRAME_HEIGHT, rows);

        LOG_APP(info, "CamPublisher", "Start grabbing");
        if(show) {
            LOG_APP(info, "CamPublisher", "Press any key to terminate");
        }

        startTime = shinda::utils::timing::steady_microseconds();

        for (;;)
        { 
            // Capture another frame from the camera
            cap.read(frame);
            if (frame.empty()) {
                LOG_APP(error, "CamPublisher", "Blank frame grabbed");
                break;
            }

            // Publish a message to shared memory
            if(shinda) {
                if(client.publish(frame.data, imgSize) != status::StatusCode::SUCCESS)
                    LOG_APP(info, "CamPublisher", "Failed to publish data.");
                //else
                //    LOG_APP(info, "CamPublisher", "Sent #" << ++i);
            }

            // Update FPS
            framesCaptured++;
            timeNow = (shinda::utils::timing::steady_microseconds() - startTime)*1e-6;
            if(timeNow - tick >= 1)
            {
                tick++;
                fps = framesCaptured;
                framesCaptured = 0;
                fpsDataPoints++;
                avgFps += fps;
            }

            //LOG_APP(info, "CamPublisher", "FPS = " << fps);

            // Show captured image in live window
            if(show) {
                cv::putText(frame, cv::format("FPS: %lu",fps), cv::Point(30, 30), cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0,0,255), 2);
                imshow("Live", frame);
                if (waitKey(fpsToMillies(30)) >= 0) {
                    break;
                }
            }
        }
        LOG_APP(info, "CamPublisher", "Average FPS: " << avgFps);
    }
    else    // IF APP WAS STARTED AS SUBSCRIBER (pub = 0/false)
    { 
        startTime = shinda::utils::timing::steady_microseconds();

        for (;;)
        {
            // Read image data from shared memory
            if(client.spin(frame.data) == status::StatusCode::SUCCESS) 
            {
                // Update FPS
                framesCaptured++;
                timeNow = (shinda::utils::timing::steady_microseconds() - startTime)*1e-6;

                if(timeNow - tick >= 1)
                {
                    tick++;
                    fps = framesCaptured;
                    framesCaptured = 0;
                    fpsDataPoints++;
                    avgFps += fps;
                }

                //LOG_APP(info, "CamPublisher", "FPS = " << fps);

                // Show received image in live window
                if(show) {
                    cv::putText(frame, cv::format("FPS: %lu",fps), cv::Point(30, 30), cv::FONT_HERSHEY_SIMPLEX, 0.8, cv::Scalar(0,0,255), 2);
                    imshow("Live", frame);
                    if (waitKey(fpsToMillies(30)) >= 0) {
                        break;
                    }
                }

                // Save image data to file
                //video.write(frame);
                //imwrite("/home/pi/" + std::to_string(framesCaptured) + ".jpg", frame);
            }
        }
    }

    return 0;
}
