// StdLib Imports
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
// Shinda Imports
#include "pubsub/ShindaClient.hpp"
#include "utils/logging.hpp"
#include "macros.hpp" 

using namespace std;
using namespace shinda;
namespace po = boost::program_options;

auto main(int ac, char* av[]) -> int
{
    ///////////////////////////////////////// IGNORE THE FOLLOWING -> ONLY FOR READING CMD PARAMS ////////////////////////////////////////////

    #pragma region SET_CUSTOM_LOGGIN_FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&shinda::utils::logging::coloring_formatter);
    #pragma endregion
    #pragma region READ_SUPPLIED_COMMAND_LINE_ARGS
    unsigned int updateRate, logLevel;
    bool pub, shinda, show;

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("pub", po::value<bool>(), "are you the publisher process?")
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5");
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "ClientMain", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        /* -------------------- SET UP LOGGING FILTER -------------------- */
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );
        
        LOG_APP(info, "ClientMain", "logLevel = " << logLevel);
    }
        
    pub = vm["pub"].as<bool>();
    LOG_APP(info, "ClientMain", "Publisher = " << pub);
    #pragma endregion

    ////////////////////7//////////////////// STOP IGNORING THE CODE -> THE INTERESTING PART STARTS NOW ////////////////////////////////////////////
    constexpr int bufSize = 64;

    if(pub)    // IF APP WAS STARTED AS PUBLISHER
    {
        // Create Client Object, which offers methods for publishsing
        // and subscribing data to a Topic with a specific name. 
        // The Topic gets created automatically on trying to publish or subscribe
        // if it doesn't already exist.
        pubsub::ShindaClient<char> client("ClientAudioDemo", bufSize);
        if(client.connect() != status::StatusCode::SUCCESS) {
            LOG_APP(error, "AudioPublisher", "Could not connect to Shinda Broker");
            exit(1);
        }

        char buffer[bufSize]{};
        auto fi = std::ifstream("./carengine.wav", std::ios::binary | std::ios::in);
        fi.seekg(44);
        bool eof = false;
        while(!eof)
        {
            LOG_APP(error, "AudioPublisher", "In");
            fi.read(buffer, bufSize); // read up to the size of the buffer
            if(fi.eof()) eof = true;

            if(client.publish(buffer, bufSize) != status::StatusCode::SUCCESS) {
                LOG_APP(info, "AudioPublisher", "Failed to publish data.");
            } 
        }   
        fi.close();
    }
    else    // IF APP WAS STARTED AS SUBSCRIBER (pub = 0/false)
    { 
        // Create Client Object, which offers methods for publishsing
        // and subscribing data to a Topic with a certain name. 
        // The Topic gets created automatically on trying to publish or subscribe
        // if it doesn't already exist. 
        pubsub::ShindaClient<char> client("ClientAudioDemo", bufSize);
        if(client.connect() != status::StatusCode::SUCCESS) {
            LOG_APP(error, "AudioSubscriber", "Could not connect to Shinda Broker");
            exit(1);
        }

        char* data;
        auto fo = std::ofstream("target.raw", ios::binary | ios::out);
        for (;;)
        {
            // Get the data which was published from shared memory by calling spin(). 
            // As a return value you will receive a pointer to the data read.
            if(client.spin(data) == status::StatusCode::SUCCESS) 
            {
                fo.write(data, bufSize);  
            }
            else {
                LOG_APP(info, "AudioSubscriber", "Failed to read data.");
            }
        }
        fo.close();
    }

    return 0;
}



















        // #include <AL/al.h>
        // #include <AL/alc.h>
        // ALCdevice *device;

        // device = alcOpenDevice(NULL);
        // if (!device) {
        //     std::cout << "Failed to open device" << std::endl;
        //     exit(1);
        // }
            
        // alGetError();

        // ALCcontext *context;
        // context = alcCreateContext(device, NULL);
        // if (!alcMakeContextCurrent(context)) {
        //     std::cout << "Failed to create context" << std::endl;
        //     exit(1);
        // }

        // ALfloat listenerOri[] = { 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f };
        // alListener3f(AL_POSITION, 0, 0, 1.0f);
        // alListener3f(AL_VELOCITY, 0, 0, 0);
        // alListenerfv(AL_ORIENTATION, listenerOri);

        // ALuint source;


        // alGenSources((ALuint)1, &source);
        // alSourcef(source, AL_PITCH, 1);
        // alSourcef(source, AL_GAIN, 1);
        // alSource3f(source, AL_POSITION, 0, 0, 0);
        // alSource3f(source, AL_VELOCITY, 0, 0, 0);
        // alSourcei(source, AL_LOOPING, AL_FALSE);

        // ALuint buffer;
        // alGenBuffers((ALuint)1, &buffer);

        // ALsizei size, freq;
        // ALenum format;
        // ALvoid *data;
        // ALboolean loop = AL_FALSE;
        // int channel, bps;
        // data = loadWAV("./carengine.wav", channel, freq, bps, size);
        // std::cout << size << std::endl;
        // if (channel == 1)
        // {
        //     if (bps == 8)
        //     {
        //         format = AL_FORMAT_MONO8;
        //     }
        //     else {
        //         format = AL_FORMAT_MONO16;
        //     }
        // }
        // else {
        //     if (bps == 8)
        //     {
        //         format = AL_FORMAT_STEREO8;
        //     }
        //     else {
        //         format = AL_FORMAT_STEREO16;
        //     }
        // }

        // alBufferData(buffer, format, data, size, freq);
        
        // alSourcei(source, AL_BUFFER, buffer);

        // alSourcePlay(source);

        // ALint source_state;
        // alGetSourcei(source, AL_SOURCE_STATE, &source_state);
        // while (source_state == AL_PLAYING) {
        //         alGetSourcei(source, AL_SOURCE_STATE, &source_state);
        // }

        // alDeleteSources(1, &source);
        // alDeleteBuffers(1, &buffer);
        // device = alcGetContextsDevice(context);
        // alcMakeContextCurrent(NULL);
        // alcDestroyContext(context);
        // alcCloseDevice(device);
                            

        // int channel, sampleRate, bps, size;
        // char* data = loadWAV("./carengine.wav", channel, sampleRate, bps, size);
        // const ALCchar *defaultDeviceName = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);
        // ALCdevice* device = alcOpenDevice(defaultDeviceName);
        // if (device == NULL)
        // {
        //     std::cout << "cannot open sound card" << std::endl;
        //     return 0;
        // }
        // else
        // {
        //     std::cout << "Device: " << defaultDeviceName << std::endl;
        // }
        // ALCcontext* context = alcCreateContext(device, NULL);
        // if (context == NULL)
        // {
        //     std::cout << "cannot open context" << std::endl;
        //     return 0;
        // }
        // alcMakeContextCurrent(context);

        // std::cout << "Channels: " << channel << std::endl;
        // std::cout << "SampleRate: " << sampleRate << std::endl;
        // std::cout << "BPS: " << bps << std::endl;
        // std::cout << "Size: " << size << std::endl;

        // unsigned int bufferid, format;
        // alGenBuffers(1, &bufferid);
        // if (channel == 1)
        // {
        //     if (bps == 8)
        //     {
        //         format = AL_FORMAT_MONO8;
        //     }
        //     else {
        //         format = AL_FORMAT_MONO16;
        //     }
        // }
        // else {
        //     if (bps == 8)
        //     {
        //         format = AL_FORMAT_STEREO8;
        //     }
        //     else {
        //         format = AL_FORMAT_STEREO16;
        //     }
        // }
        // alBufferData(bufferid, format, data, size, sampleRate);
        // unsigned int sourceid;
        // alGenSources(1, &sourceid);
        // alSourcei(sourceid, AL_BUFFER, bufferid);
        // alSourcePlay(sourceid);

        // while (true)
        // {

        // }

        // alDeleteSources(1, &sourceid);
        // alDeleteBuffers(1, &bufferid);

        // alcDestroyContext(context);
        // alcCloseDevice(device);
        // delete[] data;

        // bool isBigEndian()
        // {
        //     int a = 1;
        //     return !((char*)&a)[0];
        // }

        // int convertToInt(char* buffer, int len)
        // {
        //     int a = 0;
        //     if (!isBigEndian())
        //         for (int i = 0; i<len; i++)
        //             ((char*)&a)[i] = buffer[i];
        //     else
        //         for (int i = 0; i<len; i++)
        //             ((char*)&a)[3 - i] = buffer[i];
        //     return a;
        // }

        // char* loadWAV(const char* fn, int& chan, int& samplerate, int& bps, int& size)
        // {
        //     char buffer[4];
        //     std::ifstream in(fn, std::ios::binary);
        //     in.read(buffer, 4); // 1-4
        //     if (strncmp(buffer, "RIFF", 4) != 0)
        //     {
        //         std::cout << "this is not a valid WAVE file" << std::endl;
        //         return NULL;
        //     }
        //     in.read(buffer, 4);      // 5-8
        //     in.read(buffer, 4);      //WAVE 9-12
        //     in.read(buffer, 4);      //fmt 13-16
        //     in.read(buffer, 4);      //16 17-20
        //     in.read(buffer, 2);      //1 21-22
        //     in.read(buffer, 2);      // 23-24
        //     chan = convertToInt(buffer, 2);
        //     in.read(buffer, 4);      // 25-28
        //     samplerate = convertToInt(buffer, 4);
        //     in.read(buffer, 4);      // 29-32
        //     in.read(buffer, 2);      // 33-34
        //     in.read(buffer, 2);      // 35-36
        //     bps = convertToInt(buffer, 2);
        //     in.read(buffer, 4);      //data // 37-40
        //     in.read(buffer, 4);      // 41-44
        //     size = convertToInt(buffer, 4);
        //     std::cout << "Size: " << size << std::endl;
        //     char* data = new char[size];
        //     in.read(data, size);
        //     return data;
        // }

        // static void list_audio_devices(const ALCchar *devices)
        // {
        //     const ALCchar *device = devices, *next = devices + 1;
        //     size_t len = 0;

        //     fprintf(stdout, "Devices list:\n");
        //     fprintf(stdout, "----------\n");
        //     while (device && *device != '\0' && next && *next != '\0') {
        //         fprintf(stdout, "%s\n", device);
        //         len = strlen(device);
        //         device += (len + 1);
        //         next += (len + 2);
        //     }
        //     fprintf(stdout, "----------\n");
        // }