// StdLib Imports
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <vector>
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
// Local Imports
#include "pubsub/ShindaClient.hpp"
#include "memory/SharedMessage.hpp"
#include "utils/logging.hpp"
#include "utils/benchmarking.hpp"
#include "utils/timing.hpp"
#include "macros.hpp"
#include "utils/random.hpp"

using namespace std;
using namespace shinda;
using namespace shinda::pubsub;
namespace po = boost::program_options;

int main(int ac, char* av[]) 
{
    #pragma region SET CUSTOM LOGGING FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&utils::logging::coloring_formatter);
    #pragma endregion 

    #pragma region APPLY COMMAND LINE ARGS
    unsigned int logLevel, epochs, subs, minMsgPower, maxMsgPower, maxPower, pubTimeout, subTimeout;
    double msgPowerStep;

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5")
        ("epochs", po::value<unsigned int>()->default_value(1), "take mean of epochs for each msgSize")
        ("minMsgPower", po::value<unsigned int>()->default_value(constants::kDefaultMinMsgPower), "2^(minMsgPower) will be smallest msgSize used for transmission >= 0")
        ("maxMsgPower", po::value<unsigned int>()->default_value(constants::kDefaultMaxMsgPower), "2^(maxMsgPower) will be largest msgSize used for transmission >= minMsgPower")
        ("msgPowerStep", po::value<double>()->default_value(constants::kDefaultMsgPowerStep), "Step for increasing MsgPower between minMsgPower & maxMsgPower > 0")
        ("maxPower", po::value<unsigned int>()->default_value(constants::kDefaultTransmitMaxPower), "2^(maxPower) data will be transmitted for each msgSize >= maxMsgPower")
        ("subs", po::value<unsigned int>()->default_value(1), "Number of subscriber processes >= 1")
        ("pubTimeout", po::value<unsigned int>()->default_value(0), "Time after which trying to publish a message is aborted in microseconds >= 0")
        ("subTimeout", po::value<unsigned int>()->default_value(0), "Time after which trying to read a message is aborted in microseconds >= 0");

    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "main", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        // -------------------- SET UP LOGGING FILTER -------------------- 
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );

        LOG_APP(debug, "main", "logLevel = " << vm["logLevel"].as<unsigned int>());
    }

    if(vm["epochs"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "main", "Invalid iterations for taking the mean for each step.Try running the script with --epochs <number >= 1>");
        return 0;
    }
    else
    {
        epochs = vm["epochs"].as<unsigned int>();
        LOG_APP(debug, "main", "epochs = " << vm["epochs"].as<unsigned int>());
    }

    if(vm["minMsgPower"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "main", "Minimum MsgSize must be at least 1 Byte. Try running the script with --minMsgPower <number >= 1>");
        return 0;
    }
    else
    {
        minMsgPower = vm["minMsgPower"].as<unsigned int>();
        LOG_APP(debug, "main", "minMsgPower = " << vm["minMsgPower"].as<unsigned int>());
    }

    if(vm["maxMsgPower"].as<unsigned int>() < minMsgPower)
    {
        LOG_APP(error, "main", "Maximum MsgSize must be at least minMsgPower. Try running the script with --maxMsgPower <number >= minMsgPower>");
        return 0;
    }
    else
    {
        maxMsgPower = vm["maxMsgPower"].as<unsigned int>();
        LOG_APP(debug, "main", "maxMsgPower = " << vm["maxMsgPower"].as<unsigned int>());
    }

    if(vm["msgPowerStep"].as<double>() < 0)
    {
        LOG_APP(error, "main", "Step must be greater than zero to increase. Try running the script with --msgPowerStep <number >= 0>");
        return 0;
    }
    else
    {
        msgPowerStep = vm["msgPowerStep"].as<double>();
        LOG_APP(debug, "main", "msgPowerStep = " << vm["msgPowerStep"].as<double>());
    }

    if(vm["maxPower"].as<unsigned int>() < maxMsgPower)
    {
        LOG_APP(error, "main", "maxPower needs to be at least maxMsgPower. Try running the script with --maxPower <number >= maxMsgPower>");
        return 0;
    }
    else
    {
        maxPower = vm["maxPower"].as<unsigned int>();
        LOG_APP(debug, "main", "maxPower = " << vm["maxPower"].as<unsigned int>());
    }

    if(vm["subs"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "main", "Invalid number of Subscribers. Try again with --subs <number >= 1>");
        return 0;
    }
    else
    {
        subs = vm["subs"].as<unsigned int>();
        LOG_APP(debug, "main", "subs = " << vm["subs"].as<unsigned int>());
    }

    if(vm["pubTimeout"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "main", "Invalid publisher timeout. Try again with --pubTimeout <number >= 0>");
        return 0;
    }
    else
    {
        pubTimeout = vm["pubTimeout"].as<unsigned int>();
        LOG_APP(debug, "main", "pubTimeout = " << vm["pubTimeout"].as<unsigned int>());
    }

    if(vm["subTimeout"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "main", "Invalid subscriber timeout. Try again with --subTimeout <number >= 0>");
        return 0;
    }
    else
    {
        subTimeout = vm["subTimeout"].as<unsigned int>();
        LOG_APP(debug, "main", "subTimeout = " << vm["subTimeout"].as<unsigned int>());
    }

    #pragma endregion   

    #pragma region MEASUREMENT SETUP

    // Initialize Variables;
    memory::SharedMsg msg = memory::SharedMsg::createEmpty();
    utils::bench::sync::IPC_Barrier barrier(subs + 1, boost::uuids::to_string(boost::uuids::random_generator()()));
    std::string shmUid = boost::uuids::to_string(boost::uuids::random_generator()());
    uint64_t msgSize, dataSize, itersPerTransmit, startTime, stopTime, msgCount, timeoutCount;
    vector<double> msgSizes, writeTimes, readTimes, startTimes, endTimes, msgsPublished, msgsReceived, subTimeoutTimes, pubTimeoutTimes;
    const int FULL_TRANSMIT_SIZE = pow(2, maxPower);

    // Setup Measurement Object
    utils::bench::MeasurementDetails details;
    details.numSubs = subs;
    details.logLevel = logLevel;
    details.maxMsgSizePower = maxPower;
    details.epochs = epochs;
    details.type = utils::bench::CAP;
    utils::bench::Measurement meas("CapBandwidthBench", details); 

    // Setup Forking
    int publisherStatus;
    vector<int> subscriberStatus;
    pid_t publisherPid;
    vector<pid_t> subscriberPids;

    #pragma endregion

    if ((publisherPid = fork()) == -1) {
        exit(1);
    }

    if (publisherPid == 0) 
    {
        LOG_APP(info, "Publisher #" << to_string(getpid()), "Started...");

        for(double i = minMsgPower; i < maxMsgPower; i+=msgPowerStep)
        {
            // Compute values/sizes of message content
            msgSize = pow(2, i);
            dataSize = msgSize - sizeof(memory::SharedMsgHeader);

            // Generate Data
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "Generating Data...");

            std::string dataPub = utils::random::random_string(dataSize);

            // Compute number of writes
            itersPerTransmit = ceil(FULL_TRANSMIT_SIZE / msgSize);  
  
            // Initial Rondevouz
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "Waiting for Subscriber to be available...");
            barrier.wait();

            // Create SHM + Warmup
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "Negotiate SHM...");
            pubsub::ShindaClient<char> client(shmUid, dataSize);
            if(client.connect() != status::StatusCode::SUCCESS)
                exit(1);
            client.try_publish(dataPub.c_str(), dataSize);

            // Signal Subscriber that it now can initialize
            LOG_APP(info, "Publisher", "Waiting for Subscriber to negotiate SHM...");
            barrier.wait();

            // Wait for subscriber to finish initialization
            LOG_APP(info, "Publisher #" << to_string(getpid()), "------- Ready for MsgSize: " << msgSize << " Byte -------");
            barrier.wait();

            // Log Progress
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "MsgSize = " << msgSize << "Byte");
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "DataSize = " << dataSize << "Byte");
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "FullTransfer = " << FULL_TRANSMIT_SIZE << "Byte");
            LOG_DEBUG(info, "Publisher #" << to_string(getpid()), "Iterations = " << itersPerTransmit);
            
            for(unsigned int mi = 0; mi < epochs; mi++)
            {
                msgCount = 0, timeoutCount = 0, startTime = 0, stopTime = 0;
            
                startTime = utils::timing::steady_microseconds();

                for(unsigned int i = 0; i < itersPerTransmit; i++) {
                    if(client.publish(dataPub.c_str(), dataSize) == status::StatusCode::SUCCESS) {
                        msgCount++;     
                    }
                    else {
                        timeoutCount++; 
                    }
                }  
                    
                stopTime = utils::timing::steady_microseconds();
                
                msgsPublished.push_back(msgCount);
                startTimes.push_back(startTime*1e-6);
                writeTimes.push_back((stopTime - startTime)*1e-6);
                msgSizes.push_back(msgSize);
            }   

            LOG_APP(info, "Publisher #" << to_string(getpid()), "Finished...");
        }

        meas.csvWriter.filename() = meas.pubPath + "publisher";
        meas.csvWriter.addColumn<double>(msgSizes, "MsgSize", "Byte");
        meas.csvWriter.addColumn<double>(writeTimes, "WriteTime", "s");
        meas.csvWriter.addColumn<double>(startTimes, "StartTimes", "s");
        //meas.csvWriter.addColumn<double>(pubTimeoutTimes, "PubTimeoutTimes", "s");
        meas.csvWriter.addColumn<double>(msgsPublished, "MsgsPublished", "");

        // Write Measurement to file
        meas.writeMeasurement();

        return 0;
    }
    else
    {
        for (int i = 0; i < subs; i++) {
            pid_t childPid = fork();
            subscriberPids.push_back(childPid);
            if (childPid == -1) {
                exit(1);
            }
            if (childPid == 0) 
            {
                LOG_APP(info, "Subscriber #" << to_string(getpid()), "Started...");
                char* dataRec;

                for(double i = minMsgPower; i < maxMsgPower; i+=msgPowerStep)
                {
                    // Compute values/sizes of message content
                    msgSize = pow(2, i);
                    dataSize = msgSize - sizeof(memory::SharedMsgHeader);

                    // Compute number of writes
                    itersPerTransmit = ceil(FULL_TRANSMIT_SIZE / msgSize);

                    // Initial Rondevouz
                    LOG_DEBUG(info, "Subscriber #" << to_string(getpid()), "Waiting for Publisher to be available...");
                    barrier.wait();

                    // Wait for Publisher to finish initialization
                    LOG_APP(info, "Subscriber", "Waiting for Publisher to negotiate SHM...");
                    barrier.wait();

                    // Create SHM
                    LOG_DEBUG(info, "Subscriber #" << to_string(getpid()), "Negotiate SHM...");
                    pubsub::ShindaClient<char> client(shmUid, dataSize);
                    if(client.connect() != status::StatusCode::SUCCESS)
                        exit(1);
                    client.try_spin(dataRec);

                    // Signal Publisher that initialization finished
                    LOG_APP(info, "Subscriber #" << to_string(getpid()), "------- Ready for MsgSize: " << msgSize << " Byte -------");
                    barrier.wait();

                    for(unsigned int mi = 0; mi < epochs; mi++)
                    {
                        msgCount = 0, timeoutCount = 0, startTime = 0, stopTime = 0;

                        startTime = utils::timing::steady_microseconds();

                        for(unsigned int i = 0; i < itersPerTransmit; i++) {
                            if(client.spin(dataRec) == status::StatusCode::SUCCESS) { 
                                msgCount++;
                            }
                            else {
                                timeoutCount++;
                            }
                        } 
                            
                        stopTime = utils::timing::steady_microseconds();

                        // Log statistics
                        msgsReceived.push_back(msgCount);
                        endTimes.push_back(stopTime*1e-6);
                        readTimes.push_back((stopTime - startTime)*1e-6);
                        //subTimeoutTimes.push_back(timeoutCount*client.subscriberTimeout()*1e-6);
                    }   
        
                    LOG_APP(info, "Subscriber #" << to_string(getpid()), "Finished...");
                }

                meas.csvWriter.filename() = meas.subPath + "subscriber_" + std::to_string(getpid());
                meas.csvWriter.addColumn<double>(readTimes, "ReadTime", "s");
                meas.csvWriter.addColumn<double>(endTimes, "EndTimes", "s");
                //meas.csvWriter.addColumn<double>(subTimeoutTimes, "SubTimeoutTimes", "s");
                meas.csvWriter.addColumn<double>(msgsReceived, "MsgsReceived", "");
                            
                // Write Measurement to file
                meas.writeMeasurement();

                return 0;
            }
        }

        // Parent
        waitpid(publisherPid, &publisherStatus, 0);
        printf("Publisher #%d finished.\n", publisherPid);

        for(int i = 0; i < subscriberPids.size(); i++) {
            waitpid(subscriberPids[i], &(subscriberStatus[i]), 0);
            printf("Subscriber #%d finished.\n", subscriberPids[i]);
        }

        barrier.release();
    }

    return 0;
}