// StdLib Imports
#include <iostream>
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
// Local Imports
#include "utils/logging.hpp"
#include "utils/benchmarking.hpp"
#include "utils/timing.hpp"
#include "macros.hpp"
#include "utils/random.hpp"
#include "memory/SharedMemoryBlock.hpp"
#include "pubsub/ShindaClient.hpp"
#include "memory/SharedMessage.hpp"

using namespace std;
using namespace shinda;
namespace po = boost::program_options;

auto main(int ac, char* av[]) -> int
{
    #pragma region SET CUSTOM LOGGING FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&utils::logging::coloring_formatter);
    #pragma endregion 

    #pragma region APPLY COMMAND LINE ARGS
    unsigned int logLevel, epochs, subs, minMsgPower, maxMsgPower, maxPower;
    double msgPowerStep;

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5")
        ("epochs", po::value<unsigned int>()->default_value(1), "take mean of epochs for each msgSize")
        ("minMsgPower", po::value<unsigned int>()->default_value(constants::kDefaultMinMsgPower), "2^(minMsgPower) will be smallest msgSize used for transmission >= 0")
        ("maxMsgPower", po::value<unsigned int>()->default_value(constants::kDefaultMaxMsgPower), "2^(maxMsgPower) will be largest msgSize used for transmission >= minMsgPower")
        ("msgPowerStep", po::value<double>()->default_value(constants::kDefaultMsgPowerStep), "Step for increasing MsgPower between minMsgPower & maxMsgPower > 0")
        ("maxPower", po::value<unsigned int>()->default_value(constants::kDefaultTransmitMaxPower), "2^(maxPower) data will be transmitted for each msgSize >= maxMsgPower")
        ("subs", po::value<unsigned int>()->default_value(1), "Number of subscriber processes >= 1");

    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "main", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        // -------------------- SET UP LOGGING FILTER -------------------- 
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );

        LOG_APP(debug, "main", "logLevel = " << vm["logLevel"].as<unsigned int>());
    }

    if(vm["epochs"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "main", "Invalid iterations for taking the mean for each step.Try running the script with --epochs <number >= 1>");
        return 0;
    }
    else
    {
        epochs = vm["epochs"].as<unsigned int>();
        LOG_APP(debug, "main", "epochs = " << vm["epochs"].as<unsigned int>());
    }

    if(vm["minMsgPower"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "main", "Minimum MsgSize must be at least 1 Byte. Try running the script with --minMsgPower <number >= 1>");
        return 0;
    }
    else
    {
        minMsgPower = vm["minMsgPower"].as<unsigned int>();
        LOG_APP(debug, "main", "minMsgPower = " << vm["minMsgPower"].as<unsigned int>());
    }

    if(vm["maxMsgPower"].as<unsigned int>() < minMsgPower)
    {
        LOG_APP(error, "main", "Maximum MsgSize must be at least minMsgPower. Try running the script with --maxMsgPower <number >= minMsgPower>");
        return 0;
    }
    else
    {
        maxMsgPower = vm["maxMsgPower"].as<unsigned int>();
        LOG_APP(debug, "main", "maxMsgPower = " << vm["maxMsgPower"].as<unsigned int>());
    }

    if(vm["msgPowerStep"].as<double>() < 0)
    {
        LOG_APP(error, "main", "Step must be greater than zero to increase. Try running the script with --msgPowerStep <number >= 0>");
        return 0;
    }
    else
    {
        msgPowerStep = vm["msgPowerStep"].as<double>();
        LOG_APP(debug, "main", "msgPowerStep = " << vm["msgPowerStep"].as<double>());
    }

    if(vm["maxPower"].as<unsigned int>() < maxMsgPower)
    {
        LOG_APP(error, "main", "maxPower needs to be at least maxMsgPower. Try running the script with --maxPower <number >= maxMsgPower>");
        return 0;
    }
    else
    {
        maxPower = vm["maxPower"].as<unsigned int>();
        LOG_APP(debug, "main", "maxPower = " << vm["maxPower"].as<unsigned int>());
    }

    if(vm["subs"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "main", "Invalid number of Subscribers. Try again with --subs <number >= 1>");
        return 0;
    }
    else
    {
        subs = vm["subs"].as<unsigned int>();
        LOG_APP(debug, "main", "subs = " << vm["subs"].as<unsigned int>());
    }

    #pragma endregion   

    #pragma region MEASUREMENT SETUP

    // Initialize Variables;     
    utils::bench::sync::IPC_Barrier barrier(subs + 1, boost::uuids::to_string(boost::uuids::random_generator()()));
    uint64_t dataSize = 0, itersPerTransmit = 0, startTime = 0, endTime = 0;
    std::string shmUid = boost::uuids::to_string(boost::uuids::random_generator()());
    char* data;
    vector<double> msgSizes, writeTimes, readTimes;
    const int FULL_TRANSMIT_SIZE = pow(2, maxPower);  

    // Setup Measurement Object
    utils::bench::MeasurementDetails details;
    details.logLevel = logLevel;
    details.maxMsgSizePower = maxPower;
    details.type = utils::bench::BASE_CAP;
    details.epochs = epochs;
    utils::bench::Measurement w_meas("BaseCapBandwidthBench", details); 
    utils::bench::Measurement r_meas("BaseCapBandwidthBench", details); 

    #pragma endregion

    #pragma region WRITE BENCHMARK

    LOG_APP(info, "main", "Starting Write Benchmark...");

    for(double i = minMsgPower; i < maxMsgPower; i+=msgPowerStep)
    {
        // Roll new SHM name
        shmUid = boost::uuids::to_string(boost::uuids::random_generator()());

        // Compute Size of a message
        dataSize = pow(2, i);

        // Generate Shared Memory Region
        //auto shm = memory::SharedMemoryBlock<char>::create(shmUid, dataSize);

        // Generate Data
        LOG_APP(info, "main", "Generating Data...");
        std::string data = utils::random::random_string(dataSize);

        // Compute number of writes
        itersPerTransmit = ceil(FULL_TRANSMIT_SIZE / dataSize);  

        pubsub::ShindaClient<char> client(shmUid, dataSize);
        if(client.connect() != status::StatusCode::SUCCESS)
            exit(1);
        client.try_publish(data.c_str(), dataSize);

        // Log Progress
        LOG_APP(info, "main", "_______________________________________");
        LOG_APP(info, "main", "Starting DataSize " << dataSize << " Byte...");
        LOG_APP(info, "main", "TransmitSize = " << FULL_TRANSMIT_SIZE << "Byte");
        LOG_APP(info, "main", "Iterations = " << itersPerTransmit);

        for(unsigned int mi = 0; mi < epochs; mi++)
        {
            startTime = utils::timing::steady_microseconds();

            //for(unsigned int i = 0; i < itersPerTransmit; i++)
            //    memcpy(shm->getMemory(), data.c_str(), dataSize);
            for(unsigned int i = 0; i < itersPerTransmit; i++)
                client.publish(data.c_str(), dataSize);

            endTime = utils::timing::steady_microseconds();

            writeTimes.push_back((endTime - startTime)*1e-6);
            msgSizes.push_back(dataSize);
        }   

        // Cleanup
        //shm->releaseShm();
    }

    #pragma endregion

    #pragma region READ BENCHMARK 
    
    LOG_APP(info, "main", "Starting Read Benchmark...");

    for(double i = minMsgPower; i < maxMsgPower; i+=msgPowerStep)
    {
        // Roll new SHM name
        shmUid = boost::uuids::to_string(boost::uuids::random_generator()());

        // Compute Size of a message
        dataSize = pow(2, i);

        // Generate Shared Memory Region
        auto shm = memory::SharedMemoryBlock<void>::create(shmUid, dataSize);

        // Generate Data
        LOG_APP(info, "main", "Generating Data...");
        data = static_cast<char*>(malloc(dataSize));

        // Compute number of writes
        itersPerTransmit = ceil(FULL_TRANSMIT_SIZE / dataSize);  

        // Log Progress
        LOG_APP(info, "main", "_______________________________________");
        LOG_APP(info, "main", "Starting DataSize " << dataSize << " Byte...");
        LOG_APP(info, "main", "TransmitSize = " << FULL_TRANSMIT_SIZE << "Byte");
        LOG_APP(info, "main", "Iterations = " << itersPerTransmit);

        for(unsigned int mi = 0; mi < epochs; mi++)
        {
            startTime = utils::timing::steady_microseconds();

            for(unsigned int i = 0; i < itersPerTransmit; i++)
                memcpy(data, shm->getMemory(), dataSize);

            endTime = utils::timing::steady_microseconds();

            readTimes.push_back((endTime - startTime)*1e-6);
        }

        // Cleanup
        shm->releaseShm();
    }

    #pragma endregion

    // Save measurement data to csv
    w_meas.csvWriter.filename() = w_meas.pubPath + "publisher";
    r_meas.csvWriter.filename() = r_meas.subPath + "subscriber_" + std::to_string(getpid());
    w_meas.csvWriter.addColumn<double>(msgSizes, "MsgSize", "Byte");
    w_meas.csvWriter.addColumn<double>(writeTimes, "WriteTime", "s");
    r_meas.csvWriter.addColumn<double>(readTimes, "ReadTime", "s");
    w_meas.writeMeasurement();
    r_meas.writeMeasurement();

    return 0;
}


