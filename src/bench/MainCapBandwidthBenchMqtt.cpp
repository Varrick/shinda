// StdLib Imports
#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <vector>
#include <mutex>
#include <condition_variable>
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include "mosquitto.h"
// Local Imports
#include "utils/logging.hpp"
#include "utils/benchmarking.hpp"
#include "utils/timing.hpp"
#include "macros.hpp"
#include "utils/random.hpp"

using namespace std;
using namespace shinda;
namespace po = boost::program_options;

// Constants
#define TOPIC                       "EXAMPLE_TOPIC"
#define PUB_ID                      "Client_PUB"
#define SUB_ID                      "Client_SUB"
#define HOST                        "localhost"
#define PORT                        1883
#define KEEPALIVE                   60
#define TIMEOUT                     -1
#define DEADLINE                    1000
#define MAX_PACKETS                 1

// Initialize Variables
static uint64_t msgSize, itersPerTransmit, startTime, stopTime, msgCount, timeoutCount;
static vector<double> msgSizes, writeTimes, readTimes, startTimes, endTimes, msgsPublished, msgsReceived, subTimeoutTimes, pubTimeoutTimes;;
static bool run = true;

void printProgress(const std::string& name)
{
    int progress = static_cast<int>(static_cast<double>(msgCount) / static_cast<double>(itersPerTransmit) * 100.0);
    LOG_APP(info, name, "Progress: " << progress << "% | MsgsProcessed: " << msgCount);
}

void pubConnectCb(struct mosquitto *mosq, void *obj, int rc)
{
	if(rc != 0){
        LOG_APP(info, "Publisher", "Could not connect to broker! Error Code: " << rc);
        mosquitto_destroy(mosq);
        throw;
    }
    LOG_APP(info, "Publisher", "Successfully connected to broker!");
}
void pubDisconnectCb(struct mosquitto *mosq, void *obj, int rc)
{
    run = false;
    if(rc != MOSQ_ERR_SUCCESS){
        LOG_APP(info, "Publisher", "Could not disconnect from broker! Error Code: " << rc);
        mosquitto_destroy(mosq);
        throw;
    }
    LOG_APP(info, "Publisher", "Successfully disconnected from broker!");
}
void publishCb(struct mosquitto *mosq, void *obj, int mid)
{
    if(msgCount == 0) {
        startTime = utils::timing::steady_microseconds();
    }
	msgCount++;
    if(msgCount % static_cast<int>(itersPerTransmit / 4) == 0) {
        printProgress("Publisher");
    }
	if(msgCount == itersPerTransmit) {
        // Log statistics
        stopTime = utils::timing::steady_microseconds();
        msgsPublished.push_back(msgCount);
        startTimes.push_back(startTime*1e-6);
        writeTimes.push_back((stopTime - startTime)*1e-6);
        pubTimeoutTimes.push_back(0);
        msgSizes.push_back(msgSize);
        // Finish Iteration
		mosquitto_disconnect(mosq);
	}
}

void subConnectCb(struct mosquitto *mosq, void *obj, int rc)
{
	if(rc != 0){
        LOG_APP(info, "Subscriber", "Could not connect to broker! Error Code: " << rc);
        mosquitto_destroy(mosq);
        throw;
    }
    LOG_APP(info, "Subscriber", "Successfully connected to broker!");
}
void subDisconnectCb(struct mosquitto *mosq, void *obj, int rc)
{
    run = false;
    if(rc != MOSQ_ERR_SUCCESS){
        LOG_APP(info, "Subscriber", "Could not disconnect from broker! Error Code: " << rc);
        mosquitto_destroy(mosq);
        throw;
    }
    LOG_APP(info, "Subscriber", "Successfully disconnected from broker!");
}
void receivedCb(struct mosquitto *mosq, void *obj, const struct mosquitto_message *msg)
{	
    msgCount++;
    if(msgCount % static_cast<int>(itersPerTransmit / 4) == 0) {
        printProgress("Subscriber");
    }
    if(msgCount == itersPerTransmit){
        // Log statistics
        stopTime = utils::timing::steady_microseconds();
        msgsReceived.push_back(msgCount);
        endTimes.push_back(stopTime*1e-6);
        subTimeoutTimes.push_back(0);
        readTimes.push_back((stopTime - startTime)*1e-6);
        // Finish Iteration
		mosquitto_disconnect(mosq);
	}
}

int main(int ac, char* av[]) 
{
    #pragma region SET CUSTOM LOGGING FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&utils::logging::coloring_formatter);
    #pragma endregion 

    #pragma region APPLY COMMAND LINE ARGS
    unsigned int minMsgPower, maxMsgPower, maxPower, msgPowerStep, subs, epochs, logLevel, qos; 

    po::options_description desc("Allowed Options");
    desc.add_options()
        ("logLevel", po::value<unsigned int>()->default_value(0), "loggingLevel 0 - 5")
        ("itersPerMsgSize", po::value<unsigned int>()->default_value(1), "take mean of itersPerMsgSize epochs for each msgSize")
        ("subs", po::value<unsigned int>()->default_value(1), "Number of subscriber processes >= 1")
        ("epochs", po::value<unsigned int>()->default_value(1), "take mean of epochs for each msgSize")
        ("qos", po::value<unsigned int>()->default_value(1), "MQTT Quality of Service <0/1/2>")
        ("minMsgPower", po::value<unsigned int>()->default_value(constants::kDefaultMinMsgPower), "2^(minMsgPower) will be smallest msgSize used for transmission >= 0")
        ("maxMsgPower", po::value<unsigned int>()->default_value(constants::kDefaultMaxMsgPower), "2^(maxMsgPower) will be largest msgSize used for transmission >= minMsgPower")
        ("msgPowerStep", po::value<double>()->default_value(constants::kDefaultMsgPowerStep), "Step for increasing MsgPower between minMsgPower & maxMsgPower > 0")
        ("maxPower", po::value<unsigned int>()->default_value(constants::kDefaultTransmitMaxPower), "2^(maxPower) data will be transmitted for each msgSize >= maxMsgPower");

    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "main", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        // -------------------- SET UP LOGGING FILTER -------------------- 
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );

        LOG_APP(debug, "main", "logLevel = " << vm["logLevel"].as<unsigned int>());
    }

    if(vm["subs"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "main", "Invalid number of Subscribers. Try again with --subs <number >= 1>");
        return 0;
    }
    else
    {
        subs = vm["subs"].as<unsigned int>();
        LOG_APP(debug, "main", "subs = " << vm["subs"].as<unsigned int>());
    }

    if(vm["epochs"].as<unsigned int>() < 1)
    {
        LOG_APP(error, "main", "Invalid iterations for taking the mean for each step.Try running the script with --epochs <number >= 1>");
        return 0;
    }
    else
    {
        epochs = vm["epochs"].as<unsigned int>();
        LOG_APP(debug, "main", "epochs = " << vm["epochs"].as<unsigned int>());
    }

    if(vm["minMsgPower"].as<unsigned int>() < 0)
    {
        LOG_APP(error, "main", "Minimum MsgSize must be at least 1 Byte. Try running the script with --minMsgPower <number >= 1>");
        return 0;
    }
    else
    {
        minMsgPower = vm["minMsgPower"].as<unsigned int>();
        LOG_APP(debug, "main", "minMsgPower = " << vm["minMsgPower"].as<unsigned int>());
    }

    if(vm["maxMsgPower"].as<unsigned int>() < minMsgPower)
    {
        LOG_APP(error, "main", "Maximum MsgSize must be at least minMsgPower. Try running the script with --maxMsgPower <number >= minMsgPower>");
        return 0;
    }
    else
    {
        maxMsgPower = vm["maxMsgPower"].as<unsigned int>();
        LOG_APP(debug, "main", "maxMsgPower = " << vm["maxMsgPower"].as<unsigned int>());
    }

    if(vm["msgPowerStep"].as<double>() < 0)
    {
        LOG_APP(error, "main", "Step must be greater than zero to increase. Try running the script with --msgPowerStep <number >= 0>");
        return 0;
    }
    else
    {
        msgPowerStep = vm["msgPowerStep"].as<double>();
        LOG_APP(debug, "main", "msgPowerStep = " << vm["msgPowerStep"].as<double>());
    }

    if(vm["maxPower"].as<unsigned int>() < maxMsgPower)
    {
        LOG_APP(error, "main", "maxPower needs to be at least maxMsgPower. Try running the script with --maxPower <number >= maxMsgPower>");
        return 0;
    }
    else
    {
        maxPower = vm["maxPower"].as<unsigned int>();
        LOG_APP(debug, "main", "maxPower = " << vm["maxPower"].as<unsigned int>());
    }

    if(vm["qos"].as<unsigned int>() > 2)
    {
        LOG_APP(error, "main", "Invalid Quality of Service. Try again with --qos <0/1/2>");
        return 0;
    }
    else
    {
        qos = vm["qos"].as<unsigned int>();
        LOG_APP(debug, "main", "QOS = " << vm["qos"].as<unsigned int>());
    }

    #pragma endregion   

    #pragma region MEASUREMENT SETUP
 
    utils::bench::sync::IPC_Barrier barrier(subs + 1, boost::uuids::to_string(boost::uuids::random_generator()()));
    struct mosquitto *mosq;
    int rc, currMsg = 0;
    const int FULL_TRANSMIT_SIZE = pow(2, maxPower);
    char* dataPub;

    // Setup Measurement Object
    utils::bench::MeasurementDetails details;
    details.numSubs = subs;
    details.logLevel = logLevel;
    details.maxMsgSizePower = maxPower;
    details.epochs = epochs;
    details.type = utils::bench::CAP;
    utils::bench::Measurement meas("CapBandwidthBenchMqtt", details); 

    int publisherStatus;
    vector<int> subscriberStatus;
    pid_t publisherPid;
    vector<pid_t> subscriberPids;

    #pragma endregion

    // Fork Publisher
    if ((publisherPid = fork()) == -1) {
        exit(1);
    }

    if (publisherPid == 0) 
    {   
        // Start Mosquitto Client
        mosquitto_lib_init();

        for(int i = minMsgPower; i < maxMsgPower; i+=msgPowerStep)
        {
            // Compute Iteration Prerequisites
            msgSize = pow(2, i);
            itersPerTransmit = ceil(FULL_TRANSMIT_SIZE / msgSize);  

            // Generate Data
            LOG_APP(info, "Publisher", "Generating Data...");
            std::string data = utils::random::random_string(msgSize).c_str();

            // Signal Subscriber that initialization finished
            LOG_APP(info, "Publisher", "-------- Ready for MsgSize: " << msgSize << " --------");
            barrier.wait();

            for(unsigned int mi = 0; mi < epochs; mi++)
            {               
                // Connect to Broker
                mosq = mosquitto_new(PUB_ID, true, NULL);
                mosquitto_connect_callback_set(mosq, pubConnectCb);
                mosquitto_disconnect_callback_set(mosq, pubDisconnectCb);
                mosquitto_publish_callback_set(mosq, publishCb);
                mosquitto_connect(mosq, HOST, PORT, KEEPALIVE);

                // Signal Subscriber that initialization finished
                LOG_APP(info, "Publisher", "-------- Ready for Epoch #" << mi+1 << " --------");
                barrier.wait();

                // Reset Variables
                msgCount = startTime = stopTime = 0, currMsg = 0, run = true;

                // Publish Data
                while(!mosquitto_loop(mosq, TIMEOUT, MAX_PACKETS) && run) {
                    if(currMsg < itersPerTransmit) {
                        mosquitto_publish(mosq, NULL, TOPIC, msgSize, data.c_str(), qos, false);
                        currMsg++;
                    }
                }

                // Cleanup
                mosquitto_destroy(mosq);
            }
        }

        // Prepare Measurement for writing to file
        LOG_APP(info, "Publisher", "It's writing time!");
        meas.csvWriter.filename() = meas.pubPath + "publisher";
        meas.csvWriter.addColumn<double>(msgSizes, "MsgSize", "Byte");
        meas.csvWriter.addColumn<double>(writeTimes, "WriteTime", "s");
        meas.csvWriter.addColumn<double>(startTimes, "StartTimes", "s");
        meas.csvWriter.addColumn<double>(pubTimeoutTimes, "PubTimeoutTimes", "s");
        meas.csvWriter.addColumn<double>(msgsPublished, "MsgsPublished", "");

        // Write Measurement to file
        meas.writeMeasurement();

        // Cleanup
        mosquitto_lib_cleanup();

        return 0;
    }
    else
    {
        // Fork Subscribers
        for (int i = 0; i < subs; i++) {
            pid_t childPid = fork();
            subscriberPids.push_back(childPid);
            if (childPid == -1) {
                exit(1);
            }

            if (childPid == 0) 
            {
                // Start Mosquitto Client
                mosquitto_lib_init();

                utils::timing::DeadlineTimer timer(std::chrono::milliseconds(DEADLINE), [&](){
                    printProgress("Subscriber");
                    LOG_APP(info, "Subscriber", "Timeout happened.");
                    stopTime = utils::timing::steady_microseconds();
                    msgsReceived.push_back(msgCount);
                    endTimes.push_back(stopTime*1e-6);
                    subTimeoutTimes.push_back(DEADLINE*1e-3);
                    readTimes.push_back((stopTime - startTime)*1e-6);
                    mosquitto_disconnect(mosq);
                });

                for(int i = minMsgPower; i < maxMsgPower; i+=msgPowerStep)
                {
                    // Compute Iteration Prerequisites
                    msgSize = pow(2, i);
                    itersPerTransmit = ceil(FULL_TRANSMIT_SIZE / msgSize);   

                    // Signal Subscriber that initialization finished
                    LOG_APP(info, "Subscriber", "-------- Ready for MsgSize: " << msgSize << " --------");
                    barrier.wait();  

                    for(unsigned int mi = 0; mi < epochs; mi++)
                    {
                        // Connect to Broker
                        mosq = mosquitto_new((SUB_ID + std::to_string(getpid())).c_str(), true, NULL);
                        mosquitto_connect_callback_set(mosq, subConnectCb);
                        mosquitto_disconnect_callback_set(mosq, subDisconnectCb);
                        mosquitto_message_callback_set(mosq, receivedCb);
                        mosquitto_connect(mosq, HOST, PORT, KEEPALIVE);
                        mosquitto_subscribe(mosq, NULL, TOPIC, qos); 

                        // Signal Subscriber that initialization finished
                        LOG_APP(info, "Subscriber", "-------- Ready for Epoch # " << mi+1 << " --------");
                        barrier.wait();  
         
                        // Reset Variables
                        msgCount = startTime = stopTime = 0, run = true, rc = MOSQ_ERR_SUCCESS;

                        timer.Start();

                        // Measure when we started receiving
                        startTime = utils::timing::steady_microseconds();

                        do{
                            rc = mosquitto_loop(mosq, TIMEOUT, MAX_PACKETS);
                            timer.Reset();
                        } while(rc == MOSQ_ERR_SUCCESS && run);

                        timer.Stop();

                        // Cleanup
                        mosquitto_destroy(mosq);
                    }
                }

                // Prepare Measurement for writing to file
                meas.csvWriter.filename() = meas.subPath + "subscriber_" + std::to_string(getpid());
                meas.csvWriter.addColumn<double>(readTimes, "ReadTime", "s");
                meas.csvWriter.addColumn<double>(endTimes, "EndTimes", "s");    
                meas.csvWriter.addColumn<double>(subTimeoutTimes, "SubTimeoutTimes", "s");
                meas.csvWriter.addColumn<double>(msgsReceived, "MsgsReceived", "");

                // Write Measurement to file
                meas.writeMeasurement();

                // Cleanup
                mosquitto_lib_cleanup(); 

                return 0;
            }
        }

        // Parent Code
        waitpid(publisherPid, &publisherStatus, 0);
        printf("Publisher #%d finished.\n", publisherPid);

        for(int i = 0; i < subscriberPids.size(); i++) {
            waitpid(subscriberPids[i], &(subscriberStatus[i]), 0);
            printf("Subscriber #%d finished.\n", subscriberPids[i]);
        }

        barrier.release();
    }

    return 0;
}