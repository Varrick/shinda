#include "memory/SharedRingBufferMember.hpp"

namespace shinda::memory
{
    SharedRingBufferMember::SharedRingBufferMember() {}
    SharedRingBufferMember::SharedRingBufferMember(const int position, const MemberType type) 
        : position_(position), type_(type) {} 

    auto SharedRingBufferMember::wait() -> void
    { 
        //LOG_DEBUG(info, "SharedRingBufferMember", "Start waiting (until notification, " << ((!type_) ? "Consumer" : "Producer") << ")...");
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(mtx_);
        while(!dataRdy_) {
            cnd_.wait(lock);
        }
        dataRdy_ = false;
        LOG_DEBUG(info, "SharedRingBufferMember", "Woken up...");
    }
    auto SharedRingBufferMember::timed_wait(const int timeoutMicros) -> bool
    { 
        LOG_DEBUG(info, "SharedRingBufferMember", "Start waiting (" << timeoutMicros*1e-6 << "s)...");
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(mtx_);
        
        if(cnd_.timed_wait(lock, boost::get_system_time() + boost::posix_time::microseconds(timeoutMicros)))
        {
            LOG_DEBUG(info, "SharedRingBufferMember", "Woken up...");
            dataRdy_ = false;
            return true;
        }
        else
        {
            LOG_DEBUG(info, "SharedRingBufferMember", "Timeout reached...");
            dataRdy_ = false;
            return false;
        }    
    }
    auto SharedRingBufferMember::notify() -> void
    {
        boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(mtx_);
        dataRdy_ = true;
        cnd_.notify_all();
        //LOG_DEBUG(info, "SharedRingBufferMember", "Notified (" << ((!type_) ? "Consumer" : "Producer") << ")");
    }
    auto SharedRingBufferMember::position() -> std::atomic<int>&
    { 
        return position_; 
    }
    auto SharedRingBufferMember::isWaiting() const -> bool
    {
        LOG_DEBUG(info, "SharedRingBufferMember", "Waiting? = " << !dataRdy_);
        return !dataRdy_;
    }

    inline auto operator<<(std::ostream& os, const SharedRingBufferMember& shm_mem) -> std::ostream&
    {
        os << std::string("-----SharedRingBufferMember-----") +
                        "\nPosition = " + std::to_string(shm_mem.position_) + "Byte" + " | (TypeSize = " + std::to_string(sizeof(shm_mem.position_)) + "Byte" + ")" + 
                        "\nWaiting? = " + ((!shm_mem.dataRdy_) ? "True" : "False") + " | (TypeSize = " + std::to_string(sizeof(shm_mem.dataRdy_)) + "Byte" + ")"
                        "\nMemberType = " + ((!static_cast<int>(shm_mem.type_)) ? "Consumer" : "Producer") + " | (TypeSize = " + std::to_string(sizeof(shm_mem.type_)) + "Byte" + ")";
        return os;
    }
}
