#include "memory/SharedRingBufferWrapper.hpp"

namespace shinda::memory
{
    SharedRingBufferWrapper::SharedRingBufferWrapper(const std::string shmUid, 
                                                     const size_t maxMsgSize, 
                                                     const size_t numSegments)
    {   
        // Create SharedMemory
        LOG_APP(warning, "SharedRingBuffer", "MaxMsgSize: " << SharedRingBuffer::estimateSize(maxMsgSize, numSegments));
        mem_ = SharedMemoryBlock<SharedRingBuffer>::create(shmUid, SharedRingBuffer::estimateSize(maxMsgSize, numSegments));

        // Update RingBuffer data in shm to current data structure       
        mem_->write(SharedRingBuffer::create(maxMsgSize, numSegments).get());

        // Get parameters from created shared memory region
        mem_->truncate(mem_->getMemory()->size);    
    
        // Get Reference to Shared Memory
        buf_ = mem_->getMemory();

        // Precalculate Addresses of data in ringbuffer
        msgsAddrLut_.clear();
        for(int i = 0; i < buf_->numSegments; i++)
            msgsAddrLut_.push_back(buf_->getMsgAddrAt(i));        
    }  
    
    SharedRingBufferWrapper::SharedRingBufferWrapper(const std::string shmUid)
    { 
        // Create SharedMemory
        mem_ = SharedMemoryBlock<SharedRingBuffer>::join(shmUid, sizeof(struct SharedRingBuffer));

        // Reshape to correct size set by Broker
        mem_->truncate(mem_->getMemory()->size);      

        // Get Reference to Shared Memory
        buf_ = mem_->getMemory();

        // Precalculate Addresses of data in ringbuffer
        msgsAddrLut_.clear();
        for(int i = 0; i < buf_->numSegments; i++)
            msgsAddrLut_.push_back(buf_->getMsgAddrAt(i));     
    }  

    auto SharedRingBufferWrapper::create(const std::string shmUid, 
                                         size_t const maxMsgSize, 
                                         const size_t numSegments)  -> std::unique_ptr<SharedRingBufferWrapper>
    {
        LOG_APP(warning, "SharedRingBuffer", "MaxMsgSize: " << maxMsgSize);
        LOG_APP(warning, "SharedRingBuffer", "NumSegments: " << numSegments);
        return std::unique_ptr<SharedRingBufferWrapper>(new SharedRingBufferWrapper(shmUid, maxMsgSize, numSegments));
    }
    
    auto SharedRingBufferWrapper::join(const std::string shmUid)    -> std::unique_ptr<SharedRingBufferWrapper>
    {
        return std::unique_ptr<SharedRingBufferWrapper>(new SharedRingBufferWrapper(shmUid));
    }
    

    // ------------------ Process Management -----------------------
    auto SharedRingBufferWrapper::close()                      -> void {
        mem_->closeShm();     
    }
    auto SharedRingBufferWrapper::release()                    -> void {
        mem_->releaseShm();
    }
    auto SharedRingBufferWrapper::createConsumer(const int id) -> void {
        buf_->consumers[id].position() = buf_->intToPos(buf_->producer.position() - 1);
        updateTail(buf_->producer.position());
        //LOG_DEBUG(info, "SharedRingBuffer", "Subscriber with ID #"  << id 
        //                                                            << " activated on Pos: " 
        //                                                            << buf_->consumers[id.position()]);
    }
    auto SharedRingBufferWrapper::removeConsumer(const int id) -> void {
        buf_->consumers[id].position() = -1;
        updateTail(buf_->producer.position());
    }
    auto SharedRingBufferWrapper::joinProducer(const int id)   -> void { 
        pubId_ = id;
        currHead_ = buf_->producer.position();
    };
    auto SharedRingBufferWrapper::joinConsumer(const int id)   -> void { 
        subId_ = id; 
        currTail_ = buf_->consumers[subId_].position();   
    };
    auto SharedRingBufferWrapper::unregisterProducer()         -> void { 
        pubId_ = -1;
    };
    auto SharedRingBufferWrapper::unregisterConsumer()         -> void { 
        subId_ = -1;  
    };

 
}   // namespace shinda::memory
