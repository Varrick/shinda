#include "memory/SharedRingBuffer.hpp"

namespace shinda::memory
{
    SharedRingBuffer::SharedRingBuffer() {}

    auto SharedRingBuffer::create(const size_t maxMsgSize, const int numSegments) -> std::unique_ptr<SharedRingBuffer>
    {
        auto bufSize = estimateSize(maxMsgSize, numSegments);
        SharedRingBuffer* ringBuf = new SharedRingBuffer();
        ringBuf = (struct SharedRingBuffer*)(malloc(bufSize));
        ringBuf->maxMsgSize = maxMsgSize;
        ringBuf->numSegments = numSegments;
        ringBuf->size = bufSize;
        ringBuf->sizeMask = numSegments - 1;
        ringBuf->lastTail = -1;

        memcpy(&(ringBuf->producer), new SharedRingBufferMember(0, MemberType::PRODUCER), sizeof(SharedRingBufferMember));
        for(int i = 0; i < constants::kMaxSubscribersPerTopic; i++) 
            memcpy(&(ringBuf->consumers[i]), new SharedRingBufferMember(-1, MemberType::CONSUMER), sizeof(SharedRingBufferMember));

        return std::unique_ptr<SharedRingBuffer>(ringBuf);
    }
    auto SharedRingBuffer::estimateSize(const size_t maxMsgSize, const int numSegments) -> size_t
    {
        return sizeof(struct shinda::memory::SharedRingBuffer) + maxMsgSize * numSegments;
    }  

    // ---------------------- Math/Helpers ---------------------------
    auto SharedRingBuffer::intToPos(const int val)      const -> int
    {
        return val & sizeMask;
    }
    auto SharedRingBuffer::getNextPos(const int pos)    const -> int
    {
        return (pos + 1) & sizeMask;
    }
    auto SharedRingBuffer::getMsgAddrAt(const int idx)  const -> char* {
    return (char*)(rawMsgs + (idx * maxMsgSize));
}
    auto SharedRingBuffer::getMsgAt(const int idx)      const -> SharedMsgHeader* {
    return (SharedMsgHeader*)(rawMsgs + (idx * maxMsgSize));
}

  inline auto operator<<(std::ostream& os, const SharedRingBuffer& buf) -> std::ostream&
  {
    os <<  std::string("-----SharedRingBufferHeader-----") +
              "\nSize = " + std::to_string(buf.size) + "Byte" + 
              "\nNumElements = " + std::to_string(buf.numSegments) + 
              "\nSizeMask = " + std::to_string(buf.sizeMask) + 
              "\nMaxMsgSize = " + std::to_string(buf.maxMsgSize) + 
              "\n-----------------------------\n";
    return os;
  }

} // namepsace shinda::memory




