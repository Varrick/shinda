#include "memory/SharedMessage.hpp"

namespace shinda::memory
{
    SharedMsgHeader::SharedMsgHeader() {}
    SharedMsgHeader::SharedMsgHeader(const size_t msgSize)
        : msgSize(msgSize),   
            hdrSize(sizeof(struct SharedMsgHeader)), 
            dataSize(msgSize - sizeof(struct SharedMsgHeader)) {} 

    auto SharedMsgHeader::createEmpty() -> SharedMsgHeader {
        return SharedMsgHeader();
    }

    auto SharedMsgHeader::createFromDataSize(const size_t dataSize) -> SharedMsgHeader{
        return SharedMsgHeader(dataSize + sizeof(struct SharedMsgHeader));
    }

    auto SharedMsgHeader::createFromMsgSize(const size_t msgSize) -> SharedMsgHeader {
        return SharedMsgHeader(msgSize);
    }

    SharedMsg::SharedMsg() {}
    SharedMsg::SharedMsg(const SharedMsgHeader hdr) : hdr(hdr) { data = malloc(hdr.dataSize); }
    SharedMsg::SharedMsg(const SharedMsgHeader hdr, void *const data) : hdr(hdr), data(data) {}
    
    auto SharedMsg::createEmpty() -> SharedMsg {
        return SharedMsg();
    } 

    auto SharedMsg::createEmptyFromMsgSize(const size_t msgSize) -> SharedMsg{
        return SharedMsg(SharedMsgHeader::createFromMsgSize(msgSize));
    }
    auto SharedMsg::createEmptyFromDataSize(const size_t dataSize) -> SharedMsg{
        return SharedMsg(SharedMsgHeader::createFromDataSize(dataSize));
    }
    auto SharedMsg::createFromData(void *const data, const size_t dataSize) -> SharedMsg{
        return SharedMsg(SharedMsgHeader::createFromDataSize(dataSize), data);
    }

    auto SharedMsg::resize(size_t dataSize) -> void
    {
        data = malloc(dataSize);
        hdr = SharedMsgHeader::createFromDataSize(dataSize);
    }

    auto operator<<(std::ostream& os, const SharedMsg& msg) -> std::ostream&
    {
        os << std::string("-----SharedMsg-----") +
                        "\nMsgSize = " + std::to_string(msg.hdr.msgSize) + " Byte" + 
                        "\nHeaderSize = " + std::to_string(msg.hdr.hdrSize) + " Byte" + 
                        "\nDataSize = " + std::to_string(msg.hdr.dataSize) + " Byte" + 
                        "\n-----------------------------\n";
        return os;
    }

}   // namespace shinda::structs