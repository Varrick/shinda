#include "pubsub/Topic.hpp"

namespace shinda::pubsub
{      
    TopicBlueprint::TopicBlueprint(const std::string name, 
                                    const int numSegments, 
                                    const size_t maxDataSize, 
                                    const bool isEssential)
    : name_(name), numSegments_(numSegments), maxDataSize_(maxDataSize), 
        maxMsgSize_(memory::SharedMsgHeader::createFromDataSize(maxDataSize).msgSize), isEssential_(isEssential)
    {
        totalSize_ = memory::SharedRingBuffer::estimateSize(maxDataSize_, numSegments_);
    }

    auto TopicBlueprint::getName()              const   -> const std::string { return name_; }
    auto TopicBlueprint::getNumSegments()       const   -> const int { return numSegments_; }
    auto TopicBlueprint::getMaxDataSize()       const   -> const size_t { return maxDataSize_; }
    auto TopicBlueprint::getMaxMsgSize()        const   -> const size_t { return maxMsgSize_; }
    auto TopicBlueprint::getTotalSize()         const   -> const size_t { return totalSize_; }
    auto TopicBlueprint::getIsEssential()       const   -> const bool { return isEssential_; }


    Topic::Topic(const std::string topic, const size_t maxDataSize, const int numSegments, 
                 const int maxNumSubs, const pid_t creator, const bool isEssential)
        : TopicBlueprint(topic, numSegments, maxDataSize, isEssential), creator_(creator), maxNumSubs_(maxNumSubs),
          subIdManager_(utils::IdManager(0, maxNumSubs - 1)),
          pubIdManager_(utils::IdManager(maxNumSubs, maxNumSubs))
    {
        ringBuf_ = memory::SharedRingBufferWrapper::create(shmUid_, maxMsgSize_, numSegments_);   
    }

    auto Topic::addSubscriber(const int pid)                            -> bool
    {
        // Search, if any Subscriber with given pid already exists
        auto it = find_if(subscribers_.begin(), subscribers_.end(), [&pid](const ShindaUser& obj) { return obj.getPid() == pid; });

        // If it does exist, error
        if(it != subscribers_.end()) {
            LOG_APP(warning, "Topic", "Subscriber with PID #" << pid << " could not be added to Topic " << name_ << ". It does already exist.");
            return false;
        }
        // If it does not exist, create Subscriber by getting a unique id for it
        else
        {
            try {
                ShindaUser user(ShindaUser(name_, pid, subIdManager_.getId(), ShindaUserType::SUBSCRIBER));
                subscribers_.push_back(user);
                ringBuf_->createConsumer(user.getId());
                LOG_APP(info, "Topic", "Subscriber with PID #" << pid << " & ID #" << user.getId() << " was added to Topic " << name_ << ".");
                return true;
            }
            catch(const std::exception& e) { 
                LOG_APP(warning, "Topic", "Subscriber with PID #" << pid << " could not be added to Topic " << name_ << ". Exception: " << e.what());
                return false;
            }     
        }
    }
    auto Topic::removeSubscriber(const int pid)                         -> bool
    {
        // Search, if any Subscriber with given pid already exists
        auto it = find_if(subscribers_.begin(), subscribers_.end(), [&pid](const ShindaUser& obj) { return obj.getPid() == pid; });

        // If it does exist, remove it
        if(it != subscribers_.end())
        {
            try {
                ringBuf_->removeConsumer(it->getId());;
                subIdManager_.releaseId(it->getId());
                subscribers_.erase(it);
                LOG_APP(info, "Topic", "Subscriber with PID #" << pid << " was removed from Topic '" << name_ << "'.");
                return true;
            }
            catch(const std::exception& e) {
                LOG_APP(warning, "Topic", "Subscriber with PID #" << pid << " could not be removed from Topic '" << name_ << "'. Exception: " << e.what() << ".");
                return false;
            }        
        }
        // If it does not exist, error
        else {
            LOG_APP(trace, "Topic", "Subscriber with PID #" << pid << " could not be removed from Topic '" << name_ << "'. Subscriber does not exist.");
            return false;
        }
    }
    auto Topic::setPublisher(const int pid)                             -> bool
    {
        try {
            ShindaUser user (name_, pid, pubIdManager_.getId(), ShindaUserType::PUBLISHER);
            publisher_ = user;
            LOG_APP(info, "Topic", "Publisher with PID #" << pid << " set for Topic '" << name_ << "'.");
            return true;
        }
        catch(const std::exception& e) {
            LOG_APP(warning, "Topic", "Publisher with PID #" << pid << " could not be set for Topic '" << name_ << "'. Exception: " << e.what()); 
            return false;
        }     
    }
    auto Topic::setPublisher(const int pid, const size_t maxMsgSize)    -> bool
    {
        try {
            ShindaUser user (name_, pid, pubIdManager_.getId(), ShindaUserType::PUBLISHER);
            publisher_ = user;
            LOG_APP(info, "Topic", "Publisher with PID #" << pid << " set for Topic '" << name_ << "'.");
            return true;
        }
        catch(const std::exception& e) {
            LOG_APP(warning, "Topic", "Publisher with PID #" << pid << " could not be set for Topic '" << name_ << "'. Exception: " << e.what());
            return false;
        }     
    }
    auto Topic::removePublisher(const int pid)                          -> bool
    {
        if(publisher_.getPid() == pid) {
            try {
                pubIdManager_.releaseId(publisher_.getId());
                LOG_APP(info, "Topic", "Publisher with PID #" << pid << " was removed from Topic '" << name_ << "'.");
                return true;
            }
            catch(const std::exception& e) {
                LOG_APP(warning, "Topic", "Publisher with PID #" << pid << " could not be removed from Topic '" << name_ << "'. Exception: " << e.what() << ".");
                return false;
            }  
        } 
        else {
            LOG_APP(trace, "Topic", "Publisher with PID #" << pid << " could not be removed from Topic '" << name_ << "'. PID not found.");
            return false;
        }
    }
    auto Topic::release()                                               -> void
    { 
        try
        {
            ringBuf_->release();
            LOG_APP(info, "Topic", "Topic '" << name_ << "' was deleted.");
        }
        catch(const exception::ShmBlockError& e) {
            throw std::runtime_error("Topic '" + name_ + "' could not be deleted. Exception: " + e.what());
        }
    }

    auto Topic::getSubscriberByPid(const int pid)   const   -> const ShindaUser
    { 
        // Search, if any Subscriber with given pid already exists
        auto it = find_if(subscribers_.begin(), subscribers_.end(), [&pid](const ShindaUser& obj) { return obj.getPid() == pid; });

        // If it does exist, error
        if(it != subscribers_.end())
        {
            LOG_DEBUG(trace, "Topic", "Subscriber with PID #" << pid << " was found.");
            return *(it);
        }
        // If it does not exist, create Subscriber by getting a unique id for it
        else {
            throw("Subscriber with PID #" + std::to_string(pid) + " could not be found in Topic '" + name_ + "'.");
        }
    }
    auto Topic::getSubscriberById(const int id)     const   -> const ShindaUser 
    { 
        // Search, if any Subscriber with given pid already exists
        auto it = find_if(subscribers_.begin(), subscribers_.end(), [&id](const ShindaUser& obj) { return obj.getId() == id; });

        // If it does exist, error
        if(it != subscribers_.end())
        {
            LOG_DEBUG(trace, "Topic", "Subscriber with ID #" << id << " was found.");
            return *(it);
        }
        // If it does not exist, create Subscriber by getting a unique id for it
        else {
            throw("Subscriber with ID #" + std::to_string(id) + " could not be found in Topic '" + name_ + "'.");
        }
    }
    auto Topic::hasPublisher()      const   -> bool { return pubIdManager_.isAnyIdInUse(); }
    auto Topic::hasSubscribers()    const   -> bool { return subIdManager_.isAnyIdInUse(); }
    auto Topic::isFull()            const   -> bool { return (hasPublisher() && !subIdManager_.isAnyIdAvailable()); }
    auto Topic::isEmpty()           const   -> bool { return (!hasPublisher() && !hasSubscribers()); }
    auto Topic::logStatus()         const   -> void { LOG_APP(debug, "Topic", "Topic '" << name_ << "' has still #" << (hasPublisher() ? 1 : 0) << " Publisher & #" << getNumSubs() << " Subscribers."); }

    auto Topic::getPublisher()      const   -> const ShindaUser { return publisher_; }
    auto Topic::getCreator()        const   -> const pid_t { return creator_; }
    auto Topic::getShmUid()         const   -> const std::string { return shmUid_; }
    auto Topic::getNumSubs()        const   -> const int { return subscribers_.size(); }
    auto Topic::getMaxNumSubs()     const   -> const int { return maxNumSubs_; }

}   // namespace shinda::pubsub