#include "pubsub/ShindaUser.hpp"

namespace shinda::pubsub
{
    ShindaUser::ShindaUser() {}
    ShindaUser::ShindaUser(const std::string topic, const int pid, const int id, const ShindaUserType userType) 
        : pid_(pid), id_(id), userType_(userType) {}

    auto ShindaUser::getPid()        const   ->  int { return pid_; }
    auto ShindaUser::getId()         const   ->  int { return id_; }
    auto ShindaUser::getUserType()   const   ->  ShindaUserType { return userType_; }
}