#include "pubsub/ShindaBroker.hpp"

namespace shinda::pubsub 
{  
    ShindaBroker::ShindaBroker(const std::string configPath) 
    { 
        // Set Mode for all file system operations
        // Only OWNER & ITS GROUP can read/write files created by the Broker
        umask(constants::kDefaultUmaskPermissions);

        // Load Config File
        std::ifstream cfgStream(configPath.c_str());
        if(!cfgStream.is_open())
            throw std::runtime_error("Config file for Broker could not be found at specified location: '" + configPath + "'");
        utils::json::parser::RSJresource cfgJ (cfgStream);

        maxUsableMem_ = static_cast<unsigned int>(cfgJ["max_memory_usage"].as<double>());
        maxTopicMem_ = static_cast<unsigned int>(cfgJ["max_topic_size"].as<double>());
        secureMode_ = cfgJ["secure_mode"].as<bool>();

        LOG_APP(info, "Broker", "MaxUsableMem: " << maxUsableMem_ << " Byte");
        LOG_APP(info, "Broker", "MaxTopicMem: " << maxTopicMem_ << " Byte");
        LOG_APP(info, "Broker", "SecureMode: " << secureMode_);

        // Read allowed topics
        for(auto topicIt = cfgJ["topics"].as_array().begin(); topicIt != cfgJ["topics"].as_array().end(); ++topicIt) {
            topicsAllowed_.push_back(TopicBlueprint((*topicIt)["name"].as<std::string>(), 
                                                    (*topicIt)["numSegments"].as<int>(), 
                                                    (*topicIt)["maxDataSize"].as<int>(), 
                                                    (*topicIt)["essential"].as<bool>()));
        }
        
        // Evaluate memory requirements of allowed topics
        for(const auto& topic : topicsAllowed_) {
            if(topic.getTotalSize() > maxTopicMem_)
                throw std::runtime_error("Topic '" + topic.getName() + "' exceeds the allowed size of a Topic. Allowed: " + std::to_string(maxTopicMem_) + " | Prefered: " + std::to_string(topic.getTotalSize()));
            if(topic.getIsEssential())
                essentialTopicsMem_ += topic.getTotalSize();
        }
        if(essentialTopicsMem_ > maxUsableMem_)
            throw std::runtime_error("Sum of memory needed for essential Topics exceed limit of available shared memory. Available: " + std::to_string(maxUsableMem_) + " | Prefered: " + std::to_string(essentialTopicsMem_));
        else
            nonEssentialTopicsMem_ = maxUsableMem_ - essentialTopicsMem_;

        server_->registerCallbacks(this);
        server_->start();
    }
    ShindaBroker::~ShindaBroker() 
    {
        stop();
    }
    auto ShindaBroker::join() -> void
    {
        server_->join();
    }
    auto ShindaBroker::stop() -> void 
    {       
        for(auto const& topic : topics_) {
            topic->release();
        }       
        
        LOG_APP(info, "Broker", "Stopped."); 
    }
    auto ShindaBroker::receivedPlatformMessage(const int sessionId, const std::string && msg) -> void
    {
        if(msg.empty()) // Disconnect happened
            onDisconnect(sessionId);
        else            // Message Received
        {
            SocketMsgs::SocketMsg recMsg;
            recMsg.ParseFromString(msg);

            switch(recMsg.msg_case())
            {
                case SocketMsgs::SocketMsg::kJoinRequest:
                    LOG_DEBUG(info, "Broker", "Message '" << "JoinRequest" << "' received at Session #" << sessionId << " from PID #" << recMsg.joinrequest().pid() << ".");
                    onJoin(sessionId, recMsg.joinrequest());      
                    break;
                case SocketMsgs::SocketMsg::kUnregisterRequest:
                    LOG_DEBUG(info, "Broker", "Message '" << "UnregisterRequest" << "' received at Session #" << sessionId << " from PID #" << recMsg.unregisterrequest().pid() << ".");
                    onUnregister(sessionId, recMsg.unregisterrequest());
                    break;
                case SocketMsgs::SocketMsg::kConnectRequest:
                    LOG_DEBUG(info, "Broker", "Message '" << "Connect" << "' received at Session #" << sessionId << " from PID #" << recMsg.connectrequest().pid() << ".");
                    onConnect(sessionId, recMsg.connectrequest());
                    break;
                default:
                    LOG_DEBUG(info, "Broker", "Unknown Message received.");
                    break;
            }
        }
    }
    auto ShindaBroker::onConnect(const int sessionId, const SocketMsgs::ConnectRequest& recMsg) -> void
    { 
        SocketMsgs::SocketMsg wrapMsg;
        std::string serializedMsg;
        wrapMsg.mutable_connectreply()->set_pid(recMsg.pid());

        // Track new session
        sessionPIDMap_.insert(std::pair<unsigned int, unsigned int>(sessionId, recMsg.pid()));

        auto it = std::find_if(topics_.begin(), topics_.end(), [recMsg](const std::unique_ptr<Topic>& topic) -> bool {  return topic->getName() == recMsg.topicname(); });
        if(it != topics_.end())  // TOPIC ALREADY EXISTS
        {
            if((*it)->isFull()) {
                wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::TOPIC_FULL));
                LOG_APP(info, "Broker", "Topic '" << (*it)->getName() << "' could not be connected to. It already exists and is full.");
            }
            else if(recMsg.maxdatasize() != (*it)->getMaxDataSize()) {
                wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::MSG_SIZE_MISMATCH));
                LOG_APP(info, "Broker", "Topic '" << (*it)->getName() << "' could not be connected to. It already exists and has MaxDataSize=" << (*it)->getMaxDataSize() << " instead of proposed MaxDataSizeTopic=" << recMsg.maxdatasize() << ".");
            }
            else {
                wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::SUCCESS)); 
                wrapMsg.mutable_connectreply()->set_shmuid((*it)->getShmUid());  
                LOG_APP(info, "Broker", "Topic '" << (*it)->getName() << "' with MaxDataSize=" << (*it)->getMaxDataSize() << " & NumElems=" << (*it)->getNumSegments() << " was connected.");
            }  
        }
        else 
        {
            bool createTopic = false, isEssential = false;
            uint16_t numSegments;
            auto it = std::find_if(topicsAllowed_.begin(), topicsAllowed_.end(), [recMsg](const TopicBlueprint& tp) -> bool {  return tp.getName() == recMsg.topicname(); });
            if(it != topicsAllowed_.end())  // TOPIC BLUEPRINT EXISTS
            {
                if(it->getMaxDataSize() == recMsg.maxdatasize())
                {
                    // ESSENTIAL TOPIC
                    if(it->getIsEssential())
                    {
                        if(it->getTotalSize() <= essentialTopicsMem_)
                        {
                            createTopic = true;
                            isEssential = true;
                            numSegments = it->getNumSegments();
                        }
                        else
                        {
                            LOG_APP(info, "Broker", "Topic '" << it->getName() << " could not be created. Not enough memory."); 
                            wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::NOT_ENOUGH_MEMORY));
                        }
                    }
                    // NON ESSENTIAL TOPIC
                    else
                    {
                        if(it->getTotalSize() <= nonEssentialTopicsMem_)
                        {
                            createTopic = true;
                            isEssential = false;
                            numSegments = it->getNumSegments();
                        }
                        else
                        {
                            // TOPICS FITS IN AVAILABLE MEMORY
                            auto segments = utils::math::fitSegments(it->getMaxMsgSize(), it->getNumSegments(), nonEssentialTopicsMem_);
                            if(segments >= constants::kMinRingbufferSegments) {
                                createTopic = true;
                                isEssential = false;
                                numSegments = segments;
                            }
                            // TOPIC DOES NOT FIT IN AVAILABLE MEMORY
                            else
                            {
                                LOG_APP(info, "Broker", "Topic '"   << recMsg.topicname() 
                                                                    << " could not be created. Not enough memory."); 
                                wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::NOT_ENOUGH_MEMORY));
                            }
                        }
                    }
                }
                else
                {
                    LOG_APP(info, "Broker", "Topic '"   << it->getName() 
                                                        << "' could not be created. MaxDataSize does not fit to topic. MaxDataSizeClient=" 
                                                        << recMsg.maxdatasize()
                                                        << " vs MaxDataSizeTopic=" 
                                                        << it->getMaxDataSize()
                                                        << " was created."); 
                    wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::MSG_SIZE_MISMATCH));
                }
            }
            // TOPIC BLUEPRINT NOT FOUND
            else
            {
                // TOPIC NOT ALLOWED
                if(secureMode_)
                {
                    LOG_APP(info, "Broker", "Topic '"   << recMsg.topicname() 
                                                        << "' could not be created. Topic not allowed.");
                    wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::TOPIC_NOT_ALLOWED));
                }
                // UNSECURE MODE -> CREATE TOPIC ANYWAYS IF MEMORY AVAILABLE
                else
                {
                    TopicBlueprint tp(recMsg.topicname(), constants::kDefaultRingbufferSegments, recMsg.maxdatasize(), false);
                    if(tp.getTotalSize() <= nonEssentialTopicsMem_)
                    {
                        createTopic = true;
                        isEssential = false;
                        numSegments = tp.getNumSegments();
                    }
                    else
                    {
                        // TOPICS FITS IN AVAILABLE MEMORY
                        auto segments = utils::math::fitSegments(tp.getMaxMsgSize(), tp.getNumSegments(), nonEssentialTopicsMem_);
                        if(segments >= constants::kMinRingbufferSegments) {
                            createTopic = true;
                            isEssential = false;
                            numSegments = segments;
                        }
                        // TOPIC DOES NOT FIT IN AVAILABLE MEMORY
                        else
                        {
                            LOG_APP(info, "Broker", "Topic '" << recMsg.topicname() << " could not be created. Not enough memory."); 
                            wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::NOT_ENOUGH_MEMORY));
                        }
                    }
                }  
            }

            // TOPIC SHALL BE CREATED & PARAMETERS WERE COLLECTED
            if(createTopic) 
            {
                TopicBlueprint tp(recMsg.topicname(), numSegments, recMsg.maxdatasize(), isEssential);

                if(tp.getTotalSize() > maxTopicMem_)
                {
                    LOG_APP(info, "Broker", "Topic '" << recMsg.topicname() << " could not be created. Topic too large."); 
                    wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::TOPIC_SIZE_NOT_ALLOWED));
                }
                else
                {
                    if(isEssential)
                        essentialTopicsMem_ -= tp.getTotalSize();
                    else
                        nonEssentialTopicsMem_ -= tp.getTotalSize();

                    // Create new Topic
                    std::unique_ptr<Topic> topic;
                    topic.reset(new Topic(
                                tp.getName(),  
                                tp.getMaxDataSize(), 
                                tp.getNumSegments(),
                                constants::kMaxSubscribersPerTopic,
                                recMsg.pid(),
                                tp.getIsEssential())); 
                                
                    LOG_APP(info, "Broker", "Topic '" << topic->getName() << "' with MaxDataSize=" << topic->getMaxDataSize() << " & MaxMsgSize=" << topic->getMaxMsgSize() << " & NumSegments=" << topic->getNumSegments() << " was created."); 
                    wrapMsg.mutable_connectreply()->set_shmuid(topic->getShmUid()); 
                    topics_.push_back(std::move(topic));
                    wrapMsg.mutable_connectreply()->set_rc(static_cast<unsigned int>(status::StatusCode::SUCCESS));
                }
            }
        }

        // Send Reply
        wrapMsg.SerializeToString(&serializedMsg);
        LOG_DEBUG(info, "Broker", "Sending Message '" << "ConnectReply" << "' at Session #" << sessionId << " to PID #" << recMsg.pid() << "...");
        server_->sendMessage(sessionId, serializedMsg); 
    }
    auto ShindaBroker::onDisconnect(const int sessionId) -> void
    { 
        // Get PID of disconnected client from ID of Session & remove entry
        unsigned int clientPID = sessionPIDMap_[sessionId];
        sessionPIDMap_.erase(sessionId);

        // Search all topics to remove disconnected client if he has not unjoined
        for(auto topic = topics_.begin(); topic != topics_.end();)
        {
            // Search Publisher and remove if found
            if((*topic)->hasPublisher()) {
                (*topic)->removePublisher(clientPID);
            }

            // Search Subscribers and remove if found   
            if((*topic)->hasSubscribers()) {
                (*topic)->removeSubscriber(clientPID);
            }

            // ERASE TOPIC IF NO PROCESS HAS JOINED
            if((*topic)->isEmpty()) 
            {
                if((*topic)->getIsEssential())
                    essentialTopicsMem_ += (*topic)->getTotalSize();
                else
                    nonEssentialTopicsMem_ += (*topic)->getTotalSize();

                (*topic)->release();
                topic = topics_.erase(topic);
            }
            else
                ++topic;
        }
    }
    auto ShindaBroker::onJoin(const int sessionId, const SocketMsgs::JoinRequest& recMsg) -> void
    {
        // Send Reply
        SocketMsgs::SocketMsg wrapMsg;
        std::string serializedMsg;

        auto it = std::find_if(topics_.begin(), topics_.end(), [recMsg](const std::unique_ptr<Topic>& topic) -> bool {  return topic->getName() == recMsg.topicname(); });
        if(it != topics_.end())  // TOPIC ALREADY EXISTS
        {
            if(recMsg.ispublisher())    // PUBLISHER
            {
                if((*it)->hasPublisher())  // PUBLISHER WAS SET BEFORE -> ONLY ONE PUBLISHER IS ALLOWED
                {
                    wrapMsg.mutable_joinreply()->set_rc(static_cast<unsigned int>(status::StatusCode::TOPIC_FULL));
                    if((*it)->getPublisher().getPid() == recMsg.pid()) {
                        LOG_APP(warning, "Broker", "PID #" << recMsg.pid() << " already publishes to Topic '" << recMsg.topicname() << "'.");
                    }
                    else if((*it)->getPublisher().getPid() != recMsg.pid()) {
                        LOG_APP(warning, "Broker", "Another Publisher (#" << (*it)->getPublisher().getPid() << ") already publishes to Topic '" << recMsg.topicname() << "'. Can't have more than one Publisher per Topic.");
                    }
                }
                else    // PUBLISHER WAS NOT SET BEFORE -> SET PUBLISHER
                {
                    if((*it)->setPublisher(recMsg.pid())) {
                        wrapMsg.mutable_joinreply()->set_rc(static_cast<unsigned int>(status::StatusCode::SUCCESS));
                        wrapMsg.mutable_joinreply()->set_id((*it)->getPublisher().getId());
                    }
                    else {
                        wrapMsg.mutable_joinreply()->set_rc(static_cast<unsigned int>(status::StatusCode::BROKER_ERROR));
                    }
                }
            }
            else    // SUBSCRIBER 
            {

                if((*it)->addSubscriber(recMsg.pid())) {
                    wrapMsg.mutable_joinreply()->set_rc(static_cast<unsigned int>(status::StatusCode::SUCCESS));
                    wrapMsg.mutable_joinreply()->set_id((*it)->getSubscriberByPid(recMsg.pid()).getId());   
                }
                else {
                    wrapMsg.mutable_joinreply()->set_rc(static_cast<unsigned int>(status::StatusCode::BROKER_ERROR));
                }   
            }
        }
        else    // TOPIC DOES NOT EXIST YET 
        {
            wrapMsg.mutable_joinreply()->set_rc(static_cast<unsigned int>(status::StatusCode::TOPIC_DOES_NOT_EXIST));
            LOG_APP(error, "Broker", "Topic '" << recMsg.topicname() << "' does not exist. Could not join as " << recMsg.ispublisher() ? "Publisher." : "Subscriber."); 
        }
        
        wrapMsg.mutable_joinreply()->set_ispublisher(recMsg.ispublisher());
        wrapMsg.mutable_joinreply()->set_pid(recMsg.pid());
        wrapMsg.SerializeToString(&serializedMsg);
        LOG_DEBUG(info, "Broker", "Sending Message '" << "JoinTopicReply" << "' at Session #" << sessionId << " to PID #" << recMsg.pid() << "...");
        server_->sendMessage(sessionId, serializedMsg);
    }
    auto ShindaBroker::onUnregister(const int sessionId, const SocketMsgs::UnregisterRequest& recMsg) -> void
    {
        SocketMsgs::SocketMsg wrapMsg;
        std::string serializedMsg;

        auto itTopic = std::find_if(topics_.begin(), topics_.end(), [recMsg](const std::unique_ptr<Topic>& topic) -> bool {  return topic->getName() == recMsg.topicname(); });
        if(itTopic != topics_.end())  // TOPIC EXISTS
        {
            if(recMsg.ispublisher())
            {
                if((*itTopic)->getPublisher().getPid() == recMsg.pid())    // PUBLISHER
                {      
                    if((*itTopic)->removePublisher(recMsg.pid())) {
                        wrapMsg.mutable_unregisterreply()->set_rc(static_cast<unsigned int>(status::StatusCode::SUCCESS));
                    }
                    else {
                        wrapMsg.mutable_unregisterreply()->set_rc(static_cast<unsigned int>(status::StatusCode::BROKER_ERROR));
                    }
                }
                else
                    wrapMsg.mutable_unregisterreply()->set_rc(static_cast<unsigned int>(status::StatusCode::BROKER_ERROR));
            }
            else
            {
                if((*itTopic)->removeSubscriber(recMsg.pid())) {
                    wrapMsg.mutable_unregisterreply()->set_rc(static_cast<unsigned int>(status::StatusCode::SUCCESS));
                }
                else {
                    wrapMsg.mutable_unregisterreply()->set_rc(static_cast<unsigned int>(status::StatusCode::BROKER_ERROR));
                }
            }
            
            if((*itTopic)->isEmpty()) // ERASE TOPIC IF NO PROCESS HAS JOINED
            {
                if((*itTopic)->getIsEssential())
                    essentialTopicsMem_ += (*itTopic)->getTotalSize();
                else
                    nonEssentialTopicsMem_ += (*itTopic)->getTotalSize();

                (*itTopic)->release();
                topics_.erase(itTopic);
            }
            else
                (*itTopic)->logStatus();
        }   
        else {
            wrapMsg.mutable_unregisterreply()->set_rc(static_cast<unsigned int>(status::StatusCode::TOPIC_DOES_NOT_EXIST));
            LOG_APP(warning, "Broker", "PID #" << recMsg.pid() << " could not unjoin Topic '" << recMsg.topicname() << "'. Topic could not be found.");
        }
   
        wrapMsg.mutable_unregisterreply()->set_ispublisher(recMsg.ispublisher());
        wrapMsg.mutable_unregisterreply()->set_pid(recMsg.pid());
        wrapMsg.SerializeToString(&serializedMsg);
        LOG_DEBUG(info, "Broker", "Sending Message '" << "UnregisterReply" << "' at Session #" << sessionId << " to PID #" << recMsg.pid() << "...");
        server_->sendMessage(sessionId, serializedMsg);
    }
    
}   // Namespace shinda::pubsub
