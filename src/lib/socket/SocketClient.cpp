#include "socket/SocketClient.hpp"

using namespace std;

namespace shinda::socket::client {

SocketClient::SocketClient()
    : ioContext(), worker(boost::asio::make_work_guard(ioContext)),
      timer(ioContext), status(socketStatus::DISCONNECTED), callbacks(nullptr),
      socket(new seqpacket_protocol::socket(ioContext)) {
}
SocketClient::~SocketClient() {
  if (runThread.joinable()) {
    runThread.join();
  }
}

void SocketClient::start() {
  if (nullptr != callbacks) {
    tryConnect();
    // in case of restart try to join thread
    if (runThread.joinable()) {
      runThread.join();
    }
    runThread = thread([&] { ioContext.run(); });
  } else {
    throw std::runtime_error("SocketClient: Fatal Error - No callbacks registered\n");
  }
}
void SocketClient::onReadyForReconnect(const boost::system::error_code &error) {
  if (error != boost::asio::error::operation_aborted) {
    tryConnect();
  }
}
void SocketClient::connectHandler(const boost::system::error_code &error) {
  if (!error) 
  {
    status = socketStatus::CONNECTED;
    LOG_APP(info, "SocketClient", "Sucessfully connected to '" << PLATFORMD_SOCKET << "'...");
    LOG_APP(info, "SocketClient", "Start Listening on '" << PLATFORMD_SOCKET << "'...");

    // Start Receiving messages from the server
    int flags = 0;
    socket->async_receive(
        boost::asio::buffer(buffer), flags,
        boost::bind(&SocketClient::readHandler, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
  } else if (status != socketStatus::SHUTDOWN) {
    LOG_APP(warning, "SocketClient", "Could not connect to '" << PLATFORMD_SOCKET << "'. Error: " << error.message());
    socket->close();
    //startReconnectTimer();
  } else {
    doClose();
  }
}
void SocketClient::startReconnectTimer() {
  timer.expires_from_now(RECONNECT_TIMEOUT);
  timer.async_wait(
      bind(&SocketClient::onReadyForReconnect, this, placeholders::_1));
}
void SocketClient::registerCallbacks(ISocketClientCallbacks *cbs) const {
  callbacks = cbs;
}
void SocketClient::doClose() {
  LOG_APP(debug, "SocketClient", "Performing Shutdown...");
  status = socketStatus::SHUTDOWN;
  worker.reset();
  ioContext.stop();
  timer.cancel();
  boost::system::error_code ec;
  socket->shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
  if(ec) {
    throw std::runtime_error("SocketClient could not be shutdown. Exception: " + ec.message());
  }
  socket->close();
  socket.reset();
  LOG_APP(debug, "SocketClient", "Socket closed.");
}
void SocketClient::stop() {
  post(ioContext, boost::bind(&SocketClient::doClose, this));
}
void SocketClient::startReceiving() {}

void SocketClient::readHandler(const boost::system::error_code &error, size_t bytesReceived) {
  string receivedData;

  if (!error && bytesReceived >= 0) {
    receivedData = string(buffer.data(), bytesReceived);
    LOG_DEBUG(debug, "SocketClient", "Message was received. #BytesReceived = " << bytesReceived);
    callbacks->receivedPlatformMessage(move(receivedData));
    int flags = 0;
    socket->async_receive(
        boost::asio::buffer(buffer), flags,
        boost::bind(&SocketClient::readHandler, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
  } else {
    /*if (bytesReceived == 0) {
      LOG_DEBUG(debug, "SocketClient", "SocketClient: Peer closed connection");
      startReconnectTimer();
    }*/
    if (error != boost::asio::error::operation_aborted) {
      LOG_DEBUG(error, "SocketClient", "SocketClient: Error occured while reading: " << error.message());
      doClose();
    }
  }
}
void SocketClient::tryConnect() {
  LOG_APP(info, "SocketClient", "Trying to connect to '" << PLATFORMD_SOCKET << "'.");
  socket->async_connect(PLATFORMD_SOCKET,
                        boost::bind(&SocketClient::connectHandler, this,
                                    boost::asio::placeholders::error));
}
void SocketClient::onSend(const std::string &msg, const boost::system::error_code &error, size_t bytesTransferred) {
  if(error) {
    throw exception::SocketError("Message could not be sent. Error: " + error.message());
  }
  else {
    LOG_DEBUG(debug, "SocketClient", "Message was sent. #BytesSent = " << bytesTransferred);
  }
}
void SocketClient::sendMessage(const std::string &msg) {
  int flags = 0;
  socket->async_send(boost::asio::buffer(msg.data(), msg.size()), flags,
                     boost::bind(&SocketClient::onSend, this,
                                 msg,
                                 boost::asio::placeholders::error,
                                 boost::asio::placeholders::bytes_transferred));
}
unique_ptr<ISocketClient> SocketClientFactory::create() {
  return make_unique<socket::client::SocketClient>();
}

} // namespace uds_socket
