#include "socket/SocketNotifier.hpp"

namespace shinda::socket
{
    auto SocketNotifier::notify(const status::StatusCode st) -> void
    {
      std::unique_lock<std::mutex> lock(m_);
      wasReplied_ = true;
      st_ = st;
      c_.notify_all(); 
    }

    auto SocketNotifier::wait(const int millisecs, const int updateMs) -> status::StatusCode
    {
      int millisPassed = 0;

      std::unique_lock<std::mutex> lock(m_);
      do {
        c_.wait_for(lock, std::chrono::milliseconds{updateMs});
        millisPassed += updateMs;
      } while(millisPassed < millisecs && !wasReplied_);

      wasReplied_ = false;
      status::StatusCode st = st_;
      st_ = status::StatusCode::SOCKET_TIMEOUT;
      return st;
    }
    
} // namespace shinda::socket