#include "socket/SocketServer.hpp"

namespace shinda::socket::server {

SocketSession::SocketSession(boost::asio::io_context& ioContext, ISocketServerCallbacks* cbs) 
  : ioContext_(ioContext), socket_(new seqpacket_protocol::socket(ioContext)), callbacks_(cbs) {}
SocketSession::~SocketSession(){}

void SocketSession::start()
{
  LOG_DEBUG(info, "Session #" << id_, "Session started.");
  LOG_DEBUG(info, "Session #" << id_, "Start to receive messages at '" << PLATFORMD_SOCKET << "'...");
  int flags = 0;
  socket_->async_receive(
        boost::asio::buffer(buffer_), flags,
        boost::bind(&SocketSession::handleRead, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
}  
void SocketSession::handleRead(const boost::system::error_code &error, size_t bytesReceived) 
{                     
  if (!error) 
  {
    if (bytesReceived == 0) 
    {
      LOG_DEBUG(info, "Session #" << id_, " disconnected.");
      stop();
    }
    else
    {
      std::string receivedData = std::string(buffer_.data(), bytesReceived);
      LOG_DEBUG(debug, "Session #" << id_, "Message was received. #BytesReceived: " << bytesReceived);
      callbacks_->receivedPlatformMessage(id_, std::move(receivedData)); 
      int flags = 0;
      socket_->async_receive(
      boost::asio::buffer(buffer_), flags,
      boost::bind(&SocketSession::handleRead, this,
                  boost::asio::placeholders::error,
                  boost::asio::placeholders::bytes_transferred));
    }
  } 
  else 
  {
    if (error != boost::asio::error::operation_aborted) {
      LOG_APP(warning, "Session #" << id_, "Error occured while reading: " << error.message());
    }
    else if((boost::asio::error::eof == error) || (boost::asio::error::connection_reset == error)) {
      LOG_APP(info, "Session #" << id_, " disconnected.");
    }
    else {
      LOG_APP(warning, "Session #" << id_, "Error occured. Message: " << error.message());
    }
    stop();
  }
}
void SocketSession::sendMessage(const std::string &msg) {
  int flags = 0;
  socket_->async_send(boost::asio::buffer(msg.data(), msg.size()), flags,
                     boost::bind(&SocketSession::onSend, this,
                                 msg,
                                 boost::asio::placeholders::error,
                                 boost::asio::placeholders::bytes_transferred));
}
void SocketSession::onSend(const std::string &msg, const boost::system::error_code &error, size_t bytesTransferred) {
  if(error)
  {
    LOG_DEBUG(error, "Session #" << id_, "Error occured while sending Message " << msg << ". Message: " <<  error.message());
    stop();
  }
  else
    LOG_DEBUG(debug, "Session #" << id_, "Message was sent. #BytesSent = " << bytesTransferred);
}
void SocketSession::stop() {
  post(ioContext_, boost::bind(&SocketSession::doClose, this));
}
void SocketSession::doClose()
{
  try {
    if (socket_->is_open()) {
    socket_->shutdown(
        seqpacket_protocol::socket::shutdown_both);
    socket_->close();
    LOG_DEBUG(info, "SocketSession #" << id_, "Was stopped.");
    }
    callbacks_->receivedPlatformMessage(id_, "");
    sessionClosedEvent(id_); 
  }
  catch(const std::exception e) {
    throw std::runtime_error("Session #" + std::to_string(id_) + " could not be stopped. Exception: " + e.what());
  }
}

// GETTER
auto SocketSession::socket() const&  ->  const seqpacket_protocol::socket& { return *(socket_.get()); }
auto SocketSession::socket()    &    ->  seqpacket_protocol::socket& { return *(socket_.get()); }
auto SocketSession::id()     const&  ->  const int& { return id_; }
auto SocketSession::id()        &    ->  int& { return id_; }

//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//-----------------------------------------------------SOCKETSERVER------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------------------------

SocketServer::SocketServer() : ioContext_(), worker_(boost::asio::make_work_guard(ioContext_)), endpoint_(seqpacket_protocol::endpoint(PLATFORMD_SOCKET)),
    status_(socketStatus::DISCONNECTED), signals_(ioContext_), callbacks_(nullptr), sessionIdManager_(utils::IdManager(0, constants::kMaxSocketConnections))
    { 
      signals_.add(SIGINT); 
      signals_.add(SIGTERM);
      signals_.async_wait(std::bind(&SocketServer::doClose, this));
    }
SocketServer::~SocketServer() {
  if (runThread_.joinable()) {
    runThread_.join();
  }
}

void SocketServer::start() {    
  if (nullptr != callbacks_) {
      LOG_APP(info, "SocketServer", "Listening to incoming connections on '" << PLATFORMD_SOCKET << "'...");
      startListen();
      // in case of restart try to join thread
      if (runThread_.joinable()) {
          runThread_.join();
      }
      runThread_ = std::thread([&] { ioContext_.run(); });
  } else {
      throw std::runtime_error("SocketServer: Fatal Error - No callbacks registered\n");
  }
    
}
void SocketServer::startListen() {
    ::unlink(PLATFORMD_SOCKET);
    acceptor_.reset(new seqpacket_protocol::acceptor(ioContext_, endpoint_, true));
    acceptor_->listen();
    startAccept();
}
void SocketServer::startAccept() {   
    std::shared_ptr<SocketSession> newSession(new SocketSession(ioContext_, callbacks_));
    acceptor_->async_accept(newSession->socket(), 
      boost::bind(&SocketServer::acceptHandler, this, newSession, boost::asio::placeholders::error));
}
void SocketServer::acceptHandler(std::shared_ptr<SocketSession> newSession, const boost::system::error_code &error) {
  if (!error) 
  {
    status_ = socketStatus::CONNECTED;
    LOG_DEBUG(debug, "SocketServer", "Sucessfully Created Socket at '" << PLATFORMD_SOCKET << "'.");  
    regSession(newSession);
    newSession->id() = sessionIdManager_.getId();
  
    newSession->start();
  } 
  else if (status_ != socketStatus::SHUTDOWN) 
  {
    LOG_APP(error, "SocketServer", "Error Creating Socket at '" << PLATFORMD_SOCKET << "'.");
    doClose();
  } 
  else 
  {
    doClose();
  } 
  startAccept();
}

void SocketServer::join()
{
  if (runThread_.joinable()) {
    runThread_.join();
  }
}

void SocketServer::sendMessage(const int &sessionId, const std::string &msg) {
  auto sess_it = find_if(sessions_.begin(), sessions_.end(), [sessionId](const auto& pair) { return pair.first->id() == sessionId; });

  if (sess_it != sessions_.end())
    post(ioContext_, boost::bind(&SocketSession::sendMessage, sess_it->first, msg));
}
void SocketServer::deleteSession(const int& sessionId)
{
  auto sess_it = find_if(sessions_.begin(), sessions_.end(), [sessionId](const auto& pair) { return pair.first->id() == sessionId; });

  if (sess_it != sessions_.end())
  {
    sessionIdManager_.releaseId(sess_it->first->id());
    sessions_.erase(sess_it);
    
    LOG_APP(debug, "SocketServer", "Session #" << sessionId << " was deleted.");
  }
  else
    LOG_APP(error, "SocketServer", "Session #" << sessionId << " could not be deleted. Session not found.");
}
void SocketServer::regSession(sessPtr session) 
{
  std::lock_guard<std::mutex> lock(mx_);
  sessions_[session] = session->sessionClosedEvent.connect(std::bind(&SocketServer::deleteSession, this, std::placeholders::_1));
}
void SocketServer::registerCallbacks(ISocketServerCallbacks *cbs) const 
{
  callbacks_ = cbs;
}
void SocketServer::stop() {
  post(ioContext_, boost::bind(&SocketServer::doClose, this));
}
void SocketServer::doClose() {
  LOG_APP(info, "SocketServer", "Performing Shutdown");
  status_ = socketStatus::SHUTDOWN; 
  acceptor_->close();
  worker_.reset();
  ioContext_.stop();
  LOG_APP(info, "SocketServer", "Server stopped.");
}
std::unique_ptr<ISocketServer> SocketServerFactory::create() {
  return std::make_unique<socket::server::SocketServer>();
}

} // namespace socket::SocketServer
