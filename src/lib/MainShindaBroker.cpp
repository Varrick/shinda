// StdLib Imports
#include <functional>
#include <iostream>
#include <unistd.h>
// 3rd Party Imports
#include <boost/program_options.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
// Local Imports
#include "pubsub/ShindaBroker.hpp"
#include "utils/logging.hpp"
#include "macros.hpp"

using namespace shinda;
namespace po = boost::program_options;

std::function<void(int)> shutdownHandler;
void sigIntHandler(int signal) { shutdownHandler(signal); }

auto main(int ac, char* av[]) -> int
{
    #pragma region SET CUSTOM LOGGING FORMAT
    auto sink = boost::log::add_console_log();
    boost::log::add_common_attributes();
    sink->set_formatter(&utils::logging::coloring_formatter);
    #pragma endregion 

    #pragma region APPLY COMMAND LINE ARGS
    unsigned int logLevel;
    std::string configPath;
    po::options_description desc("Allowed Options");
    desc.add_options()
        ("help,h", "Produce Help Message")
        ("logLevel,l", po::value<unsigned int>()->default_value(2), "Log only specific messages <0 - 5> (0 = all messages, 5 = only highest priority messages")
        ("configPath,c", po::value<std::string>()->default_value("/etc/shinda/config.json"), "Path to the config file");
    
    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);
    
    if (vm.count("help")) {
        std::cout << desc << std::endl;
    }
    if(vm["logLevel"].as<unsigned int>() > 5)
    {
        LOG_APP(error, "BrokerMain", "Invalid Logging Level. Try running the script with --logLevel <0 <= number <= 5>");
        return 0;
    }
    else
    {
        logLevel = vm["logLevel"].as<unsigned int>();
        /* -------------------- SET UP LOGGING FILTER -------------------- */
        boost::log::core::get()->set_filter
        (
            boost::log::trivial::severity >= logLevel
        );

        LOG_APP(info, "BrokerMain", "logLevel = " << vm["logLevel"].as<unsigned int>());  
    }
    configPath = vm["configPath"].as<std::string>();
    LOG_APP(info, "BrokerMain", "ConfigPath: " << configPath);
    #pragma endregion

    /* --------------------- Start Broker Execution ----------------------- */
    pubsub::ShindaBroker broker(configPath);

    // Setup Abort-Handler 
    shutdownHandler = [&](int signal) {
        LOG_APP(info, "BrokerMain", "Caught signal " << signal);
        broker.stop();
        exit(0);
    };
    std::signal(SIGINT, sigIntHandler);

    // Wait until Broker has finished (and/or STRG-C happened)
    broker.join();

    return 0;
}

    