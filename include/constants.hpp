#pragma once 

#include <string>
#include <sys/mman.h>

namespace constants
{
    // Broker
    inline constexpr char kDefaultConfigPath[] {"/etc/shinda/config.json"};

    // Ringbuffer
    inline constexpr int kMinRingbufferSegments {4};
    inline constexpr int kDefaultRingbufferSegments {16};
    inline constexpr int kDefaultShmPermissions {0660};
    inline constexpr int kDefaultMmapPermissions {PROT_WRITE | PROT_READ};
    inline constexpr int kDefaultUmaskPermissions {0117};

    // Publisher/Subscriber
    inline constexpr int kMaxSubscribersPerTopic {10};
    inline constexpr int kDefaultPubTimeoutUs {0};
    inline constexpr int kDefaultSubTimeoutUs {0};

    // Socket
    inline constexpr int kMaxSocketConnections {1000};
    inline constexpr int kDefaultSocketTimeoutMs {1000};
    inline constexpr int kDefaultSocketUpdateMs {100};
    
    // Synchronization
    inline constexpr int kDefaultNumNops {1000};
    inline constexpr int kDefaultNumScheduleYields {5000};

    // Benchmarking
    inline constexpr int kDefaultTransmitMaxPower {27};
    inline constexpr int kDefaultMaxMsgPower {25};
    inline constexpr int kDefaultMinMsgPower {5};
    inline constexpr int kDefaultMsgPowerStep {1};
}