#pragma once

#include <numeric>
#include <type_traits>
#include <cmath>
// Local Imports
#include "memory/SharedRingBuffer.hpp"
#include "constants.hpp"

namespace shinda::utils::math
{
    template<
        typename T, 
        typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    auto static mean(const std::vector<T>& data) -> double
    {
        double sum = 0;
        std::for_each (std::begin(data), std::end(data), [&](const T sample) {
            sum += sample;
        });
        
        return sum / data.size();
    }

    template<
        typename T, 
        typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type>
    auto static stdev(const std::vector<T>& data) -> double
    {
        mean = mean<T>(data);
        double accum = 0.0;
        std::for_each (std::begin(data), std::end(data), [&](const T sample) {
            accum += std::pow((sample - mean), 2);
        });

        return sqrt(accum / (data.size()-1));
    }

    auto inline static circLead(const int head, const int tail, const int bufElems) -> int
    {
        if (head >= tail) return head - tail;
        return bufElems - (tail - head);
    }

    auto static fitSegments(const size_t maxMsgSize, const int startNumSegments, const size_t availableMem) -> uint16_t
    {
        uint16_t numSegments = startNumSegments;
        size_t totalSize;
        while(numSegments >= constants::kMinRingbufferSegments)
        {
            numSegments /= 2;
            totalSize = memory::SharedRingBuffer::estimateSize(maxMsgSize, numSegments);
            if(totalSize > availableMem)
                continue;
            else
                break;          
        }
        
        if(numSegments < constants::kMinRingbufferSegments)
            return 0;
        else
            return numSegments;
    }

}   // namespace shinda::utils::math
