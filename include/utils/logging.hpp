#pragma once

#include <iostream>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/expressions/record.hpp>

namespace shinda::utils::logging
{
    void coloring_formatter(const boost::log::record_view& rec, boost::log::formatting_ostream& strm)
    {  
        auto severity = rec[boost::log::trivial::severity];
        if (severity)
        {
            // Set the color
            switch (severity.get())
            {
                case boost::log::trivial::trace:
                    strm << "\033[35m";
                    break;
                case boost::log::trivial::debug:
                    strm << "\033[37m";
                    break;
                case boost::log::trivial::info:
                    strm << "\033[36m";
                    break;
                case boost::log::trivial::warning:
                    strm << "\033[31m";
                    break;
                case boost::log::trivial::error:
                case boost::log::trivial::fatal:
                    strm << "\033[35m";
                    break;
                default:
                    break;
            }
        }

        // Format Message
        strm 
            << "[" << rec[boost::log::trivial::severity] << "]"
            << rec[boost::log::expressions::smessage];
            //<< "<" << log::expressions::attr<unsigned int>("LineID") << ">";
             //<< "[" << log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S:%f") << "]"
    }

}   // namespace shinda::utils::logging
