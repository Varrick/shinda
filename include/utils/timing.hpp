#pragma once

#include <chrono>
#include <functional>
#include <atomic>
#include <thread>
#include <condition_variable>

namespace shinda::utils::timing
{ 

    inline size_t wallClockTime_milliseconds() 
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>
                (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    }

    inline size_t wallClockTime_microseconds() 
    {
        return std::chrono::duration_cast<std::chrono::microseconds>
                (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    }

    inline size_t wallClockTime_nanoseconds()
    {
        return std::chrono::duration_cast<std::chrono::nanoseconds>
                (std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    }  

    inline size_t steady_milliseconds() 
    {
        return std::chrono::duration_cast<std::chrono::milliseconds>
                (std::chrono::steady_clock::now().time_since_epoch()).count();
    }

    inline size_t steady_microseconds() 
    {
        return std::chrono::duration_cast<std::chrono::microseconds>
                (std::chrono::steady_clock::now().time_since_epoch()).count();
    }

    inline size_t steady_nanoseconds()
    {
        return std::chrono::duration_cast<std::chrono::nanoseconds>
                (std::chrono::steady_clock::now().time_since_epoch()).count();
    }  


    /***
     * Timer that executes a user provided function if a certain amount of time
     * has elapsed before the timer was reset. If the timeout was reached, the timer
     * thread automatically finishes. Call Stop for cleanup.
     * 
     ***/
    class DeadlineTimer
    {

    public:
        DeadlineTimer(const std::chrono::milliseconds &timeoutInterval,
             const std::function<void ()>& task)       
             : mTimeoutInterval(timeoutInterval), mTask(task), mRunning(false)
        {
        }

        ~DeadlineTimer()
        {
            Stop();
        }

        bool IsRunning() const
        {
            return mRunning;
        }

         /***
         * Starts the timer. If the timer is not reset before the timeout interval has elapsed,
         * the provided function will be executed after which the thread finishes.
         * 
         ***/
        void Start()
        {
            Stop();

            mRunning = true;
            mThread = std::thread([this]
            {
                while (mRunning)
                {
                    std::unique_lock<std::mutex> lock(mRunCondMutex);
                    auto waitResult = mRunCondition.wait_for(lock, mTimeoutInterval);
                    if (mRunning && waitResult == std::cv_status::timeout) {
                        mRunning = false;
                        mTask();
                    }
                }
            });
        }

        /***
         * Restarts waiting for the timeout interval if the timer was started.
         * 
         ***/
        void Reset()
        {
            mRunCondition.notify_all();
        }

        /***
         * Stops the timer and joins the timer thread if joinable..
         * 
         ***/
        void Stop()
        {
            std::unique_lock<std::mutex> lock(mStopMutex);
            mRunning = false;
            mRunCondition.notify_all();
            if(mThread.joinable())
                mThread.join();
        }

    private:
        std::chrono::milliseconds mTimeoutInterval;
        std::function<void ()> mTask;
        std::atomic_bool mRunning;
        std::condition_variable mRunCondition;
        std::mutex mRunCondMutex;
        std::thread mThread;
        std::mutex mStopMutex;

    };


};  // namespace shinda::utils::timing