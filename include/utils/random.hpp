#include <random>

namespace shinda::utils::random
{
    std::string random_string(const int length)
    {
        std::random_device rd;
        std::mt19937 mt(rd());
        std::uniform_int_distribution<int> dist(0, RAND_MAX);

        auto randchar = [&]() -> char
        {
            const char charset[] =
            "0123456789"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "abcdefghijklmnopqrstuvwxyz";
            const size_t max_index = (sizeof(charset) - 1);
            return charset[dist(mt) % max_index];
        };
        std::string str(length,0);
        std::generate_n(str.begin(), length, randchar);
        return str;
    }
}