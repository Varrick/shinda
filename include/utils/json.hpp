#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <unordered_map>
#include <utility>
#include <iostream>
#include <climits>
#include <fstream>
#include <cstdint>
#include <cmath>
#include <cctype>
#include <deque>
#include <map>
#include <type_traits>
#include <initializer_list>
#include <ostream>

namespace shinda::utils::json::parser
{

/** **************************************************************************************
*                                                                                        *
*    A Ridiculously Simple JSON Parser for C++ (RSJp-cpp)                                *
*    Version 2.x                                                                         *
*    ----------------------------------------------------------                          *
*    Copyright (C) 2018  Subhrajit Bhattacharya                                          *
*                                                                                        *
*    This program is free software: you can redistribute it and/or modify                *
*    it under the terms of the GNU General Public License as published by                *
*    the Free Software Foundation, either version 3 of the License, or                   *
*    (at your option) any later version.                                                 *
*                                                                                        *
*    This program is distributed in the hope that it will be useful,                     *
*    but WITHOUT ANY WARRANTY; without even the implied warranty of                      *
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                       *
*    GNU General Public License for more details <http://www.gnu.org/licenses/>.         *
*                                                                                        *
*                                                                                        *
*    Contact:  subhrajit@gmail.com                                                       *
*              https://www.lehigh.edu/~sub216/ , http://subhrajit.net/                   *
*                                                                                        *
*                                                                                        *
*************************************************************************************** **/

static char const* RSJobjectbrackets = "{}";
static char const* RSJarraybrackets = "[]";
static char RSJobjectassignment = ':';
static char RSJarraydelimiter = ',';

static std::vector<char const*> RSJbrackets = {RSJobjectbrackets, RSJarraybrackets};
static std::vector<char const*> RSJstringquotes = {"\"\"", "''"};
static char RSJcharescape = '\\';
static std::string RSJlinecommentstart = "//";

static std::string RSJprinttab = "    ";

enum RSJresourceType { RSJ_UNINITIATED, RSJ_UNKNOWN, RSJ_OBJECT, RSJ_ARRAY, RSJ_LEAF };

// ============================================================
// Direct string manipulation functions

inline 
std::string to_string (RSJresourceType rt) {
    switch (rt) {
        case RSJ_UNINITIATED: return("RSJ_UNINITIATED");
        case RSJ_UNKNOWN: return("RSJ_UNKNOWN");
        case RSJ_OBJECT: return("RSJ_OBJECT");
        case RSJ_ARRAY: return("RSJ_ARRAY");
        case RSJ_LEAF: return("RSJ_LEAF");
    }
}

enum StrTrimDir { STRTRIM_L=1, STRTRIM_R=2, STRTRIM_LR=3 };

inline 
std::string strtrim (std::string str, std::string chars=" \t\n\r", int max_count=-1, StrTrimDir dirs=STRTRIM_LR) {
    if (str.empty()) return(str);
    if (max_count<0) max_count = str.length();
    
    if (dirs & STRTRIM_L) { // left trim
        int p;
        for (p=0; p<max_count; ++p)
            if (chars.find(str[p])==std::string::npos) break;
        str.erase (0, p); // const_str::l_erase(p)
    }
    
    if (dirs & STRTRIM_R) { // right trim
        int q, strlenm1=str.length()-1;
        for (q=0; q<max_count; ++q)
            if (chars.find(str[strlenm1-q])==std::string::npos) break;
        str.erase (str.length()-q, q); // const_str::r_erase(q)
    }
    
    return (str);
}

inline 
std::string strip_outer_quotes (std::string str, char* qq=NULL) {
    str = strtrim (str);
    
    std::string ret = strtrim (str, "\"");
    if (ret.length()==str.length()) {
        ret = strtrim (str, "'");
        if (qq && ret!=str) *qq = '\'';
    }
    else if (qq)
        *qq = '"';
    
    return (ret);
}

// ----------------

inline 
int is_bracket (char c, std::vector<char const*>& bracks, int indx=0) {
    for (int b=0; b<bracks.size(); ++b)
        if (c==bracks[b][indx]) 
            return (b);
    return (-1);
}

inline 
std::vector<std::string> split_RSJ_array (const std::string& str) { // TODO: Make efficient. This function is speed bottleneck.
    // splits, while respecting brackets and escapes
    std::vector<std::string> ret;
    
    std::string current;
    std::vector<int> bracket_stack;
    std::vector<int> quote_stack;
    bool escape_active = false;
    int bi;
    
    for (int a=0; a<str.length(); ++a) { // *
        
        // delimiter
        if ( bracket_stack.size()==0  &&  quote_stack.size()==0  &&  str[a]==RSJarraydelimiter ) {
            ret.push_back (current);
            current.clear(); bracket_stack.clear(); quote_stack.clear(); escape_active = false;
            continue; // to *
        }
        
        // ------------------------------------
        // checks for string
        
        if (quote_stack.size() > 0) { // already inside string
            if (str[a]==RSJcharescape)  // an escape character
                escape_active = !escape_active;
            else if (!escape_active  &&  str[a]==RSJstringquotes[quote_stack.back()][1] ) { // close quote
                quote_stack.pop_back();
                escape_active = false;
            }
            else
                escape_active = false;
            
            current.push_back (str[a]);
            continue; // to *
        }
        
        if (quote_stack.size()==0) { // check for start of string
            if ((bi = is_bracket (str[a], RSJstringquotes)) >= 0) {
                quote_stack.push_back (bi);
                current.push_back (str[a]);
                continue; // to *
            }
        }
        
        // ------------------------------------
        // checks for comments
        
        if (quote_stack.size()==0) { // comment cannot start inside string
            
            // single-line commenst
            if (str.compare (a, RSJlinecommentstart.length(), RSJlinecommentstart) == 0) {
                // ignore until end of line
                int newline_pos = str.find ("\n", a);
                if (newline_pos == std::string::npos)
                    newline_pos = str.find ("\r", a);
                
                if (newline_pos != std::string::npos)
                    a = newline_pos; // point to the newline character (a will be incremented)
                else // the comment continues until EOF
                    a = str.length();
                continue;
            }
        }
        
        // ------------------------------------
        // checks for brackets
        
        if ( bracket_stack.size()>0  &&  str[a]==RSJbrackets[bracket_stack.back()][1] ) { // check for closing bracket
            bracket_stack.pop_back();
            current.push_back (str[a]);
            continue;
        }
        
        if ((bi = is_bracket (str[a], RSJbrackets)) >= 0) {
            bracket_stack.push_back (bi);
            current.push_back (str[a]);
            continue; // to *
        }
        
        // ------------------------------------
        // otherwise
        current.push_back (str[a]);
    }
    
    if (current.length() > 0)
        ret.push_back (current);
    
    return (ret);
}

inline 
std::string insert_tab_after_newlines (std::string str) {
    for (int a=0; a<str.length(); ++a)
        if (str[a]=='\n') {
            str.insert (a+1, RSJprinttab);
            a += RSJprinttab.length();
        }
    return (str);
}


// ============================================================

// forward declarations
class RSJparsedData;
class RSJresource;

// Objet and array typedefs
typedef std::unordered_map <std::string,RSJresource>    RSJobject;
typedef std::vector <RSJresource>                       RSJarray;

// ------------------------------------
// Main classes

class RSJresource {
/* Use: RSJresource("RSJ_string_data").as<RSJobject>()["keyName"].as<RSJarray>()[2].as<int>()
        RSJresource("RSJ_string_data")["keyName"][2].as<int>()  */
private:
    // main data
    std::string data; // can be object, vector or leaf data
    bool _exists;      // whether the RSJ resource exists.
    
    // parsed data
    RSJparsedData* parsed_data_p;
    
public:
    // constructor
    RSJresource () : _exists (false), parsed_data_p (NULL) { } // no data field.
    
    RSJresource (std::string str) : data (str), _exists (true), parsed_data_p (NULL) { }
    RSJresource (const char* str) : RSJresource(std::string(str)) { }
    
    // other convertion
    template <class dataType>
    RSJresource (dataType d) : RSJresource(std::to_string(d)) { }
    
    // read from file and stream
    RSJresource (std::istream& is) : _exists (true), parsed_data_p (NULL) {
        data = std::string ( (std::istreambuf_iterator<char>(is)), (std::istreambuf_iterator<char>()) );
    }
    RSJresource (std::ifstream& ifs) : _exists (true), parsed_data_p (NULL) {
        std::istream& is = ifs;
        data = std::string ( (std::istreambuf_iterator<char>(is)), (std::istreambuf_iterator<char>()) );
    }
    
    // free allocated memory for parsed data
    ~RSJresource();
    
    // deep copy
    RSJresource (const RSJresource& r);
    RSJresource& operator= (const RSJresource& r);
    
    // ------------------------------------
    // parsers (old)
    RSJresourceType parse (bool force=false);
    void parse_full (bool force=false, int max_depth=INT_MAX, int* parse_count_for_verbose_p=NULL); // recursively parse the entire JSON text
    // parser (new)
    void fast_parse (std::string* str_p=NULL, bool copy_string=false, int max_depth=INT_MAX, int* parse_start_str_pos=NULL); // TODO: finish.
    
    RSJobject& as_object (bool force=false);
    RSJarray& as_array (bool force=false);
    
    // ------------------------------------
    
    // access raw data and other attributes
    int size(void);
    std::string& raw_data (void) { return (data); }
    bool exists (void) { return (_exists); }
    bool is_parsed (void) { return (parsed_data_p!=NULL); }
    RSJresourceType type (void);
    // emitter
    std::string as_str (bool print_comments=false, bool update_data=true);
    void print (bool print_comments=false, bool update_data=true) 
        { std::cout << as_str(print_comments,update_data) << std::endl; }
    
    // opertor[]
    RSJresource& operator[] (std::string key); // object
    RSJresource& operator[] (int indx); // array
    
    // ------------------------------------
    
    // as
    template <class dataType>
    dataType as (const dataType& def = dataType()) { // specialized outside class declaration
        if (!exists()) return (def);
        return dataType (data); // default behavior for unknown types: invoke 'dataType(std::string)'
    }
    
    // as_vector
    template <class dataType, class vectorType=std::vector<dataType> > // vectorType should have push_back method
    vectorType as_vector (const vectorType& def = vectorType());
    
    // as_map
    template <class dataType, class mapType=std::unordered_map<std::string,dataType> > // mapType should have operator[] defined
    mapType as_map (const mapType& def = mapType());    
};

// ------------------------------------------------------------

class RSJparsedData {
public:
    RSJobject object;
    RSJarray array;
    
    RSJresourceType type;
    RSJparsedData() : type(RSJ_UNKNOWN) {}
    
    // parser (single-level)
    void parse (const std::string& data, RSJresourceType typ = RSJ_UNKNOWN) {
        std::string content = strtrim(data);
        
        if (typ==RSJ_OBJECT || typ==RSJ_UNKNOWN) {
            // parse as object:
            content = strtrim (strtrim (content, "{", 1, STRTRIM_L ), "}", 1, STRTRIM_R );
            if (content.length() != data.length()) { // a valid object
                std::vector<std::string> nvPairs = split_RSJ_array (content);
                for (int a=0; a<nvPairs.size(); ++a) {
                    std::size_t assignmentPos = nvPairs[a].find (RSJobjectassignment);
                    object.insert (make_pair( 
                                        strip_outer_quotes (nvPairs[a].substr (0,assignmentPos) ) ,
                                        RSJresource (strtrim (nvPairs[a].substr (assignmentPos+1) ) )
                               ) );
                }
                if (object.size() > 0) {
                    type = RSJ_OBJECT;
                    return;
                }
            }
        }
        
        if (typ==RSJ_ARRAY || typ==RSJ_UNKNOWN) {
            // parse as array
            content = strtrim (strtrim (content, "[", 1, STRTRIM_L ), "]", 1, STRTRIM_R );
            if (content.length() != data.length()) { // a valid array
                std::vector<std::string> nvPairs = split_RSJ_array (content);
                for (int a=0; a<nvPairs.size(); ++a) 
                    array.push_back (RSJresource (strtrim (nvPairs[a]) ) );
                if (array.size() > 0) {
                    type = RSJ_ARRAY;
                    return;
                }
            }
        }
        
        if (typ==RSJ_UNKNOWN)
            type = RSJ_LEAF;
    }

    // remove non-existing items inserted due to accessing
    int cleanup(void) {
    
        if (type==RSJ_OBJECT) {
            bool found = true;
            while (found) {
                found = false;
                for (auto it=object.begin(); it!=object.end(); ++it)
                    if (!(it->second.exists())) {
                        object.erase(it);
                        found = true;
                        break; // break for loop since it is now invalid
                    }
            }
            return (object.size());
        }
        
        if (type==RSJ_ARRAY) { // erases only the non-existent elements at the tail
            while (!(array[array.size()-1].exists()))
                array.pop_back();
            return (array.size());
        }
        
        if (type==RSJ_LEAF)
            return (1);
        
        return (0);
    }
    
    // size
    int size(void) { return (cleanup()); }
};


// ------------------------------------------------------------
// RSJresource member functions

inline 
RSJresource::~RSJresource (){
    if (parsed_data_p) delete parsed_data_p;
}

inline 
RSJresource::RSJresource (const RSJresource& r) {
    data=r.data;
    _exists = r._exists;
    if(r.parsed_data_p) parsed_data_p = new RSJparsedData(*(r.parsed_data_p));
    else parsed_data_p = NULL;
}

inline 
RSJresource& RSJresource::operator= (const RSJresource& r) {
    data=r.data;
    _exists = r._exists;
    if(r.parsed_data_p) parsed_data_p = new RSJparsedData(*(r.parsed_data_p));
    else parsed_data_p = NULL;
    return *this;
}

inline 
int RSJresource::size (void) {
    if (!exists()) return (0);
    parse(); // parse if not parsed
    return (parsed_data_p->size());
}

inline 
RSJresourceType RSJresource::type (void) {
    if (!exists()) return (RSJ_UNINITIATED);
    parse(); // parse if not parsed
    return (parsed_data_p->type);
}

inline 
std::string RSJresource::as_str (bool print_comments, bool update_data) {
    if (exists()) {
        std::string ret;
        parse(); // parse if not parsed
        parsed_data_p->cleanup();
        
        if (parsed_data_p->type==RSJ_OBJECT) {
            ret = "{\n";
            for (auto it=parsed_data_p->object.begin(); it!=parsed_data_p->object.end(); ++it) {
                ret += RSJprinttab + "'" + it->first + "': " + insert_tab_after_newlines( it->second.as_str (print_comments, update_data) );
                if (std::next(it) != parsed_data_p->object.end()) ret += ",";
                if (print_comments)
                    ret += " // " + to_string(it->second.type());
                ret += "\n";
            }
            ret += "}";
        }
        else if (parsed_data_p->type==RSJ_ARRAY) {
            ret = "[\n";
            for (auto it=parsed_data_p->array.begin(); it!=parsed_data_p->array.end(); ++it) {
                ret += RSJprinttab + insert_tab_after_newlines( it->as_str (print_comments, update_data) );
                if (std::next(it) != parsed_data_p->array.end()) ret += ",";
                if (print_comments)
                    ret += " // " + to_string(it->type());
                ret += "\n";
            }
            ret += "]";
        }
        else // RSJ_LEAF or RSJ_UNKNOWN
             ret = strtrim (data);
        
        if (update_data) data = ret;
        return (ret);
    }
    else
        return ("");
}

// Parsers

inline 
RSJresourceType RSJresource::parse (bool force) {
    if (!parsed_data_p)  parsed_data_p = new RSJparsedData;
    if (parsed_data_p->type==RSJ_UNKNOWN || force)  parsed_data_p->parse (data, RSJ_UNKNOWN);
    return (parsed_data_p->type);
}

inline 
void RSJresource::parse_full (bool force, int max_depth, int* parse_count_for_verbose_p) { // recursive parsing (slow)
    if (max_depth==0) return;
    if (!parsed_data_p)  parsed_data_p = new RSJparsedData;
    if (parsed_data_p->type==RSJ_UNKNOWN || force)  parsed_data_p->parse (data, RSJ_UNKNOWN);
    // verbose
    if (parse_count_for_verbose_p) {
        (*parse_count_for_verbose_p)++;
        if ( (*parse_count_for_verbose_p) % 100 == 0)
            std::cout << "parse_full: " << (*parse_count_for_verbose_p) << " calls." << std::endl;
    }
    // recursive parse children if not already parsed
    if (parsed_data_p->type==RSJ_OBJECT) 
        for (auto it=parsed_data_p->object.begin(); it!=parsed_data_p->object.end(); ++it)
            it->second.parse_full (force, max_depth-1, parse_count_for_verbose_p);
    else if (parsed_data_p->type==RSJ_ARRAY)
        for (auto it=parsed_data_p->array.begin(); it!=parsed_data_p->array.end(); ++it) 
            it->parse_full (force, max_depth-1, parse_count_for_verbose_p);
}

// ============================================================

// ------------------------------------------------------------

inline 
RSJobject& RSJresource::as_object (bool force) {
    if (!parsed_data_p)  parsed_data_p = new RSJparsedData;
    if (parsed_data_p->type==RSJ_UNKNOWN || force)  parsed_data_p->parse (data, RSJ_OBJECT);
    return (parsed_data_p->object);
}

inline 
RSJresource& RSJresource::operator[] (std::string key) { // returns reference
    return ( (as_object())[key] ); // will return empty resource (with _exists==false) if 
                                            // either this resource does not exist, is not an object, or the key does not exist
}

inline 
RSJarray& RSJresource::as_array (bool force) {
    if (!parsed_data_p)  parsed_data_p = new RSJparsedData;
    if (parsed_data_p->type==RSJ_UNKNOWN || force)  parsed_data_p->parse (data, RSJ_ARRAY);
    return (parsed_data_p->array);
}

inline 
RSJresource& RSJresource::operator[] (int indx) { // returns reference
    as_array();
    if (indx >= parsed_data_p->array.size())
        parsed_data_p->array.resize(indx+1); // insert empty resources
    return (parsed_data_p->array[indx]); // will return empty resource (with _exists==false) if 
                                            // either this resource does not exist, is not an object, or the key does not exist
}

// ------------------------------------------------------------
// special 'as':

template <class dataType, class vectorType> inline 
vectorType RSJresource::as_vector (const vectorType& def) { // returns copy -- for being consistent with other 'as' specializations
    if (!exists()) return (def);
    vectorType ret;
    as_array();
    for (auto it=parsed_data_p->array.begin(); it!=parsed_data_p->array.end(); ++it)
        ret.push_back (it->as<dataType>());
    return (ret);
}

template <class dataType, class mapType> inline 
mapType RSJresource::as_map (const mapType& def) { // returns copy -- for being consistent with other 'as' specializations
    if (!exists()) return (def);
    mapType ret;
    as_object();
    for (auto it=parsed_data_p->object.begin(); it!=parsed_data_p->object.end(); ++it)
        ret[it->first] = it->second.as<dataType>();
    return (ret);
}

// ============================================================
// Specialized .as() member functions

// Helper preprocessor directives
#define rsjObject  as<RSJobject>()
#define rsjArray   as<RSJarray>()
#define rsjAs(t)   as<t>()


// RSJobject
template <> inline 
RSJobject RSJresource::as<RSJobject> (const RSJobject& def) { // returns copy -- for being consistent with other 'as' specializations
    if (!exists()) return (def);
    return (as_object());
}

// RSJarray
template <> inline 
RSJarray  RSJresource::as<RSJarray> (const RSJarray& def) { // returns copy -- for being consistent with other 'as' specializations
    if (!exists()) return (def);
    return (as_array());
}

// ------------------------------------
// Elementary types

// String
template <> inline 
std::string  RSJresource::as<std::string> (const std::string& def) {
    if (!exists()) return (def);
    
    char qq = '\0';
    std::string ret = strip_outer_quotes (data, &qq);
    
    std::vector< std::vector<std::string> > escapes = { {"\\n","\n"}, {"\\r","\r"}, {"\\t","\t"}, {"\\\\","\\"} };
    if (qq=='"')
        escapes.push_back ({"\\\"","\""});
    else if (qq=='\'')
        escapes.push_back ({"\\'","'"});
    
    for (int a=0; a<escapes.size(); ++a)
        for ( std::size_t start_pos=ret.find(escapes[a][0]); start_pos!=std::string::npos; start_pos=ret.find(escapes[a][0],start_pos) ) {
            ret.replace (start_pos, escapes[a][0].length(), escapes[a][1]);
            start_pos += escapes[a][1].length();
        }
    
    return (ret);
}

// integer
template <> inline 
int  RSJresource::as<int> (const int& def) {
    if (!exists()) return (def);
    return (atoi (strip_outer_quotes(data).c_str() ) );
}

// double
template <> inline 
double  RSJresource::as<double> (const double& def) {
    if (!exists()) return (def);
    return (atof (strip_outer_quotes(data).c_str() ) );
}

// bool
template <> inline 
bool  RSJresource::as<bool> (const bool& def) {
    if (!exists()) return (def);
    std::string cleanData = strip_outer_quotes (data);
    if (cleanData=="true" || cleanData=="TRUE" || cleanData=="True" || atoi(cleanData.c_str())!=0) return (true);
    return (false);
}

}

// -------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------
// -------------------------------------------------------------------------------------------------------------

namespace shinda::utils::json::serializer
{
/*****************************************************************************************
*                       Do what the fuck you want public license                         *
******************************************************************************************/

using std::map;
using std::deque;
using std::string;
using std::enable_if;
using std::initializer_list;
using std::is_same;
using std::is_convertible;
using std::is_integral;
using std::is_floating_point;

namespace {
    string json_escape( const string &str ) {
        string output;
        for( unsigned i = 0; i < str.length(); ++i )
            switch( str[i] ) {
                case '\"': output += "\\\""; break;
                case '\\': output += "\\\\"; break;
                case '\b': output += "\\b";  break;
                case '\f': output += "\\f";  break;
                case '\n': output += "\\n";  break;
                case '\r': output += "\\r";  break;
                case '\t': output += "\\t";  break;
                default  : output += str[i]; break;
            }
        return std::move( output );
    }
}

class JSON
{
    union BackingData {
        BackingData( double d ) : Float( d ){}
        BackingData( long   l ) : Int( l ){}
        BackingData( bool   b ) : Bool( b ){}
        BackingData( string s ) : String( new string( s ) ){}
        BackingData()           : Int( 0 ){}

        deque<JSON>        *List;
        map<string,JSON>   *Map;
        string             *String;
        double              Float;
        long                Int;
        bool                Bool;
    } Internal;

    public:
        enum class Class {
            Null,
            Object,
            Array,
            String,
            Floating,
            Integral,
            Boolean
        };

        template <typename Container>
        class JSONWrapper {
            Container *object;

            public:
                JSONWrapper( Container *val ) : object( val ) {}
                JSONWrapper( std::nullptr_t )  : object( nullptr ) {}

                typename Container::iterator begin() { return object ? object->begin() : typename Container::iterator(); }
                typename Container::iterator end() { return object ? object->end() : typename Container::iterator(); }
                typename Container::const_iterator begin() const { return object ? object->begin() : typename Container::iterator(); }
                typename Container::const_iterator end() const { return object ? object->end() : typename Container::iterator(); }
        };

        template <typename Container>
        class JSONConstWrapper {
            const Container *object;

            public:
                JSONConstWrapper( const Container *val ) : object( val ) {}
                JSONConstWrapper( std::nullptr_t )  : object( nullptr ) {}

                typename Container::const_iterator begin() const { return object ? object->begin() : typename Container::const_iterator(); }
                typename Container::const_iterator end() const { return object ? object->end() : typename Container::const_iterator(); }
        };

        JSON() : Internal(), Type( Class::Null ){}

        JSON( initializer_list<JSON> list ) 
            : JSON() 
        {
            SetType( Class::Object );
            for( auto i = list.begin(), e = list.end(); i != e; ++i, ++i )
                operator[]( i->ToString() ) = *std::next( i );
        }

        JSON( JSON&& other )
            : Internal( other.Internal )
            , Type( other.Type )
        { other.Type = Class::Null; other.Internal.Map = nullptr; }

        JSON& operator=( JSON&& other ) {
            ClearInternal();
            Internal = other.Internal;
            Type = other.Type;
            other.Internal.Map = nullptr;
            other.Type = Class::Null;
            return *this;
        }

        JSON( const JSON &other ) {
            switch( other.Type ) {
            case Class::Object:
                Internal.Map = 
                    new map<string,JSON>( other.Internal.Map->begin(),
                                          other.Internal.Map->end() );
                break;
            case Class::Array:
                Internal.List = 
                    new deque<JSON>( other.Internal.List->begin(),
                                      other.Internal.List->end() );
                break;
            case Class::String:
                Internal.String = 
                    new string( *other.Internal.String );
                break;
            default:
                Internal = other.Internal;
            }
            Type = other.Type;
        }

        JSON& operator=( const JSON &other ) {
            ClearInternal();
            switch( other.Type ) {
            case Class::Object:
                Internal.Map = 
                    new map<string,JSON>( other.Internal.Map->begin(),
                                          other.Internal.Map->end() );
                break;
            case Class::Array:
                Internal.List = 
                    new deque<JSON>( other.Internal.List->begin(),
                                      other.Internal.List->end() );
                break;
            case Class::String:
                Internal.String = 
                    new string( *other.Internal.String );
                break;
            default:
                Internal = other.Internal;
            }
            Type = other.Type;
            return *this;
        }

        ~JSON() {
            switch( Type ) {
            case Class::Array:
                delete Internal.List;
                break;
            case Class::Object:
                delete Internal.Map;
                break;
            case Class::String:
                delete Internal.String;
                break;
            default:;
            }
        }

        template <typename T>
        JSON( T b, typename enable_if<is_same<T,bool>::value>::type* = 0 ) : Internal( b ), Type( Class::Boolean ){}

        template <typename T>
        JSON( T i, typename enable_if<is_integral<T>::value && !is_same<T,bool>::value>::type* = 0 ) : Internal( (long)i ), Type( Class::Integral ){}

        template <typename T>
        JSON( T f, typename enable_if<is_floating_point<T>::value>::type* = 0 ) : Internal( (double)f ), Type( Class::Floating ){}

        template <typename T>
        JSON( T s, typename enable_if<is_convertible<T,string>::value>::type* = 0 ) : Internal( string( s ) ), Type( Class::String ){}

        JSON( std::nullptr_t ) : Internal(), Type( Class::Null ){}

        static JSON Make( Class type ) {
            JSON ret; ret.SetType( type );
            return ret;
        }

        static JSON Load( const string & );

        template <typename T>
        void append( T arg ) {
            SetType( Class::Array ); Internal.List->emplace_back( arg );
        }

        template <typename T, typename... U>
        void append( T arg, U... args ) {
            append( arg ); append( args... );
        }

        template <typename T>
            typename enable_if<is_same<T,bool>::value, JSON&>::type operator=( T b ) {
                SetType( Class::Boolean ); Internal.Bool = b; return *this;
            }

        template <typename T>
            typename enable_if<is_integral<T>::value && !is_same<T,bool>::value, JSON&>::type operator=( T i ) {
                SetType( Class::Integral ); Internal.Int = i; return *this;
            }

        template <typename T>
            typename enable_if<is_floating_point<T>::value, JSON&>::type operator=( T f ) {
                SetType( Class::Floating ); Internal.Float = f; return *this;
            }

        template <typename T>
            typename enable_if<is_convertible<T,string>::value, JSON&>::type operator=( T s ) {
                SetType( Class::String ); *Internal.String = string( s ); return *this;
            }

        JSON& operator[]( const string &key ) {
            SetType( Class::Object ); return Internal.Map->operator[]( key );
        }

        JSON& operator[]( unsigned index ) {
            SetType( Class::Array );
            if( index >= Internal.List->size() ) Internal.List->resize( index + 1 );
            return Internal.List->operator[]( index );
        }

        JSON &at( const string &key ) {
            return operator[]( key );
        }

        const JSON &at( const string &key ) const {
            return Internal.Map->at( key );
        }

        JSON &at( unsigned index ) {
            return operator[]( index );
        }

        const JSON &at( unsigned index ) const {
            return Internal.List->at( index );
        }

        int length() const {
            if( Type == Class::Array )
                return Internal.List->size();
            else
                return -1;
        }

        bool hasKey( const string &key ) const {
            if( Type == Class::Object )
                return Internal.Map->find( key ) != Internal.Map->end();
            return false;
        }

        int size() const {
            if( Type == Class::Object )
                return Internal.Map->size();
            else if( Type == Class::Array )
                return Internal.List->size();
            else
                return -1;
        }

        Class JSONType() const { return Type; }

        /// Functions for getting primitives from the JSON object.
        bool IsNull() const { return Type == Class::Null; }

        string ToString() const { bool b; return std::move( ToString( b ) ); }
        string ToString( bool &ok ) const {
            ok = (Type == Class::String);
            return ok ? std::move( json_escape( *Internal.String ) ): string("");
        }

        double ToFloat() const { bool b; return ToFloat( b ); }
        double ToFloat( bool &ok ) const {
            ok = (Type == Class::Floating);
            return ok ? Internal.Float : 0.0;
        }

        long ToInt() const { bool b; return ToInt( b ); }
        long ToInt( bool &ok ) const {
            ok = (Type == Class::Integral);
            return ok ? Internal.Int : 0;
        }

        bool ToBool() const { bool b; return ToBool( b ); }
        bool ToBool( bool &ok ) const {
            ok = (Type == Class::Boolean);
            return ok ? Internal.Bool : false;
        }

        JSONWrapper<map<string,JSON>> ObjectRange() {
            if( Type == Class::Object )
                return JSONWrapper<map<string,JSON>>( Internal.Map );
            return JSONWrapper<map<string,JSON>>( nullptr );
        }

        JSONWrapper<deque<JSON>> ArrayRange() {
            if( Type == Class::Array )
                return JSONWrapper<deque<JSON>>( Internal.List );
            return JSONWrapper<deque<JSON>>( nullptr );
        }

        JSONConstWrapper<map<string,JSON>> ObjectRange() const {
            if( Type == Class::Object )
                return JSONConstWrapper<map<string,JSON>>( Internal.Map );
            return JSONConstWrapper<map<string,JSON>>( nullptr );
        }


        JSONConstWrapper<deque<JSON>> ArrayRange() const { 
            if( Type == Class::Array )
                return JSONConstWrapper<deque<JSON>>( Internal.List );
            return JSONConstWrapper<deque<JSON>>( nullptr );
        }

        string dump( int depth = 1, string tab = "  ") const {
            string pad = "";
            for( int i = 0; i < depth; ++i, pad += tab );

            switch( Type ) {
                case Class::Null:
                    return "null";
                case Class::Object: {
                    string s = "{\n";
                    bool skip = true;
                    for( auto &p : *Internal.Map ) {
                        if( !skip ) s += ",\n";
                        s += ( pad + "\"" + p.first + "\" : " + p.second.dump( depth + 1, tab ) );
                        skip = false;
                    }
                    s += ( "\n" + pad.erase( 0, 2 ) + "}" ) ;
                    return s;
                }
                case Class::Array: {
                    string s = "[";
                    bool skip = true;
                    for( auto &p : *Internal.List ) {
                        if( !skip ) s += ", ";
                        s += p.dump( depth + 1, tab );
                        skip = false;
                    }
                    s += "]";
                    return s;
                }
                case Class::String:
                    return "\"" + json_escape( *Internal.String ) + "\"";
                case Class::Floating:
                    return std::to_string( Internal.Float );
                case Class::Integral:
                    return std::to_string( Internal.Int );
                case Class::Boolean:
                    return Internal.Bool ? "true" : "false";
                default:
                    return "";
            }
            return "";
        }

        friend std::ostream& operator<<( std::ostream&, const JSON & );

    private:
        void SetType( Class type ) {
            if( type == Type )
                return;

            ClearInternal();
          
            switch( type ) {
            case Class::Null:      Internal.Map    = nullptr;                break;
            case Class::Object:    Internal.Map    = new map<string,JSON>(); break;
            case Class::Array:     Internal.List   = new deque<JSON>();     break;
            case Class::String:    Internal.String = new string();           break;
            case Class::Floating:  Internal.Float  = 0.0;                    break;
            case Class::Integral:  Internal.Int    = 0;                      break;
            case Class::Boolean:   Internal.Bool   = false;                  break;
            }

            Type = type;
        }

    private:
      void ClearInternal() {
        switch( Type ) {
          case Class::Object: delete Internal.Map;    break;
          case Class::Array:  delete Internal.List;   break;
          case Class::String: delete Internal.String; break;
          default:;
        }
      }

    private:

        Class Type = Class::Null;
};

JSON Array() {
    return std::move( JSON::Make( JSON::Class::Array ) );
}

template <typename... T>
JSON Array( T... args ) {
    JSON arr = JSON::Make( JSON::Class::Array );
    arr.append( args... );
    return std::move( arr );
}

JSON Object() {
    return std::move( JSON::Make( JSON::Class::Object ) );
}

std::ostream& operator<<( std::ostream &os, const JSON &json ) {
    os << json.dump();
    return os;
}

namespace {
    JSON parse_next( const string &, size_t & );

    void consume_ws( const string &str, size_t &offset ) {
        while( isspace( str[offset] ) ) ++offset;
    }

    JSON parse_object( const string &str, size_t &offset ) {
        JSON Object = JSON::Make( JSON::Class::Object );

        ++offset;
        consume_ws( str, offset );
        if( str[offset] == '}' ) {
            ++offset; return std::move( Object );
        }

        while( true ) {
            JSON Key = parse_next( str, offset );
            consume_ws( str, offset );
            if( str[offset] != ':' ) {
                std::cerr << "Error: Object: Expected colon, found '" << str[offset] << "'\n";
                break;
            }
            consume_ws( str, ++offset );
            JSON Value = parse_next( str, offset );
            Object[Key.ToString()] = Value;
            
            consume_ws( str, offset );
            if( str[offset] == ',' ) {
                ++offset; continue;
            }
            else if( str[offset] == '}' ) {
                ++offset; break;
            }
            else {
                std::cerr << "ERROR: Object: Expected comma, found '" << str[offset] << "'\n";
                break;
            }
        }

        return std::move( Object );
    }

    JSON parse_array( const string &str, size_t &offset ) {
        JSON Array = JSON::Make( JSON::Class::Array );
        unsigned index = 0;
        
        ++offset;
        consume_ws( str, offset );
        if( str[offset] == ']' ) {
            ++offset; return std::move( Array );
        }

        while( true ) {
            Array[index++] = parse_next( str, offset );
            consume_ws( str, offset );

            if( str[offset] == ',' ) {
                ++offset; continue;
            }
            else if( str[offset] == ']' ) {
                ++offset; break;
            }
            else {
                std::cerr << "ERROR: Array: Expected ',' or ']', found '" << str[offset] << "'\n";
                return std::move( JSON::Make( JSON::Class::Array ) );
            }
        }

        return std::move( Array );
    }

    JSON parse_string( const string &str, size_t &offset ) {
        JSON String;
        string val;
        for( char c = str[++offset]; c != '\"' ; c = str[++offset] ) {
            if( c == '\\' ) {
                switch( str[ ++offset ] ) {
                case '\"': val += '\"'; break;
                case '\\': val += '\\'; break;
                case '/' : val += '/' ; break;
                case 'b' : val += '\b'; break;
                case 'f' : val += '\f'; break;
                case 'n' : val += '\n'; break;
                case 'r' : val += '\r'; break;
                case 't' : val += '\t'; break;
                case 'u' : {
                    val += "\\u" ;
                    for( unsigned i = 1; i <= 4; ++i ) {
                        c = str[offset+i];
                        if( (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f') || (c >= 'A' && c <= 'F') )
                            val += c;
                        else {
                            std::cerr << "ERROR: String: Expected hex character in unicode escape, found '" << c << "'\n";
                            return std::move( JSON::Make( JSON::Class::String ) );
                        }
                    }
                    offset += 4;
                } break;
                default  : val += '\\'; break;
                }
            }
            else
                val += c;
        }
        ++offset;
        String = val;
        return std::move( String );
    }

    JSON parse_number( const string &str, size_t &offset ) {
        JSON Number;
        string val, exp_str;
        char c;
        bool isDouble = false;
        long exp = 0;
        while( true ) {
            c = str[offset++];
            if( (c == '-') || (c >= '0' && c <= '9') )
                val += c;
            else if( c == '.' ) {
                val += c; 
                isDouble = true;
            }
            else
                break;
        }
        if( c == 'E' || c == 'e' ) {
            c = str[ offset++ ];
            if( c == '-' ){ ++offset; exp_str += '-';}
            while( true ) {
                c = str[ offset++ ];
                if( c >= '0' && c <= '9' )
                    exp_str += c;
                else if( !isspace( c ) && c != ',' && c != ']' && c != '}' ) {
                    std::cerr << "ERROR: Number: Expected a number for exponent, found '" << c << "'\n";
                    return std::move( JSON::Make( JSON::Class::Null ) );
                }
                else
                    break;
            }
            exp = std::stol( exp_str );
        }
        else if( !isspace( c ) && c != ',' && c != ']' && c != '}' ) {
            std::cerr << "ERROR: Number: unexpected character '" << c << "'\n";
            return std::move( JSON::Make( JSON::Class::Null ) );
        }
        --offset;
        
        if( isDouble )
            Number = std::stod( val ) * std::pow( 10, exp );
        else {
            if( !exp_str.empty() )
                Number = std::stol( val ) * std::pow( 10, exp );
            else
                Number = std::stol( val );
        }
        return std::move( Number );
    }

    JSON parse_bool( const string &str, size_t &offset ) {
        JSON Bool;
        if( str.substr( offset, 4 ) == "true" )
            Bool = true;
        else if( str.substr( offset, 5 ) == "false" )
            Bool = false;
        else {
            std::cerr << "ERROR: Bool: Expected 'true' or 'false', found '" << str.substr( offset, 5 ) << "'\n";
            return std::move( JSON::Make( JSON::Class::Null ) );
        }
        offset += (Bool.ToBool() ? 4 : 5);
        return std::move( Bool );
    }

    JSON parse_null( const string &str, size_t &offset ) {
        JSON Null;
        if( str.substr( offset, 4 ) != "null" ) {
            std::cerr << "ERROR: Null: Expected 'null', found '" << str.substr( offset, 4 ) << "'\n";
            return std::move( JSON::Make( JSON::Class::Null ) );
        }
        offset += 4;
        return std::move( Null );
    }

    JSON parse_next( const string &str, size_t &offset ) {
        char value;
        consume_ws( str, offset );
        value = str[offset];
        switch( value ) {
            case '[' : return std::move( parse_array( str, offset ) );
            case '{' : return std::move( parse_object( str, offset ) );
            case '\"': return std::move( parse_string( str, offset ) );
            case 't' :
            case 'f' : return std::move( parse_bool( str, offset ) );
            case 'n' : return std::move( parse_null( str, offset ) );
            default  : if( ( value <= '9' && value >= '0' ) || value == '-' )
                           return std::move( parse_number( str, offset ) );
        }
        std::cerr << "ERROR: Parse: Unknown starting character '" << value << "'\n";
        return JSON();
    }
}

JSON JSON::Load( const string &str ) {
    size_t offset = 0;
    return std::move( parse_next( str, offset ) );
}

} // namespace shinda::utils::json::serializer