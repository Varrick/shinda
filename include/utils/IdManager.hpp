#pragma once

namespace shinda::utils
{
    class IdManager 
    {
        private:
            std::vector<int> unused_;
            std::vector<bool> used_;
            int minId_;
            int maxId_;
            int numIds_;

        public:
            IdManager(const int minId, const int maxId)
            : maxId_(maxId), minId_(minId), numIds_(maxId - minId + 1), unused_(maxId - minId + 1), used_(maxId - minId + 1)
            {
                for (auto i = 0; i <= maxId-minId; ++i) 
                    unused_[i] = i;
            }

            auto getId() -> int
            {
                if (unused_.empty())
                    throw std::runtime_error("There are no free ID's available.");
                auto r = unused_.back();
                used_[r] = true;
                unused_.pop_back();
                return minId_ + r;
            }

            auto releaseId(int id) -> void
            {
                if (id < minId_ || id > maxId_)
                    throw std::runtime_error("ID out of range. Must be between " + std::to_string(minId_) + " and " + std::to_string(maxId_) + "."); 
                id -= minId_;
                if (used_[id] == false)
                    throw std::runtime_error("ID " + std::to_string(id) + " could not be released. Id not in use."); 
                used_[id] = false;
                unused_.push_back(id);
            }

            auto isAnyIdInUse() const -> bool
            {
                return unused_.size() != numIds_;
            }

            auto isAnyIdAvailable() const -> bool
            {
                return unused_.size() > 0;
            }
    };
} // namespace shinda::utils