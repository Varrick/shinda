#pragma once

// StdLib Imports
#include <string>
#include <fstream>
#include <vector>
#include <type_traits>
#include <sys/stat.h>
#include <iostream>
#include <thread>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <time.h>
// Local Imports
#include "macros.hpp"
#include "utils/json.hpp"
#include "memory/SharedMemoryBlock.hpp"

namespace shinda::utils::bench
{
    enum MeasurementType
    {
        UNKNOWN = -1,
        ITERS = 1,
        CAP = 2,
        BASE_CAP = 3
    };

    struct MeasurementDetails
    {
        std::string name{""};
        uint64_t msgSize{0};
        uint64_t iters{0};
        uint16_t numSubs{0};
        uint16_t epochs{0};
        uint8_t logLevel{0};
        uint16_t maxMsgSizePower{0};
        std::string time{""};
        MeasurementType type{UNKNOWN};

        public:
            auto writeToFile(const std::string& filePath) -> void
            {
                try
                {
                    utils::json::serializer::JSON paramsContent = {
                        "Name", name,
                        "MsgSize", msgSize,
                        "Iters", iters,
                        "NumSubs", numSubs,
                        "LogLevel", logLevel,
                        "Time", time,
                        "Type", int(type),
                        "MaxMsgSizePower", maxMsgSizePower,
                        "Epochs", epochs,
                    };

                    std::ofstream paramsFile;
                    paramsFile.open (filePath + std::string("/params.json"));
                    paramsFile << paramsContent << std::endl;
                    paramsFile.close();
                }
                catch(const std::exception& e)
                {
                    throw e;
                }
            }
    };


class CSVWriter final
{
    private:
        std::string filename_;
        std::string delimiter_;
        std::string header_;
        std::string units_;
        std::vector<std::string> dataRows_;
    
    public:
        CSVWriter(const std::string& filename, const std::string& delimiter = ",") 
        : filename_(filename), delimiter_(delimiter) {}
        auto filename()     const -> std::string { return filename_; }
        auto delimiter()    const -> std::string { return delimiter_; }

        template<typename T>
        auto addColumn(const std::vector<T>& data, const std::string& colName, 
                        const std::string& unit ="") -> bool
        {
            if(data.size() > 0)
            {
                bool wasEmpty = dataRows_.size() == 0;
                size_t elemsReq = data.size() - dataRows_.size();
                // Expand number of rows if needed
                for(int i = 0; i < elemsReq; i++)
                {
                    if(wasEmpty)
                        dataRows_.push_back(std::string(""));
                    else
                        dataRows_.push_back(delimiter_);
                }

                // Build rows of data
                header_ += colName + delimiter_;
                units_ += unit + delimiter_;
                try
                {
                    for(int i = 0; i < data.size(); i++)
                        dataRows_[i] += std::to_string(data[i]) + delimiter_;
                }
                catch(std::exception e)
                {
                    LOG_DEBUG(error, "CSVWriter", "Could not add column '" << colName << "'. Error: '" << e.what() << "' happened.");
                    return false;
                }
            }
            else
            {
                LOG_DEBUG(error, "CSVWriter", "Could not add column '" << colName << "'. No data found.");
                return false;
            }
            return true;
        }

        auto writeAll(const bool append = true) -> bool
        {
            try
            {
                // Remove old file and write from scratch
                struct stat buffer;
                if(!append)
                {
                    if(stat((filename_ +".csv").c_str() , &buffer) == 0)
                        remove((filename_ + ".csv").c_str());
                }
                std::ofstream file(filename_ + ".csv");

                // Write all rows to file
                file << header_ << "\n";
                file << units_ << "\n";
                for(auto const& row : dataRows_)
                    file << row << "\n";

                file.close();
                LOG_DEBUG(trace, "CSVWriter", "CSV-File '" << filename_ << ".csv" << "' was written.");
                return true;
            }
            catch(std::exception e)
            {
                LOG_DEBUG(error, "CSVWriter", "Could not write data to '" << filename_ << ".csv'" << ". Error: '" << e.what() << "' happened.");
                return true;
            } 
        }
};

class Measurement final
{
    public:
        std::string name;
        std::string basePath;
        std::string measPath;
        std::string dataPath;
        std::string subPath;
        std::string pubPath;
        MeasurementDetails details;
        CSVWriter csvWriter;

        Measurement(const std::string& name, MeasurementDetails details, 
                    const std::string& basePath = "measurements/")
            : name(name), basePath(basePath), details(details), csvWriter("")
            {
                int opt;
                char timeStr[64];
                time_t t = time(NULL);
                struct tm *tm;
                struct stat st = {0};

                // Get Current Time
                tm = localtime(&t);
                strftime(timeStr, sizeof(timeStr), "%d.%m.%y_%H-%M-%S", tm);

                // Create Measurement BasePath if it does not exist
                if (stat(basePath.c_str(), &st) == -1) 
                    mkdir(basePath.c_str(), 0777);

                // Create Measurement Path if it does not exist
                measPath = basePath
                    + "/[" 
                    + name
                    + "]"
                    + "_time="
                    + timeStr;

                if (stat(measPath.c_str(), &st) == -1) 
                    mkdir(measPath.c_str(), 0777); 

                // Create Measurement Path if it does not exist
                dataPath = std::string(measPath) + std::string("/data/");
                if (stat(dataPath.c_str(), &st) == -1) 
                    mkdir(dataPath.c_str(), 0777); 

                // Write measurement parameters to file
                details.name = name;
                details.time = timeStr;
                
                // Create Publisher/Subscriber Paths if they do not exist
                subPath = std::string(dataPath) + std::string("/subscribers/");
                if (stat(subPath.c_str(), &st) == -1) 
                    mkdir(subPath.c_str(), 0777);    

                pubPath = std::string(dataPath) + std::string("/publishers/");
                if (stat(pubPath.c_str(), &st) == -1) 
                    mkdir(pubPath.c_str(), 0777);
            }   

        auto writeMeasurement() -> void
        {
            details.writeToFile(measPath);
            csvWriter.writeAll();
        }
  };
}   // namespace shinda::utils::bench

namespace shinda::utils::bench::sync
{

class IPC_Barrier
{
    private:
      std::unique_ptr<memory::SharedMemoryBlock<pthread_barrier_t>> memory_;
      pthread_barrier_t barrier_;
      std::string uid_;
      int numP_;

    public:
      IPC_Barrier(const int numP, const std::string& uid = "IPC_BARRIER") : uid_(uid), numP_(numP)
      {
        memory_ = memory::SharedMemoryBlock<pthread_barrier_t>::open(uid_, sizeof(pthread_barrier_t));

        // If first process to create barrier with this uid, initialize it properly
        if(!memory_->wasJoined())
        {
          pthread_barrierattr_t barattr;
          pthread_barrierattr_init(&barattr);
          pthread_barrierattr_setpshared(&barattr, PTHREAD_PROCESS_SHARED);

          // Write Synching Variables To Shared Memory
          if(pthread_barrier_init(memory_->getMemory(), &barattr, numP_) != 0)
              throw std::runtime_error("Barrier initialization failed.");
        }
      }

      auto wait() -> void
      {
        LOG_DEBUG(debug, "IPC_Barrier", "Waiting on Barrier '" << uid_ << "'.");
        int ret = pthread_barrier_wait(memory_->getMemory());
        if(ret != PTHREAD_BARRIER_SERIAL_THREAD && ret != 0)
            throw("Barrier waiting failed.");
        else
          LOG_DEBUG(debug, "IPC_Barrier", "Barrier '" << uid_ << "' was surpassed.");
      }

      auto release() -> void
      {
        if(pthread_barrier_destroy(memory_->getMemory()) != 0)
          throw std::runtime_error("Barrier destruction failed.");
        memory_->releaseShm();
      }

      auto numP() const  ->  const int { return numP_; }
      auto uid()  const  ->  std::string { return uid_; }
};

} // namespace shinda::utils::bench::sync