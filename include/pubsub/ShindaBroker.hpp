#pragma once

// 3rd Party Imports
#include "SocketMessages.pb.h"
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
// Local Imports
#include "pubsub/Topic.hpp"
#include "socket/SocketServer.hpp"
#include "macros.hpp"
#include "exceptions.hpp"
#include "status_codes.hpp"
#include "utils/json.hpp"
#include "utils/math.hpp"
#include "constants.hpp"

namespace shinda::pubsub 
{  
    /** @class ShindaBroker
     * @brief Negotiates setup and maintains Topics and therefore Shared Memory Regions via UDS-Sockets with other processes.
     * 
     * A ShindaBroker process must be running in the background to allow clients to share data via shared memory.
     * In order to achieve this, Client processes may request creation or access to shared memory via UDS-Sockets from the ShindaBroker.
     * The ShindaBroker himself keeps internally track of all Topic objects (and therefore SharedMemoryBlock objects) created and which processes are currently using them.
     * If a Topic is not longer used by at least one process, it gets automatically deleted.
     */
    class ShindaBroker : public socket::server::ISocketServerCallbacks
    {
        private:
            std::unique_ptr<socket::server::ISocketServer> server_ {socket::server::SocketServerFactory::create()};
            std::vector<std::unique_ptr<Topic>> topics_;
            std::vector<TopicBlueprint> topicsAllowed_;
            std::map<size_t, size_t> sessionPIDMap_;     
            size_t maxUsableMem_;
            size_t maxTopicMem_;
            bool secureMode_;

            size_t essentialTopicsMem_ {0};
            size_t nonEssentialTopicsMem_ {0};

        public:
            /** Constructor
             * 
             * @param configPath Path to the config.json file of shinda
             */
            ShindaBroker() = delete;
            ShindaBroker(const ShindaBroker&) = delete;
            ShindaBroker(ShindaBroker&&) = delete;
            ShindaBroker(std::string configPath = std::string(constants::kDefaultConfigPath));
            ~ShindaBroker();

            /** Signals the ShindaBroker to stop its execution.
             * 
             * All Topic objects will be destroyed and Client processes will be informed about the shutdown.
             * 
             * @returns Nothing.
             * @warning Call this method only for cleanup if you application has finished its execution in which the ShindaBroker is needed.
             */
            auto stop()             ->  void;

            /** Blocks until the server thread has finished execition
             * 
             * Waits until the server thread has finished execition. This will block the calling thread.
             * 
             * @returns Nothing.
             */
            auto join()             ->  void;

        private:  
            auto receivedPlatformMessage(int sessionId, const std::string&& msg)                -> void override;
            auto onJoin(int sessionId, const SocketMsgs::JoinRequest& recMsg)                   -> void;
            auto onUnregister(int sessionId, const SocketMsgs::UnregisterRequest& recMsg)       -> void;
            auto onConnect(int sessionId, const SocketMsgs::ConnectRequest& recMsg)             -> void;
            auto onDisconnect(int sessionId)                                                    -> void;

    };  // Class ShindaBroker
}   // Namespace shinda::pubsub
    
    
