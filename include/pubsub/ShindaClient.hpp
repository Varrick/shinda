#pragma once

// 3rd Party Imports
#include "SocketMessages.pb.h"
// Local Imports
#include "socket/SocketClient.hpp" 
#include "socket/SocketNotifier.hpp"
#include "macros.hpp"
#include "memory/SharedRingBufferWrapper.hpp"
#include "memory/SharedMessage.hpp"
#include "pubsub/ShindaUser.hpp"
#include "status_codes.hpp"

namespace shinda::pubsub 
{
    /** @class ShindaClient
     * @brief Main interface to the user of methods for transferring data through shared memory.
     * 
     * Offers methods for writing/reading to/from shared memory. The ShindaClient class
     * will internally negotiate the shared memory with the Broker process, which needs to be running.
     */
    template<typename T>
    class ShindaClient : public socket::client::ISocketClientCallbacks
    {
        private:
            std::unique_ptr<socket::client::ISocketClient> sock_ {socket::client::SocketClientFactory::create()}; 
            memory::SharedMsg msgOut_;
            socket::SocketNotifier sockAwait_;
            std::unique_ptr<memory::SharedRingBufferWrapper> ringBuf_;
            size_t maxDataSize_;
            size_t maxMsgSize_;
            int subTimeout_;
            int pubTimeout_;  
            std::string topic_;
            std::string shmUid_;

            bool isPublisher_ {false};
            bool isSubscriber_ {false};
            bool isConnected_ {false};
            pid_t pid_ {getpid()};
            
        public:
            ShindaClient() = delete;
            ShindaClient(const ShindaClient&) = delete;
            ShindaClient(ShindaClient&&) = delete;

            /**
            * Constructor
            * 
            * @param topic_ Topic to which this client shall connect
            * @param maxDataSize_ Maximum data size for a publish in byte
            * @param pubTimeoutUs Timeout in microseconds for publishing (0 equals waiting/blocking forever)
            * @param subTimeoutUs Timeout in microseconds for receiving (0 equals waiting/blocking forever)
            */
            ShindaClient(std::string topic, size_t maxDataSize, 
                        int pubTimeout = constants::kDefaultPubTimeoutUs, 
                        int subTimeout = constants::kDefaultSubTimeoutUs);
            ~ShindaClient();

            /** Tries to publish a message to the shared memory and blocks until the data could be written.
             * 
             * This method tries to publish a message to the shared memory associated by its Topic and will block indefinetly if 
             * the buffer stays full.
             * If the Topic does not yet exist, it will automatically be created and the message will be published afterwards.
             * If the Topic does already exist internally through creation by other processes
             * and there is already another process publishing to this Topic, the method will 
             * return false, because only one publisher can exist for a Topic at any time.
             * 
             * @param data Pointer to the data, which shall be published. Pointed data is not allowed to contain pointers. This will result in undefiend behaviour.
             * @param dataSize Size of the data, which the data pointer points to.
             * @returns StatusCode showing if publishing of the data to shared memory was successfull.
             * @retval SUCCESS Data was published successfully.
             * @retval NO_CONNECTION If the connection to the Broker is currently not established.
             * @retval SOCKET_TIMEOUT If the request to the Broker for joining the Topic as publisher timed out.
             * @retval TOPIC_DOES_NOT_EXIST If the Topic you want to join as publisher does currently not exist.
             * @retval MSG_SIZE_MISMATCH If the data you want to publish is larger then the maximum allowed data size for the Topic.
             * @retval BROKER_ERROR If the Broker encounters a critical error on the request for joining the Topic as publisher.
             * @warning Data is not allowed to contain any pointers or objects/data structures that contain pointers themselves. If pointers get stored in shared memory,
             * they refer to the address space of the associated process. Other process have other address spaces, so the pointer(s) would point to invalid locations for other processes.
             * @warning Publish will indefinetly try to write a message and thereby block further program execution
            */
            auto publish(const T* data, size_t dataSize) -> status::StatusCode;

            /** Tries to publish a message to the shared memory and returns immediatly if the buffer is full
             * 
             * This method tries to publish a message to the shared memory associated by its Topic and will return immediatly
             * if the buffer is full and publishing could not be done.
             * If the Topic does not yet exist, it will
             * automatically be created and the message will be published afterwards.
             * If the Topic does already exist internally through creation by other processes
             * and there is already another process publishing to this Topic, the method will 
             * return false, because only one publisher can exist for a Topic at any time.
             * 
             * @param data Pointer to the data, which shall be published. Pointed data is not allowed to contain pointers. This will result in undefiend behaviour.
             * @param dataSize Size of the data, which the data pointer points to.
             * @returns StatusCode showing if publishing of the data to shared memory was successfull.
             * @retval SUCCESS Data was published successfully.
             * @retval BUFFER_FULL If no data could be published to shared memory currently.
             * @retval NO_CONNECTION If the connection to the Broker is currently not established.
             * @retval SOCKET_TIMEOUT If the request to the Broker for joining the Topic as publisher timed out.
             * @retval TOPIC_DOES_NOT_EXIST If the Topic you want to join as publisher does currently not exist.
             * @retval MSG_SIZE_MISMATCH If the data you want to publish is larger then the maximum allowed data size for the Topic.
             * @retval BROKER_ERROR If the Broker encounters a critical error on the request for joining the Topic as publisher.
             * @warning Data is not allowed to contain any pointers or objects/data structures that contain pointers themselves. If pointers get stored in shared memory,
             * they refer to the address space of the associated process. Other process have other address spaces, so the pointer(s) would point to invalid locations for other processes.
            */
            auto try_publish(const T* data, size_t dataSize) -> status::StatusCode;

            /** Tries to read a message from the shared memory until data is available
             * 
             * This method tries to read a message from the shared memory which is associated by its Topic until it receives data and can therefore block forever.
             * If the Topic does not yet exist, it will automatically be created. Calls to this method will only yield results, if 
             * a publisher process is publishing messages to the same Topic.
             * 
             * @param data Output parameter which will be set to point to the received data
             * @returns status::StatusCode showing if call was successfull and if data is valid. 
             * @retval SUCCESS Data was read successfully.ly.
             * @retval NO_CONNECTION If the connection to the Broker is currently not established.
             * @retval SOCKET_TIMEOUT If the request to the Broker for joining the Topic as subscriber timed out.
             * @retval TOPIC_DOES_NOT_EXIST If the Topic you want to join as subscriber does currently not exist.
             * @retval BROKER_ERROR If the Broker encounters a critical error on the request for joining the Topic as subcriber.
             * @warning Output parameter contains only valid data if status::StatusCode::SUCCESS has happened.
             * @warning Spin will indefinetly try to read a message and thereby block further program execution
            */
            auto spin(T*& data) -> status::StatusCode;

            /** Tries to read a message from the shared memory and will return immediatly if the buffer is empty
             * 
             * This method tries to read a message from shared memory which is associated by its Topic and will return immediatly if
             * the buffer is empty indicating the status code 
             * If the Topic does not yet exist, it will automatically be created. Calls to this method will only yield results, if 
             * a publisher process is publishing messages to the same Topic.
             * 
             * @param data Output parameter which will be set to point to the received data
             * @returns status::StatusCode showing if call was successfull and if data is valid. 
             * @retval SUCCESS Data was read successfully.
             * @retval BUFFER_EMPTY Reading from shared memory was not possible because the buffer is empty.
             * @retval NO_CONNECTION If the connection to the Broker is currently not established.
             * @retval SOCKET_TIMEOUT If the request to the Broker for joining the Topic as subscriber timed out.
             * @retval TOPIC_DOES_NOT_EXIST If the Topic you want to join as subscriber does currently not exist.
             * @retval BROKER_ERROR If the Broker encounters a critical error on the request for joining the Topic as subcriber.
             * @warning Output parameter contains only valid data if status::StatusCode::SUCCESS has happened.
             * @warning Spin will indefinetly try to read a message and thereby block further program execution
             */
            auto try_spin(T*& data) -> status::StatusCode;

            /** Tries to connect to the Broker as blocking call
             * 
             * Starts setting up the socket for communicating with the ShindaBroker. Initiates creation of the Topic and blocks until constants::kDefaultSocketTimeoutMs was reached or 
             * connecting was achieved successfully.
             * 
             * @returns status::StatusCode showing if the connect request to the Broker was successfull.
             * @retval SUCCESS Connection was successfully established.
             * @retval TOPIC_FULL If the Topic of this client already exists and no more slots for subscribers/publishers are available.
             * @retval MSG_SIZE_MISMATCH If the Topic of this client already exists but with another maximum message size which is incompatible.
             * @retval SOCKET_TIMEOUT If the request to the Broker for connecting timed out.
             * @retval NOT_ENOUGH_MEMORY Not enough memory available to create Topic.   
             * @retval TOPIC_NOT_ALLOWED Topic was not defined in config.json and is therefore not allowed to be created.
             * @retval TOPIC_SIZE_NOT_ALLOWED  Maximum size of a Topic defined in config.json was surpassed.
             */
            auto connect() -> status::StatusCode;

            /** Tries to stop publishing/subscribing to a Topic
             * 
             * Sends an unjoin request to the Broker for stopping to subscribe/publish to the Topic of this ShindaClient. Blocks until constants::kDefaultSocketTimeoutMs is reached or 
             * unregistering was achieved successfully.
             * 
             * @returns status::StatusCode showing if the unjoin request to the Broker was successfull.
             * @retval SUCCESS Unregistering from the Topic as subscriber/publisher was successfull.
             * @retval NO_CONNECTION If the connection to the Broker is currently not established.
             * @retval SOCKET_TIMEOUT If the request to the Broker for unregistering timed out.
             * @retval TOPIC_DOES_NOT_EXIST If the Topic you want to unjoin as publisher/subscriber does currently not exist.
             * @retval BROKER_ERROR If the Broker encounters a critical error on the request for unregistering the Topic as publisher/subcriber.
             */
            auto unjoin(ShindaUserType userType) -> status::StatusCode;

            auto isPublisher()      const -> bool;
            auto isSubscriber()     const -> bool;
            auto isConnected()      const -> bool;
            auto getTopic()         const -> std::string;

        private:      
            auto join(ShindaUserType usrType) -> status::StatusCode;

            auto sendConnect()                                      const   -> void;
            auto sendJoin(ShindaUserType usrType)                   const   -> void;
            auto sendUnregister(ShindaUserType usrType)             const   -> void;

            auto receivedPlatformMessage(const std::string&& msg)           -> void override;
            auto onConnect(const SocketMsgs::ConnectReply& recMsg)          -> void;
            auto onDisconnect()                                             -> void;
            auto onJoin(const SocketMsgs::JoinReply& recMsg)                -> void;
            auto onUnregister(const SocketMsgs::UnregisterReply& recMsg)    -> void;

    };  // Class ShindaClient


    template<typename T>
    ShindaClient<T>::ShindaClient(const std::string topic, const size_t maxDataSize, 
                                    const int pubTimeout, const int subTimeout)
    : subTimeout_(subTimeout), pubTimeout_(pubTimeout), 
        maxMsgSize_(memory::SharedMsgHeader::createFromDataSize(maxDataSize).msgSize), topic_(topic), maxDataSize_(maxDataSize)
        {  
            sock_->registerCallbacks(this);
        }
    
    template<typename T>
    ShindaClient<T>::~ShindaClient()
    {
        try {
            if(isPublisher_)
                this->unjoin(ShindaUserType::PUBLISHER);
            if(isSubscriber_)
                this->unjoin(ShindaUserType::SUBSCRIBER);
            sock_->stop();
            LOG_DEBUG(info, "ShindaClient #" << getpid(), "Socket stopped.");   
        }
        catch(const std::exception& e) {
            LOG_APP(error, "ShindaClient #" << getpid(), "Socket could not be stopped. Exception: " << e.what());   
        }
    
        LOG_APP(info, "ShindaClient #" << getpid(), "Stopped."); 
    }

    template<typename T>
    inline auto ShindaClient<T>::publish(const T *const data, const size_t dataSize) -> status::StatusCode
    {
        if(isConnected_ && isPublisher_)  
        { 
            if(dataSize <= maxDataSize_) 
                return ringBuf_->enqueue(data, dataSize, pubTimeout_); 
            else {
                LOG_APP(error, "ShindaClient #" << getpid(), "Publishing failed. Datasize (" << dataSize << " Bytes) must be smaller than " << maxDataSize_ << " Bytes.");
                return status::StatusCode::MSG_SIZE_MISMATCH;   
            }
        }
        else if(isConnected_ && !isPublisher_)    
        {     
            if(auto st = this->join(ShindaUserType::PUBLISHER); st != status::StatusCode::SUCCESS) {
                return st;
            }
            else 
                return this->publish(data, dataSize);    
        }  
        else {
            LOG_APP(error, "ShindaClient #" << getpid(), "Publishing failed. Broker not reachable.");
            return status::StatusCode::NO_CONNECTION;
        }     
    }

    template<typename T>
    inline auto ShindaClient<T>::try_publish(const T *const data, const size_t dataSize) -> status::StatusCode
    {
        if(isConnected_ && isPublisher_)  
        { 
            if(dataSize <= maxDataSize_) 
                return ringBuf_->try_enqueue(data, dataSize); 
            else {
                LOG_APP(error, "ShindaClient #" << getpid(), "Publishing failed. Datasize (" << dataSize << " Bytes) must be smaller than " << maxDataSize_ << " Bytes.");
                return status::StatusCode::MSG_SIZE_MISMATCH;   
            }
        }
        else if(isConnected_ && !isPublisher_)    
        {     
            if(auto st = this->join(ShindaUserType::PUBLISHER); st != status::StatusCode::SUCCESS) {
                return st;
            }
            else 
                return this->try_publish(data, dataSize);    
        }  
        else {
            LOG_APP(error, "ShindaClient #" << getpid(), "Publishing failed. Broker not reachable.");
            return status::StatusCode::NO_CONNECTION;
        }     
    }

    template<typename T>
    inline auto ShindaClient<T>::spin(T*& data) -> status::StatusCode
    {
        if(isConnected_ && isSubscriber_)  
        {
            data = static_cast<T*>(msgOut_.data);  
            return ringBuf_->dequeue(&msgOut_, subTimeout_);
        }
        else if(isConnected_ && !isSubscriber_)
        {
            if(auto st = this->join(ShindaUserType::SUBSCRIBER); st != status::StatusCode::SUCCESS) {
                return st;
            }
            else 
                return this->spin(data);   
        }
        else {
            LOG_APP(error, "ShindaClient #" << getpid(), "Spin failed. Broker not reachable.");
            return status::StatusCode::NO_CONNECTION;
        }
    }

    template<typename T>
    inline auto ShindaClient<T>::try_spin(T*& data) -> status::StatusCode
    {
        if(isConnected_ && isSubscriber_)  
        {
            data = static_cast<T*>(msgOut_.data);  
            return ringBuf_->try_dequeue(&msgOut_);
        }
        else if(isConnected_ && !isSubscriber_)
        {
            if(auto st = this->join(ShindaUserType::SUBSCRIBER); st != status::StatusCode::SUCCESS) {
                return st;
            }
            else 
                return this->try_spin(data);   
        }
        else {
            LOG_APP(error, "ShindaClient #" << getpid(), "Spin failed. Broker not reachable.");
            return status::StatusCode::NO_CONNECTION;
        }
    }

    template<typename T>
    auto ShindaClient<T>::connect() -> status::StatusCode
    {
        if(!isConnected_) {
            sock_->start();
            sendConnect();
            return sockAwait_.wait();;
        }
        else
            return status::StatusCode::SUCCESS;
    }

    template<typename T>
    auto ShindaClient<T>::unjoin(const ShindaUserType userType) -> status::StatusCode
    {
        if(isConnected_) {
            if(userType == ShindaUserType::PUBLISHER)
                this->sendUnregister(ShindaUserType::PUBLISHER);
            else if(userType == ShindaUserType::SUBSCRIBER)
                this->sendUnregister(ShindaUserType::SUBSCRIBER);
            
            return sockAwait_.wait();
        }
        else
            return status::StatusCode::NO_CONNECTION;
    }

    template<typename T>
    auto ShindaClient<T>::isPublisher() const -> bool { return isPublisher_; }

    template<typename T>
    auto ShindaClient<T>::isSubscriber() const -> bool { return isSubscriber_; }

    template<typename T>
    auto ShindaClient<T>::isConnected() const -> bool { return isConnected_; }

    template<typename T>
    auto ShindaClient<T>::getTopic() const -> std::string { return topic_; }
  
    template<typename T>
    auto ShindaClient<T>::join(const ShindaUserType usrType) -> status::StatusCode
    {
        this->sendJoin(usrType);  
        return sockAwait_.wait();  
    }

    template<typename T>
    auto ShindaClient<T>::sendConnect() const -> void
    {
        LOG_DEBUG(info, "ShindaClient #" << getpid(), "Sending Message '" << "Connect" << "' for Topic '" << topic_ << "'."); 
        SocketMsgs::SocketMsg msgWrap;
        std::string serializedMsg;
        msgWrap.mutable_connectrequest()->set_pid(pid_);
        msgWrap.mutable_connectrequest()->set_topicname(topic_);
        msgWrap.mutable_connectrequest()->set_maxdatasize(maxDataSize_);
        msgWrap.SerializeToString(&serializedMsg);
        sock_->sendMessage(serializedMsg);
    }

    template<typename T>
    inline auto ShindaClient<T>::sendJoin(const ShindaUserType usrType) const -> void
    {
        LOG_DEBUG(info, "ShindaClient #" << getpid(), "Request Broker for joining Topic '" << topic_ << "' as " << userTypeToString(usrType) << "...");
        LOG_DEBUG(info, "ShindaClient #" << getpid(), "Sending Message '" << "JoinTopic" << "' for Topic '" << topic_ << "' as " << userTypeToString(usrType) << "...");
        SocketMsgs::SocketMsg msgWrap;
        std::string serializedMsg;
        msgWrap.mutable_joinrequest()->set_ispublisher(static_cast<int>(usrType));
        msgWrap.mutable_joinrequest()->set_pid(pid_);
        msgWrap.mutable_joinrequest()->set_topicname(topic_);
        msgWrap.SerializeToString(&serializedMsg);
        sock_->sendMessage(serializedMsg);
    }

    template<typename T>
    auto ShindaClient<T>::sendUnregister(const ShindaUserType usrType) const -> void
    {
        LOG_DEBUG(info, "ShindaClient #" << getpid(), "Sending Message '" << "UnjoinTopic" << "' for Topic '" << topic_ << "' as " << userTypeToString(usrType) << "...");
        SocketMsgs::SocketMsg msgWrap;
        std::string serializedMsg;
        msgWrap.mutable_unregisterrequest()->set_pid(pid_);
        msgWrap.mutable_unregisterrequest()->set_topicname(topic_);
        msgWrap.mutable_unregisterrequest()->set_ispublisher(static_cast<int>(usrType));
        msgWrap.SerializeToString(&serializedMsg);
        sock_->sendMessage(serializedMsg);
    }

    template<typename T>
    auto ShindaClient<T>::onConnect(const SocketMsgs::ConnectReply& recMsg) -> void
    {
        status::StatusCode st = static_cast<status::StatusCode>(recMsg.rc());

        if(st == status::StatusCode::SUCCESS) 
        {
            LOG_APP(info, "ShindaClient #" << getpid(), "Connection to Broker established.");
            ringBuf_ = memory::SharedRingBufferWrapper::join(recMsg.shmuid());
            shmUid_ = recMsg.shmuid();
            isConnected_ = true;
        }
        else {
            LOG_APP(error, "ShindaClient #" << getpid(), "Connecting to Broker with Topic '" << topic_ << "' failed. Request to Broker failed with StatusCode '" << statusCodeToString(st) << "'.");
        }

        sockAwait_.notify(st);
    }

    template<typename T>
    auto ShindaClient<T>::onDisconnect() -> void
    {
        LOG_APP(error, "ShindaClient #" << getpid(), "Disconnect from Broker happened!");
        isConnected_ = isPublisher_ = isSubscriber_ = false;
        ringBuf_->close();
        ringBuf_.reset();
        sock_->stop();
    }

    template<typename T>
    auto ShindaClient<T>::onJoin(const SocketMsgs::JoinReply& recMsg) -> void
    {
        status::StatusCode st = static_cast<status::StatusCode>(recMsg.rc());

        if(st == status::StatusCode::SUCCESS)
        {
            if(msgOut_.hdr.msgSize != maxMsgSize_) {
                msgOut_ = memory::SharedMsgHeader::createFromMsgSize(maxMsgSize_);
            }

            // If we want to join as publisher
            if(recMsg.ispublisher()) {
                isPublisher_ = true;
                ringBuf_->joinProducer(recMsg.id());
                LOG_APP(info, "ShindaClient #" << getpid(), "Joined Topic '" << topic_ << "' as Publisher.");
            } 
            // If we want to join as subscriber
            else {   
                isSubscriber_ = true;
                ringBuf_->joinConsumer(recMsg.id());
                LOG_APP(info, "ShindaClient #" << getpid(), "Joined Topic '" << topic_ << "' as Subscriber."); 
            }           
        }
        else
        {
            if(recMsg.ispublisher()) {
                LOG_APP(error, "ShindaClient #" << getpid(), "Joining as Publisher to Topic '" << topic_ << "' failed. Request to Broker failed with StatusCode '" << statusCodeToString(st) << "'.");
            }
            else {
                LOG_APP(error, "ShindaClient #" << getpid(), "Joining as Subscriber to Topic '" << topic_ << "' failed. Request to Broker failed with StatusCode '" << statusCodeToString(st) << "'.");
            }
        }
        sockAwait_.notify(st);
    }

    template<typename T>
    auto ShindaClient<T>::onUnregister(const SocketMsgs::UnregisterReply& recMsg) -> void
    {
        status::StatusCode st = static_cast<status::StatusCode>(recMsg.rc());

        if(st == status::StatusCode::SUCCESS)
        {
            // If we want to unjoin as publisher
            if(recMsg.ispublisher()) {
                isPublisher_ = false;
                ringBuf_->unregisterProducer();
                LOG_APP(info, "ShindaClient #" << getpid(), "Unjoined Topic '" << topic_ << "' as Publisher.");
            } 
            // If we want to unjoin as subscriber
            else {   
                isSubscriber_ = false;
                ringBuf_->unregisterConsumer();
                LOG_APP(info, "ShindaClient #" << getpid(), "Unjoined Topic '" << topic_ << "' as Subscriber."); 
            }           
        }
        else
        {
            if(recMsg.ispublisher()) {
                LOG_APP(error, "ShindaClient #" << getpid(), "Unregistering as Publisher from Topic '" << topic_ << "' failed. Request to Broker failed with StatusCode '" << statusCodeToString(st) << "'.");
            }
            else {
                LOG_APP(error, "ShindaClient #" << getpid(), "Unregistering as Subscriber from Topic '" << topic_ << "' failed. Request to Broker failed with StatusCode '" << statusCodeToString(st) << "'.");
            }
        }
        sockAwait_.notify(st);
    }

    template<typename T>
    auto ShindaClient<T>::receivedPlatformMessage(const std::string&& msg) -> void
    {   
        if(msg.empty()) // Disconnect happened
            onDisconnect();
        else
        {
            SocketMsgs::SocketMsg recMsg;
            recMsg.ParseFromString(msg);

            switch(recMsg.msg_case())
            {
                case SocketMsgs::SocketMsg::kConnectReply:
                    LOG_DEBUG(info, "ShindaClient #" << getpid(), "Message '" << "ConnectReply" << "' received.");
                    onConnect(recMsg.connectreply());
                    break;
                case SocketMsgs::SocketMsg::kJoinReply:
                    LOG_DEBUG(info, "ShindaClient #" << getpid(), "Message '" << "JoinReply" << "' received.");
                    onJoin(recMsg.joinreply());
                    break;
                case SocketMsgs::SocketMsg::kUnregisterReply:
                    LOG_DEBUG(info, "ShindaClient #" << getpid(), "Message '" << "UnregisterReply" << "' received.");
                    onUnregister(recMsg.unregisterreply());
                    break;
                default:
                    LOG_DEBUG(info, "ShindaClient #" << getpid(), "Unknown Message received.");
                    break;
            }
        }
    }

}   // Namespace shinda::pubsub
    
    
