#pragma once

#include <string>

namespace shinda::pubsub
{
    enum class ShindaUserType
    {
        SUBSCRIBER = 0,
        PUBLISHER,
    };

    const static char* userTypeToString(ShindaUserType t) noexcept
    {
        switch (t)
        {
            case ShindaUserType::SUBSCRIBER: return "Subscriber";
            case ShindaUserType::PUBLISHER: return "PUBLISHER";
            default: return "User type not found";
        }
    }

    struct ShindaUser {
        protected:
            int pid_;
            int id_;
            ShindaUserType userType_;

        public:
            ShindaUser();
            ShindaUser(std::string topic, int pid, int id, ShindaUserType userType);

            auto getPid()        const   ->  int;
            auto getId()         const   ->  int;
            auto getUserType()   const   ->  ShindaUserType;
    };
}   // namespace shinda::pubsub