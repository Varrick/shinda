#pragma once

// 3rd Party Imports
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/uuid_generators.hpp>
// Local Imports
#include "memory/SharedRingBufferWrapper.hpp"
#include "memory/SharedRingBuffer.hpp"
#include "pubsub/ShindaUser.hpp"
#include "utils/IdManager.hpp"
#include "utils/math.hpp"

namespace shinda::pubsub
{
    class TopicBlueprint
    {
        protected:
            std::string name_;
            int numSegments_;
            size_t maxDataSize_;
            size_t maxMsgSize_;
            size_t totalSize_;
            bool isEssential_;

        public:
            TopicBlueprint(std::string name, int numSegments, size_t maxDataSize, bool isEssential);

            auto getName()              const   -> const std::string;
            auto getNumSegments()       const   -> const int;
            auto getMaxDataSize()       const   -> const size_t;
            auto getMaxMsgSize()        const   -> const size_t;
            auto getTotalSize()         const   -> const size_t;
            auto getIsEssential()       const   -> const bool;
    };

    class Topic : public TopicBlueprint
    {
        private:
            // Process Management
            pid_t creator_;
            ShindaUser publisher_;
            std::vector<ShindaUser> subscribers_;
            utils::IdManager subIdManager_;
            utils::IdManager pubIdManager_;

            // Ringbuffer Management
            std::string shmUid_ {boost::uuids::to_string(boost::uuids::random_generator()())};
            int maxNumSubs_;
            std::unique_ptr<memory::SharedRingBufferWrapper> ringBuf_;
     
        public:
            Topic() = delete;
            Topic(const Topic&) = delete;
            Topic(Topic&&) = delete;
            Topic(std::string topic, size_t maxDataSize, int numSegments, int maxNumSubs, pid_t creator, bool isEssential);

            // Manage Buffer Members
            auto addSubscriber(int pid)                     -> bool;
            auto removeSubscriber(int pid)                  -> bool;
            auto setPublisher(int pid)                      -> bool;
            auto setPublisher(int pid, size_t maxMsgSize)   -> bool;
            auto removePublisher(int pid)                   -> bool;

            // Manipulate Buffer
            auto release()                                  -> void;

            // Observe Buffer Members
            auto hasPublisher()                     const   -> bool;
            auto hasSubscribers()                   const   -> bool;
            auto isFull()                           const   -> bool;
            auto isEmpty()                          const   -> bool;
            auto logStatus()                        const   -> void;

            // Getter
            auto getSubscriberByPid(int pid)        const   -> const ShindaUser;
            auto getSubscriberById(int id)          const   -> const ShindaUser;
            auto getPublisher()                     const   -> const ShindaUser;
            auto getCreator()                       const   -> const pid_t;
            auto getShmUid()                        const   -> const std::string;
            auto getNumSubs()                       const   -> const int;
            auto getMaxNumSubs()                    const   -> const int;
    }; 
}   // namespace shinda::pubsub