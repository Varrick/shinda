#pragma once

// StdLib Imports
#include <iostream>
#include <memory>
#include <cstring>
#include <stdexcept>
#include <chrono>
// 3rd Party Imports
#include <boost/log/trivial.hpp>
// Local Imports
#include "SharedRingBuffer.hpp"
#include "SharedMessage.hpp"
#include "SharedMemoryBlock.hpp"
#include "macros.hpp"
#include "status_codes.hpp"
#include "exceptions.hpp"
#include "utils/math.hpp"
#include "constants.hpp"

namespace shinda::memory
{
    class SharedRingBufferWrapper
    { 
        private:
            std::unique_ptr<SharedMemoryBlock<SharedRingBuffer>> mem_;
            int pubId_;
            int subId_;
            
            // Variables for control of buffer & faster access
            SharedRingBuffer* buf_;
            SharedMsgHeader currHdr_;
            SharedMsgHeader* currHdrAddr_; 
            std::vector<char*> msgsAddrLut_;  
            char* currAddr_;
            int currHead_;
            int nextHead_;
            int currTail_;
            int nextTail_;

            SharedRingBufferWrapper(std::string shmUid, size_t maxMsgSize, size_t numSegments);
            SharedRingBufferWrapper(std::string shmUid);

        public:
            SharedRingBufferWrapper() = delete;
            SharedRingBufferWrapper(const SharedRingBufferWrapper&) = delete;
            SharedRingBufferWrapper(SharedRingBufferWrapper&&) = delete;
            auto static create(std::string shmUid, size_t maxMsgSize, size_t numSegments) -> std::unique_ptr<SharedRingBufferWrapper>;
            auto static join(std::string shmUid)                                          -> std::unique_ptr<SharedRingBufferWrapper>;
            
            // ------------------ Write/Read Buffer ------------------------
            auto enqueue(const void* data, size_t dataSize, int timoutMicros)   -> status::StatusCode;   
            auto try_enqueue(const void* data, size_t dataSize)                 -> status::StatusCode;                

            auto dequeue(memory::SharedMsg* msgRead, int timoutMicros)          -> status::StatusCode;
            auto try_dequeue(memory::SharedMsg* msgRead)                        -> status::StatusCode;
            
            // ------------------ Process Management -----------------------
            auto close()                -> void;
            auto release()              -> void;
            auto createConsumer(int id) -> void;
            auto removeConsumer(int id) -> void;
            auto joinProducer(int id)   -> void;
            auto joinConsumer(int id)   -> void;
            auto unregisterProducer()   -> void;
            auto unregisterConsumer()   -> void;

        private:
            // ------------------ Write/Read Buffer ------------------------
            auto isMsgReadable()                                const -> bool;
            auto isMsgWritable()                                      -> bool;
            auto updateTail(int producerPos)                          -> int;
            auto writeMessage(const void* data, size_t dataSize)      -> void;
            auto readMessage(memory::SharedMsg* msgRead)              -> void;
    };

        // ------------------ Write/Read Buffer ------------------------
    inline auto SharedRingBufferWrapper::enqueue(const void* const data, 
                                          const size_t dataSize, 
                                          const int timoutMicros) -> status::StatusCode
    { 
        // COMPUTE NEXT SEGMENT IN BUFFER
        nextHead_ = buf_->getNextPos(currHead_);
        
        // POLL UNTIL NEXT MSG WRITABLE
        LOG_DEBUG(info, "SharedRingBuffer", "To Write: " << nextHead_ << " | LastTail: " << buf_->lastTail);
        while(!isMsgWritable()) { 
            sched_yield();
        }

        // COPY MESSAGE TO SHARED MEMORY
        writeMessage(data, dataSize);

        // MOVE TO NEXT SEGMENT IN BUFFER
        buf_->producer.position() = nextHead_;
        currHead_ = nextHead_;

        return status::StatusCode::SUCCESS;
    }       
    inline auto SharedRingBufferWrapper::try_enqueue(const void* const data, size_t dataSize) -> status::StatusCode
    { 
        // COMPUTE NEXT SEGMENT IN BUFFER
        nextHead_ = buf_->getNextPos(currHead_);
        
        // CHECK ONCE IF NEXT MSG WRITABLE
        LOG_DEBUG(info, "SharedRingBuffer", "Try to Write: " << nextHead_ << " | LastTail: " << buf_->lastTail);
        if(!isMsgWritable()) {
            LOG_DEBUG(trace, "SharedRingBuffer", "Buffer is full. (Head: " << currHead_   << ").");  
            return status::StatusCode::BUFFER_FULL; 
        }      

        // COPY MESSAGE TO SHARED MEMORY
        writeMessage(data, dataSize);

        // MOVE TO NEXT SEGMENT IN BUFFER
        buf_->producer.position() = nextHead_;
        currHead_ = nextHead_;

        return status::StatusCode::SUCCESS;
    }                  

    inline auto SharedRingBufferWrapper::dequeue(memory::SharedMsg *const msgRead, const int timoutMicros) -> status::StatusCode
    {
        // COMPUTE NEXT SEGMENT TO READ FROM
        nextTail_ = buf_->getNextPos(currTail_);

        // POLL UNTIL NEXT MSG READABLE
        LOG_DEBUG(debug, "SharedRingBuffer", "To read: " << nextTail_ << " | Head: " << buf_->producer.position());
        while(!isMsgReadable()) {
            sched_yield();
        }

        // NEXT MSG IS READABLE -> MOVE TO ITS LOCATION
        buf_->consumers[subId_].position() = nextTail_;
        currTail_ = nextTail_;

        // COPY MESSAGE FROM SHARED MEMORY
        readMessage(msgRead); 

        return status::StatusCode::SUCCESS;
    }
    inline auto SharedRingBufferWrapper::try_dequeue(memory::SharedMsg *const msgRead) -> status::StatusCode
    {
        // COMPUTE NEXT SEGMENT TO READ FROM
        nextTail_ = buf_->getNextPos(currTail_);

        // CHECK ONCE IF NEXT MSG READABLE
        LOG_DEBUG(debug, "SharedRingBuffer", "Try read: " << nextTail_ << " | Head: " << buf_->producer.position());  
        if(!isMsgReadable()) {
            LOG_DEBUG(trace, "SharedRingBuffer", "Buffer is empty. (Tail: " << currTail_ << " | Head: " << buf_->producer.position()); 
            return status::StatusCode::BUFFER_EMPTY;
        }

        // NEXT MSG IS READABLE -> MOVE TO ITS LOCATION
        buf_->consumers[subId_].position() = nextTail_;
        currTail_ = nextTail_;

        // Copy message header & data seperatly
        readMessage(msgRead);

        return status::StatusCode::SUCCESS;
    }
    
    // ------------------ Write/Read Buffer ------------------------
    inline auto SharedRingBufferWrapper::isMsgReadable() const   -> bool
    {
        // Is the producer.position() at the location where we want to read?
        if(buf_->producer.position() != nextTail_) {
            return true;
        }
        LOG_DEBUG(trace, "SharedRingBuffer", "Buffer is empty. (Tail: " << currTail_ << " | Head: " << buf_->producer.position() << ")");
        return false;
    }
    inline auto SharedRingBufferWrapper::isMsgWritable() -> bool
    {
        // Is producer.position() at sames slowest consumer?
        if(nextHead_ != buf_->lastTail) {
            return true;
        }
        // Is producer.position() still at sames slowest consumer after updating slowest consumer?
        if(updateTail(currHead_) == nextHead_) {
            LOG_DEBUG(trace, "SharedRingBuffer", "Buffer is full. (Head: " << currHead_ << " | LastTail: " << buf_->lastTail << ")");  
            return false; 
        }
        return true;
    }
    inline auto SharedRingBufferWrapper::updateTail(const int producerPos) -> int
    {
        // Update slowest consumer
        int lead = 0, last = buf_->numSegments, currLead, currConPos;
        for(size_t i = 0; i < constants::kMaxSubscribersPerTopic; i++)
        { 
            if((currConPos = buf_->consumers[i].position()) >= 0 
                && (currLead = utils::math::circLead(producerPos, currConPos, buf_->numSegments)) >= lead) {
                lead = currLead;
                last = currConPos;   
            }
        }
        LOG_DEBUG(debug, "SharedRingBufferWrapper", "LastTail is now " << last);
        buf_->lastTail = last;

        return last;
    }
    inline auto SharedRingBufferWrapper::writeMessage(const void* const data, size_t dataSize) -> void
    {
        LOG_DEBUG(debug, "SharedRingBuffer", "Writing MSG at Pos: " << currHead_);
        currHdr_ = SharedMsgHeader::createFromDataSize(dataSize);
        currAddr_ = msgsAddrLut_[currHead_];
        memcpy(currAddr_, &currHdr_, currHdr_.hdrSize);
        memcpy(currAddr_ + currHdr_.hdrSize, data, currHdr_.dataSize);
        LOG_DEBUG(debug, "SharedRingBuffer", "Data written at " << currHead_); 
    }
    inline auto SharedRingBufferWrapper::readMessage(memory::SharedMsg *const msgRead) -> void
    {
        LOG_DEBUG(debug, "SharedRingBuffer", "Read MSG at Pos: " << currTail_);
        currAddr_ = msgsAddrLut_[nextTail_];
        currHdrAddr_ = (SharedMsgHeader*) currAddr_;
        memcpy(&(msgRead->hdr), currAddr_, currHdrAddr_->hdrSize);
        memcpy(msgRead->data, currAddr_ + currHdrAddr_->hdrSize, currHdrAddr_->dataSize);   
        LOG_DEBUG(debug, "SharedRingBuffer", "Data read at " << nextTail_); 
    }


}   // namespace shinda::memory
