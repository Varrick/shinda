#pragma once

// StdLib Imports
#include <string>
// Local Imports
#include "utils/timing.hpp"

namespace shinda::memory
{
    struct SharedMsgHeader
    {
        public:
            size_t msgSize;
            size_t hdrSize;
            size_t dataSize;

        public:
            SharedMsgHeader();
            SharedMsgHeader(size_t msgSize);

            auto static createEmpty()                       -> SharedMsgHeader;
            auto static createFromDataSize(size_t dataSize) -> SharedMsgHeader;
            auto static createFromMsgSize(size_t msgSize)   -> SharedMsgHeader;
    };

    /** @struct SharedMsg
     * @brief Container which is used to transfer data to/from shared memory
     * 
     * A SharedMsg contains information about the size of its payload and header as well as a timestamp
     * at which the message was created. The payload pointer member contains the data which shall be/was transferred
     * through shared memory. 
     */
    struct SharedMsg
    {
        public: 
            /** @var hdr
             * \brief Contains further information about size and time of creation of the message.
             */  
            SharedMsgHeader hdr;
            
            /** @var payload
             * \brief Data which was/shall be transferred through shared memory. 
             */
            void* data; 

        public:
            SharedMsg();
            SharedMsg(SharedMsgHeader hdr);
            SharedMsg(SharedMsgHeader hdr, void* data);
            
            auto static createEmpty()                                           -> SharedMsg;
            auto static createEmptyFromMsgSize(size_t msgSize)                  -> SharedMsg;
            auto static createEmptyFromDataSize(size_t dataSize)                -> SharedMsg;
            auto static createFromData(void* data, size_t dataSize)             -> SharedMsg;

            /** Reallocates memory for the payload member
             * 
             * Call to this method will dynamically allocate memory for the payload member of the SharedMsg as of the amount specified in dataSize. Also
             * header size information will be updated. 
             * @param dataSize Bytes to allocate dynamically for the payload member.
             * @returns Nothing.
             */
            auto resize(size_t dataSize) -> void;

            friend auto operator<<(std::ostream&, const SharedMsg&) -> std::ostream&;
    };

    auto operator<<(std::ostream& os, const SharedMsg& msg) -> std::ostream&;

}   // namespace shinda::structs