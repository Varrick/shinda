#pragma once

// StdLib Imports
#include <atomic>
#include <memory>
#include <typeinfo>
#include <algorithm>
// 3rd Party Imports
#include <boost/log/trivial.hpp>
// Local Imports
#include "SharedRingBufferMember.hpp"
#include "SharedMessage.hpp"
#include "constants.hpp"

namespace shinda::memory
{
  class SharedRingBuffer
  {
    private:
      SharedRingBuffer();

    public: 
      size_t size;
      size_t maxMsgSize;
      size_t sizeMask;
      int numSegments;
      std::atomic<int> lastTail;
      SharedRingBufferMember producer;
      SharedRingBufferMember consumers[constants::kMaxSubscribersPerTopic];
      char rawMsgs[1];

      auto static create(size_t maxMsgSize, int numSegments)       -> std::unique_ptr<SharedRingBuffer>;
      auto static estimateSize(size_t maxMsgSize, int numSegments) -> size_t;

      // ---------------------- Math/Helpers ---------------------------
      auto intToPos(int val)      const -> int;
      auto getNextPos(int pos)    const -> int;
      auto getMsgAddrAt(int idx)  const -> char*;
      auto getMsgAt(int idx)      const -> SharedMsgHeader*;

      friend auto operator<<(std::ostream&, const SharedRingBuffer&) -> std::ostream&;
  };

  inline auto operator<<(std::ostream& os, const SharedRingBuffer& buf) -> std::ostream&;

} // namepsace shinda::memory




