#pragma once

// StdLib imports
#include <mutex>
#include <condition_variable>
#include <pthread.h>
#include <unistd.h>
#include <atomic>
// 3rd Party Imports
#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_upgradable_mutex.hpp>
#include <boost/thread/thread_time.hpp>
#include <boost/log/trivial.hpp>
// Local imports
#include "memory/SharedMemoryBlock.hpp"
#include "macros.hpp"

namespace shinda::memory
{
    enum class MemberType {
        CONSUMER = 0,
        PRODUCER
    };

    class SharedRingBufferMember
    {
        private:
            std::atomic<bool> dataRdy_ {false};
            std::atomic<int> position_;
            MemberType type_;
            
            boost::interprocess::interprocess_mutex mtx_;
            boost::interprocess::interprocess_condition cnd_;

        public:
            SharedRingBufferMember(const SharedRingBufferMember&) = delete;
            SharedRingBufferMember(SharedRingBufferMember&&) = delete;
            SharedRingBufferMember();
            SharedRingBufferMember(int position, MemberType type);

            template<typename Obj, typename Fun, typename... Args>
            auto pollAdaptive(const int micros, Obj* obj, Fun(Obj::* fun)(Args...), Args ... args) -> bool
            {
                LOG_DEBUG(info, "SharedRingBufferMember", "Starting Adaptive Polling...");

                uint16_t attempts = 0;

                while(!(obj->*fun)(args...))
                {
                    attempts++;
                    if(__builtin_expect(attempts < constants::kDefaultNumNops, 1)) {
                        LOG_DEBUG(trace, "SharedRingBufferMember", "NOP.");
                    } // NOP
                    else if(__builtin_expect(attempts < constants::kDefaultNumScheduleYields, 1)) {
                        LOG_DEBUG(trace, "SharedRingBufferMember", "Yield.");
                        sched_yield();
                    } // Call Scheduler
                    else {
                        LOG_DEBUG(trace, "SharedRingBufferMember", "Wait...");
                        if(micros > 0) {
                            if(!timed_wait(micros)) {
                                return false;
                            }
                        }
                        else {
                            wait();
                        }
                    } // Non-busy wait
                }

                return true;
            }
            auto timed_wait(int timeoutMicros)          -> bool;
            auto wait()                                 -> void;
            auto notify()                               -> void;
            auto isWaiting()                      const -> bool;
            auto position()                             -> std::atomic<int>&;

            friend auto operator<<(std::ostream&, const SharedRingBufferMember&) -> std::ostream&;
    };

    inline auto operator<<(std::ostream& os, const SharedRingBufferMember& shm_mem) -> std::ostream&;
}
