#pragma once

#include <iostream>
// StdLib Imports
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdexcept>
#include <cerrno>
// Local Imports
#include "macros.hpp"
#include "exceptions.hpp"
#include "constants.hpp"

namespace shinda::memory
{
    enum class SHM_OPEN_TYPE {
            CREATE,
            JOIN,
            OPEN
    };

    template<typename T>
    class SharedMemoryBlock
    {
        private:
            std::unique_ptr<T, void(*)(void*)> mem_ {nullptr, null_deleter};
            std::string uid_;
            int shmPerm_;
            int mapPerm_;
            int fd_;
            size_t size_;
            bool wasJoined_;
        
            SharedMemoryBlock(const std::string uid, const size_t size, const SHM_OPEN_TYPE openType, 
                                const int shmPerm = constants::kDefaultShmPermissions, 
                                const int mapPerm = constants::kDefaultMmapPermissions);        

        public:
            SharedMemoryBlock() = delete;
            SharedMemoryBlock(const SharedMemoryBlock<T> &) = delete;
            SharedMemoryBlock(SharedMemoryBlock<T> &&) = delete;

            auto static create(std::string uid, size_t size, 
                                int shmPerm = constants::kDefaultShmPermissions, 
                                int mapPerm = constants::kDefaultMmapPermissions) 
                                -> std::unique_ptr<SharedMemoryBlock<T>>;
            auto static join(std::string uid, size_t size, 
                                int shmPerm = constants::kDefaultShmPermissions, 
                                int mapPerm = constants::kDefaultMmapPermissions) 
                                -> std::unique_ptr<SharedMemoryBlock<T>>;
            auto static open(std::string uid, size_t size, 
                                int shmPerm = constants::kDefaultShmPermissions, 
                                int mapPerm = constants::kDefaultMmapPermissions) 
                                -> std::unique_ptr<SharedMemoryBlock<T>>;
            auto write(const T *const dataIn)                           -> void;
            auto read(T const * dataOut)                          const -> void;
            auto write(const void *const dataIn, size_t dataSize)       -> void;
            auto read(void const* dataOut, size_t dataSize)       const -> void;
            auto truncate(size_t size)                                  -> void;
            auto closeShm()                                             -> void;
            auto releaseShm()                                           -> void;
            
            // GETTER
            auto getMemory()    &       ->  T*;
            auto getShmPerm()   const   ->  const int;
            auto getMapPerm()   const   ->  const int;
            auto getSize()      const   ->  const size_t;
            auto getUid()       const   ->  const std::string;
            auto wasJoined()    const   ->  const bool;
        private:
            static void null_deleter(void*);         
            auto map_block(int fd, size_t size, int mapPerm) -> void;

        template <typename X>
        friend auto operator<<(std::ostream&, const SharedMemoryBlock<X>&) -> std::ostream&;

    }; // class SharedMemoryBlock

    template<typename T>
    SharedMemoryBlock<T>::SharedMemoryBlock(const std::string uid, const size_t size, 
                    const SHM_OPEN_TYPE openType, const int shmPerm, const int mapPerm) 
    : uid_(uid), shmPerm_(shmPerm), mapPerm_(mapPerm), size_(size)
    {
        switch(openType) 
        { 
            case SHM_OPEN_TYPE::CREATE:
                fd_ = shm_open(uid.c_str(), O_RDWR | O_CREAT | O_EXCL, shmPerm);
                if(errno == EEXIST || fd_ == -1) // Check if SHM already exists or an error happened
                    throw exception::ShmBlockError("[SharedMemoryBlock] SHM with UID '" + uid + "' could not be created. Error: " + strerror(errno));   

                // Reshape to given size
                truncate(size);
                wasJoined_ = false;
                LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid << "' was created successfully."); 
                break;

            case SHM_OPEN_TYPE::JOIN:
                fd_ = shm_open(uid.c_str(), O_RDWR, 0);
                if (fd_ == -1)
                    throw exception::ShmBlockError("[SharedMemoryBlock] SHM with UID '" + uid + "' could not be joined. Error: " + strerror(errno));

                map_block(fd_, size_, mapPerm_);
                wasJoined_ = true;
                LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid << "' was joined successfully."); 
                break;
            
            case SHM_OPEN_TYPE::OPEN:
                fd_ = shm_open(uid.c_str(), O_RDWR | O_CREAT | O_EXCL, shmPerm);
                if(errno == EEXIST) // Check if SHM already exists
                {
                    fd_ = shm_open(uid.c_str(), O_RDWR, shmPerm);
                    if(fd_ < 0 && errno == ENOENT) // Check if SHM got deleted
                        throw exception::ShmBlockError("[SharedMemoryBlock] SHM with UID '" + uid + "' could not be joined because it does not exist");
                    else
                    {
                        map_block(fd_, size_, mapPerm_);
                        wasJoined_ = true;
                        LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was joined."); 
                    }    
                } 
                else    // SHM does not exist, resize properly
                {
                    truncate(size);
                    wasJoined_ = false;
                    LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was created."); 
                } 
                break;
        }     
    }           
   
    template <typename T>
    auto SharedMemoryBlock<T>::create(const std::string uid, const size_t size, const int shmPerm, const int mapPerm) 
                        -> std::unique_ptr<SharedMemoryBlock<T>>
    {
        return std::unique_ptr<SharedMemoryBlock<T>>(new SharedMemoryBlock<T>(uid, size, SHM_OPEN_TYPE::CREATE, shmPerm, mapPerm));
    }
    
    template <typename T>
    auto SharedMemoryBlock<T>::join(const std::string uid, const size_t size, const int shmPerm, const int mapPerm) 
                        -> std::unique_ptr<SharedMemoryBlock<T>>
    {
        return std::unique_ptr<SharedMemoryBlock<T>>(new SharedMemoryBlock<T>(uid, size, SHM_OPEN_TYPE::JOIN, shmPerm, mapPerm));
    }
    
    template <typename T>
    auto SharedMemoryBlock<T>::open(const std::string uid, const size_t size, const int shmPerm, const int mapPerm) 
                        -> std::unique_ptr<SharedMemoryBlock<T>>
    {
        return std::unique_ptr<SharedMemoryBlock<T>>(new SharedMemoryBlock<T>(uid, size, SHM_OPEN_TYPE::OPEN, shmPerm, mapPerm));
    }
    
    template <typename T>
    inline auto SharedMemoryBlock<T>::write(const T *const dataIn)                                 -> void 
    {
        memcpy(mem_.get(), dataIn, size_);
        LOG_DEBUG(trace, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was written. #BytesWritten = " << sizeof(T));
    }
    
    template <typename T>
    inline auto SharedMemoryBlock<T>::read(T const * dataOut)                                const -> void
    {
        memcpy(dataOut, mem_, size_);
        LOG_DEBUG(trace, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was read. #BytesRead = " << sizeof(T));
    }
    
    template <typename T>
    inline auto SharedMemoryBlock<T>::write(const void *const dataIn, const size_t dataSize)       -> void 
    {
        if(dataSize > size_) {
            throw exception::DataSizeMismatchError("[SharedMemoryBlock] Writing Memoryblock with UID '" + uid_ + "' failed. Invalid data size (DataSize: " + std::to_string(dataSize) + " | Actual SHM Size: " + std::to_string(size_) + ").");
        }
        memcpy(mem_, dataIn, size_);
        LOG_DEBUG(trace, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was written. #BytesWritten = " << dataSize);     
    }
    
    template <typename T>
    inline auto SharedMemoryBlock<T>::read(void const* dataOut, const size_t dataSize)       const -> void
    {
        if(dataSize > size_) {
            throw exception::DataSizeMismatchError("[SharedMemoryBlock] Reading Memoryblock with UID '" + uid_ + "' failed. Invalid data size (DataSize: " + std::to_string(dataSize) + " | Actual SHM Size: " + std::to_string(size_) + ").");
        }
        memcpy(dataOut, mem_, dataSize);
        LOG_DEBUG(trace, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was read. #BytesRead = " << dataSize);
    }
    
    template <typename T>
    auto SharedMemoryBlock<T>::truncate(const size_t size)  -> void
    {   
        // Resize Shared Memory Region To Shared Struct Size
        if(ftruncate(fd_, size) == -1) {
            throw exception::ShmBlockError("[SharedMemoryBlock] ftruncate failed to resize to size: " + std::to_string(size) + " Byte. Error: " + strerror(errno));
        }
        // Remap Shared Memory into process address space
        LOG_DEBUG(trace, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' resized to " << size << "Byte");
        map_block(fd_, size, mapPerm_);
        size_ = size;  
    } 
    
    template <typename T>
    auto SharedMemoryBlock<T>::closeShm()                   -> void 
    {
        // Close File Descriptor
        if(munmap(mem_.get(), size_) == -1) {
            throw exception::ShmBlockError("[SharedMemoryBlock] SHM with UID " + uid_ + " could not be unmapped.");
        }

        LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was closed.");
    }
    
    template <typename T>
    auto SharedMemoryBlock<T>::releaseShm()                 -> void
    {
        // Close File Descriptor and mark shared memory block for os garbage collector
        if(shm_unlink(uid_.c_str()) == -1) {
            throw exception::ShmBlockError("[SharedMemoryBlock] SHM with UID " + uid_ + " could not be unlinked.");
        }
        closeShm();
        
        LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' was unlinked.");
    }
    
    // GETTER
    template <typename T>
    auto SharedMemoryBlock<T>::getMemory()    &       ->  T* { return mem_.get(); }
    
    template <typename T>
    auto SharedMemoryBlock<T>::getShmPerm()   const   ->  const int { return shmPerm_; }
    
    template <typename T>
    auto SharedMemoryBlock<T>::getMapPerm()   const   ->  const int { return mapPerm_; }
    
    template <typename T>
    auto SharedMemoryBlock<T>::getSize()      const   ->  const size_t { return size_; } 
    
    template <typename T>
    auto SharedMemoryBlock<T>::getUid()       const   ->  const std::string { return uid_; } 
    
    template <typename T>
    auto SharedMemoryBlock<T>::wasJoined()    const   ->  const bool { return wasJoined_; } 

    template <typename T>
    void SharedMemoryBlock<T>::null_deleter(void*){;}           
    
    template <typename T>
    auto SharedMemoryBlock<T>::map_block(const int fd, const size_t size, const int mapPerm) -> void
    {
        auto mem = static_cast<T*>(mmap(NULL, size, mapPerm, MAP_SHARED, fd, 0)); 
        if(mem == MAP_FAILED) {
            throw exception::ShmBlockError("[SharedMemoryBlock] MMAP failed to map shared memory block to local address space");
        }
        else {
            LOG_DEBUG(debug, "SharedMemoryBlock", "Memoryblock with UID '" << uid_ << "' mapped.");      
            mem_.reset(mem);  
        }        
    }

    template <typename T>
    auto operator<<(std::ostream& os, const SharedMemoryBlock<T>& shm) -> std::ostream&
    {
        struct stat statBuf;
        if(fstat(shm.fd_, &statBuf) < 0)
            LOG_DEBUG(debug, "SharedMemoryBlock", "fstat() on shared memory failed with errno " << errno);

        os << std::string("----SharedMemoryBlock Stats----") +
                    "\nUID = " + shm.uid_ + " | (TypeSize = " + std::to_string(sizeof(shm.uid_)) + "Byte" + ")" + 
                    "\nShmPermissions = " + std::to_string(shm.shmPerm_) + " | (TypeSize = " + std::to_string(sizeof(shm.shmPerm_)) + "Byte" + ")" + 
                    "\nMapPermissions = " + std::to_string(shm.mapPerm_) + " | (TypeSize = " + std::to_string(sizeof(shm.mapPerm_)) + "Byte" + ")" +   
                    "\nSize = " + std::to_string(shm.size_) + " | (TypeSize = " + std::to_string(sizeof(shm.size_)) + "Byte" + ")" +
                    "\nRealSize = " + std::to_string(statBuf.st_size) + " | (TypeSize = " + std::to_string(sizeof(statBuf.st_size)) + "Byte" + ")" + 
                    "\n-----------------------------\n";
        return os;
    }
} // namespace shinda::memory

