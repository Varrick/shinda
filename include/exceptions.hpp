#pragma once

#include <stdexcept>

namespace shinda::exception
{
    class ShmBlockError : public std::runtime_error
    {
    public:
        ShmBlockError(const std::string& msg) : runtime_error(msg) {}
    };

    class DataSizeMismatchError : public std::runtime_error
    {
    public:
        DataSizeMismatchError(const std::string& msg) : runtime_error(msg) {}
    };

    class SocketError : public std::runtime_error
    {
    public:
        SocketError(const std::string& msg) : runtime_error(msg) {}
    };
}