#pragma once

// StdLib imports
#include <mutex>
#include <condition_variable>
#include <pthread.h>
#include <unistd.h>
// 3rd Party Imports
#include <boost/log/trivial.hpp>
// Local imports
#include "macros.hpp"
#include "status_codes.hpp"
#include "constants.hpp"

namespace shinda::socket
{

class SocketNotifier
{
  private:
    bool wasReplied_ {false};
    status::StatusCode st_ {status::StatusCode::SOCKET_TIMEOUT};
    std::mutex m_;
    std::condition_variable c_;

  public:
    auto notify(status::StatusCode st) -> void;
    auto wait(int millisecs = constants::kDefaultSocketTimeoutMs, 
              int updateMs = constants::kDefaultSocketUpdateMs) -> status::StatusCode;
};

} // namespace shinda::socket