#pragma once

// 3rd Party Imports
#include <boost/log/trivial.hpp>
// Local Imports
#include "interfaces/ISocketClient.hpp"
#include "SocketDefinitions.hpp"
#include "macros.hpp"
#include "exceptions.hpp"

namespace shinda::socket::client {

class SocketClient final : public ISocketClient {
public:
  SocketClient();
  ~SocketClient();
  void stop() override;
  void start() override;
  void sendMessage(const std::string &) override;
  void registerCallbacks(ISocketClientCallbacks *) const override;

private:
  void connectHandler(const boost::system::error_code&);
  void startReceiving();
  void tryConnect();
  void onReadyForReconnect(const boost::system::error_code&);

  void startReconnectTimer();
  void doClose();
  void readHandler(const boost::system::error_code&, std::size_t);
  void onSend(const std::string&, const boost::system::error_code&, std::size_t);

  std::array<char, SOCKET_BUFFER_SIZE> buffer;

  boost::asio::io_context ioContext;
  std::unique_ptr<seqpacket_protocol::socket> socket;
  boost::asio::executor_work_guard<boost::asio::io_context::executor_type> worker;

  std::thread runThread;
  boost::asio::steady_timer timer;

  shinda::socket::socketStatus status;

  mutable ISocketClientCallbacks *callbacks;
};
} // namespace socket::client
