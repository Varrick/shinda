#pragma once

#include "ISocketCallbacks.hpp"

#include <memory>
#include <iostream>
#include <array>
#include <string>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>


namespace shinda::socket::server
{
class ISocketSession {
public:
  ISocketSession(void) = default;
  virtual ~ISocketSession() noexcept = default;
  ISocketSession(const ISocketSession &) = delete;
  ISocketSession(ISocketSession &&) = delete;
  ISocketSession &operator=(const ISocketSession &) = delete;
  ISocketSession &operator=(ISocketSession &&) = delete;
  void *operator new[](size_t) = delete;

  virtual void start() = 0;
  virtual void stop() = 0;
  virtual void sendMessage(const std::string &) = 0;
  virtual void handleRead(const boost::system::error_code &, std::size_t) = 0;
};
} // namespace socket::server