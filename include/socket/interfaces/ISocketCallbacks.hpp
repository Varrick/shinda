#pragma once

#include <string>

namespace shinda::socket::client {
class ISocketClientCallbacks {
public:
  virtual ~ISocketClientCallbacks() noexcept = default;

  virtual void receivedPlatformMessage(const std::string &&) = 0;
};
} // namespace socket::client

namespace shinda::socket::server {
class ISocketServerCallbacks {
public:
  virtual ~ISocketServerCallbacks() noexcept = default;

  virtual void receivedPlatformMessage(const int sessionId, const std::string && msg) = 0;
};
} // namespace socket::server