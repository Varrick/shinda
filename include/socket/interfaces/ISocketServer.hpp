#pragma once

#include "ISocketCallbacks.hpp"

#include <memory>
#include <iostream>
#include <array>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>


namespace shinda::socket::server {

class ISocketServer {
public:
  ISocketServer(void) = default;
  virtual ~ISocketServer() noexcept = default;
  ISocketServer(const ISocketServer &) = delete;
  ISocketServer(ISocketServer &&) = delete;
  ISocketServer &operator=(const ISocketServer &) = delete;
  ISocketServer &operator=(ISocketServer &&) = delete;
  void *operator new[](size_t) = delete;

  virtual void start() = 0;
  virtual void stop() = 0;
  virtual void join() = 0;
  virtual void registerCallbacks(ISocketServerCallbacks *cbs) const = 0;
  virtual void sendMessage(const int &session_id, const std::string &msg) = 0;
};

class SocketServerFactory final {
public:
  SocketServerFactory(void) = delete;
  static std::unique_ptr<ISocketServer> create();
};

}// namespace socket::server
