#pragma once

#include "ISocketCallbacks.hpp"

#include <memory>
#include <iostream>
#include <array>
#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>

namespace shinda::socket::client {

class ISocketClient {
public:
  ISocketClient(void) = default;
  virtual ~ISocketClient() noexcept = default;
  ISocketClient(const ISocketClient &) = delete;
  ISocketClient(ISocketClient &&) = delete;
  ISocketClient &operator=(const ISocketClient &) = delete;
  ISocketClient &operator=(ISocketClient &&) = delete;
  void *operator new[](size_t) = delete;

  virtual void start() = 0;
  virtual void stop() = 0;
  virtual void registerCallbacks(ISocketClientCallbacks *cbs) const = 0;
  virtual void sendMessage(const std::string &) = 0;
};

class SocketClientFactory final {
public:
  SocketClientFactory(void) = delete;
  static std::unique_ptr<ISocketClient> create();

};
} // namespace socket::client

