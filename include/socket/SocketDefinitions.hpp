#pragma once

#include <boost/asio.hpp>

namespace shinda::socket {

    constexpr auto RECONNECT_TIMEOUT{boost::asio::chrono::seconds{2}};
    constexpr const char *PLATFORMD_SOCKET{"/tmp/socket.sock"};
    constexpr auto SOCKET_BUFFER_SIZE{500};

    enum socketStatus { CONNECTED, DISCONNECTED, SHUTDOWN };

    struct seqpacket_protocol {
        int type() const { return SOCK_SEQPACKET; }
        int protocol() const { return 0; }
        int family() const { return AF_UNIX; }

        typedef boost::asio::local::basic_endpoint<seqpacket_protocol> endpoint;
        typedef boost::asio::basic_seq_packet_socket<seqpacket_protocol> socket;
        typedef boost::asio::basic_socket_acceptor<seqpacket_protocol> acceptor;

        #if !defined(BOOST_ASIO_NO_IOSTREAM)
        /// The UNIX domain iostream type.
        typedef boost::asio::basic_socket_iostream<seqpacket_protocol> iostream;
        #endif // !defined(BOOST_ASIO_NO_IOSTREAM)
    };

}   // namespace socket