#pragma once

// 3rd Party Imports
#include <boost/log/trivial.hpp>
#include <boost/signals2/signal.hpp>
#include <boost/lexical_cast.hpp>
// Local Imports
#include "interfaces/ISocketServer.hpp"
#include "interfaces/ISocketSession.hpp"
#include "SocketDefinitions.hpp"
#include "macros.hpp"
#include "utils/IdManager.hpp"
#include "exceptions.hpp"
#include "constants.hpp"

namespace shinda::socket::server {

class SocketServer;

  class SocketSession final : public ISocketSession
  {
    public:
      boost::signals2::signal<void(const int&)> sessionClosedEvent;

      SocketSession(boost::asio::io_context&, ISocketServerCallbacks*);
      ~SocketSession();
      void start() override;
      void stop() override;
      void sendMessage(const std::string &msg) override;
      void handleRead(const boost::system::error_code &, std::size_t) override;
      auto socket()     const&  ->  const seqpacket_protocol::socket&;
      auto socket()       &     ->  seqpacket_protocol::socket&;
      auto id()         const&  ->  const int&;
      auto id()           &     ->  int&;

    private:
      int id_ {0};
      boost::asio::io_context& ioContext_;
      std::unique_ptr<seqpacket_protocol::socket> socket_;
      std::array<char, SOCKET_BUFFER_SIZE> buffer_;
      mutable ISocketServerCallbacks *callbacks_;

      void doClose();
      void onSend(const std::string&, const boost::system::error_code &, std::size_t);
  };

class SocketServer final : public ISocketServer {
  private:
    // Server Management Members
    boost::asio::io_context ioContext_;
    std::thread runThread_;
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> worker_;
    seqpacket_protocol::endpoint endpoint_;
    std::unique_ptr<seqpacket_protocol::acceptor> acceptor_;
    socketStatus status_;
    boost::asio::signal_set signals_;

    // Session Management Members
    using sessPtr = std::shared_ptr<SocketSession>;
    using connection = boost::signals2::scoped_connection;
    std::mutex mx_;
    std::map<sessPtr, connection> sessions_;
    utils::IdManager sessionIdManager_;
    mutable ISocketServerCallbacks *callbacks_;

  public:
    SocketServer();
    ~SocketServer();
    void start() override;
    void stop() override;
    void join() override;
    void registerCallbacks(ISocketServerCallbacks *) const override;
    void sendMessage(const int& sessionId, const std::string& msg) override;

  private:
    void doClose();
    void startListen();
    void startAccept();
    void acceptHandler(std::shared_ptr<SocketSession>, const boost::system::error_code &);
    void regSession(sessPtr);
    void deleteSession(const int& sessionId);
  };

} // namespace socket::server