#pragma once

// General
#define LOG_DEBUGGING                       0
#define LOG_APPLICATIONS                    1

#define LOG_FORMAT(severity, caller, msg) BOOST_LOG_TRIVIAL(severity) << "[" << caller << "]" << " " << msg

#if LOG_DEBUGGING == 1
#define LOG_DEBUG(severity, caller, msg) LOG_FORMAT(severity, caller, msg)
#else
#define LOG_DEBUG(severity, caller, msg)
#endif

#if LOG_APPLICATIONS == 1
#define LOG_APP(severity, caller, msg) LOG_FORMAT(severity, caller, msg)
#else
#define LOG_APP(severity, caller, msg)
#endif