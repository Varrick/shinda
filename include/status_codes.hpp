/// \file

#pragma once

/** StatusCode
 * @brief Everything regarding noncritical status codes
 **/
namespace shinda::status
{
    /** @enum StatusCode
     * @brief StatusCodes for providing feedback to the user about noncritical results that may limit the experience of using the Shinda library
     */
    enum class StatusCode: size_t
    { 
        SUCCESS = 0,            ///< Action was successfully performed.
        BUFFER_FULL,            ///< Writing to shared memory was not possible because the buffer is full.
        BUFFER_EMPTY,           ///< Reading from shared memory was not possible because the buffer is empty.
        SOCKET_TIMEOUT,         ///< Request to Broker timed out.
        NO_CONNECTION,          ///< Client is currently not connected to Broker.
        MSG_SIZE_MISMATCH,      ///< Message size of message to write/read does not fit message size of the Topic.
        TOPIC_FULL,             ///< Limit of amount of subscribers/publisher reached. 
        TOPIC_DOES_NOT_EXIST,   ///< Topic to which you want to join as publisher/subscriber does not exist.
        BROKER_ERROR,           ///< Critical error at Broker happened.   
        NOT_ENOUGH_MEMORY,      ///< Not enough memory available to create Topic.   
        TOPIC_NOT_ALLOWED,      ///< Topic was not defined in config.json and is therefore not allowed to be created.
        TOPIC_SIZE_NOT_ALLOWED, ///< Maximum size of a Topic defined in config.json was surpassed.
    };

    const static char* statusCodeToString(StatusCode e) noexcept
    {
        switch (e)
        {
            case StatusCode::SUCCESS: return "Success";
            case StatusCode::BUFFER_FULL: return "BUFFER_FULL";
            case StatusCode::BUFFER_EMPTY: return "BUFFER_EMPTY";
            case StatusCode::SOCKET_TIMEOUT: return "Socket Timeout";
            case StatusCode::NO_CONNECTION: return "No Connection";
            case StatusCode::MSG_SIZE_MISMATCH: return "Message Size Mismatch";
            case StatusCode::TOPIC_FULL: return "Topic Full";
            case StatusCode::TOPIC_DOES_NOT_EXIST: return "Topic does not exist";
            case StatusCode::BROKER_ERROR: return "Broker Error";
            case StatusCode::NOT_ENOUGH_MEMORY: return "Not enough memory";
            case StatusCode::TOPIC_NOT_ALLOWED: return "Topic not allowed";
            case StatusCode::TOPIC_SIZE_NOT_ALLOWED: return "Topic size not allowed";
            default: return "Status Code not found";
        }
    }
}

