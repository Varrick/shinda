var structshinda_1_1memory_1_1SharedMsgHeader =
[
    [ "SharedMsgHeader", "structshinda_1_1memory_1_1SharedMsgHeader.html#a32943562d14c4ea895f3b3aa4ad89853", null ],
    [ "SharedMsgHeader", "structshinda_1_1memory_1_1SharedMsgHeader.html#a801494f16f84c3398aed32e4360f478d", null ],
    [ "createEmpty", "structshinda_1_1memory_1_1SharedMsgHeader.html#a74573e918e344fb71bf2aa16f3ca5d8a", null ],
    [ "createFromDataSize", "structshinda_1_1memory_1_1SharedMsgHeader.html#a7ac5e0e1c32c410e365aded43ccd0a77", null ],
    [ "createFromMsgSize", "structshinda_1_1memory_1_1SharedMsgHeader.html#a62203025a2ce74667420544c844b174b", null ],
    [ "dataSize", "structshinda_1_1memory_1_1SharedMsgHeader.html#a78f08ed5a40b78ccf3ac1f6c75978d67", null ],
    [ "hdrSize", "structshinda_1_1memory_1_1SharedMsgHeader.html#abad15a27e678fa457655e453e870d71a", null ],
    [ "msgSize", "structshinda_1_1memory_1_1SharedMsgHeader.html#a14fc5228673a1c47029f29a35da69853", null ]
];