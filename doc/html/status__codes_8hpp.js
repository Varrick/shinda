var status__codes_8hpp =
[
    [ "StatusCode", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cf", [
      [ "SUCCESS", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfad0749aaba8b833466dfcbb0428e4f89c", null ],
      [ "BUFFER_FULL", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfaf46524fa13d0e02e2743d1db7279375d", null ],
      [ "BUFFER_EMPTY", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa0b6a3ccf5a6e64dff6a5acf1337ce340", null ],
      [ "SOCKET_TIMEOUT", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa2a63be70c23867e82534f3abc8251ca8", null ],
      [ "NO_CONNECTION", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa9dafba0f78599e40537aec876c30325b", null ],
      [ "MSG_SIZE_MISMATCH", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfab8287579a59ce970afe47fc93cad6767", null ],
      [ "TOPIC_FULL", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa669cf651826a219955b82aaf2a3cf8c6", null ],
      [ "TOPIC_DOES_NOT_EXIST", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa944dbcf70fdbfd7e15bd9b703d45d68b", null ],
      [ "BROKER_ERROR", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa315e738311a7a9f90773f11a09e95656", null ],
      [ "NOT_ENOUGH_MEMORY", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfaea8a564efbb910c22c45506553bb7469", null ],
      [ "TOPIC_NOT_ALLOWED", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfa7fc6bd6922cfa7b8c8a09e3d366c38a2", null ],
      [ "TOPIC_SIZE_NOT_ALLOWED", "status__codes_8hpp.html#af906d0ad677e6b87696a89969b6984cfab2f0a68407baa16cd515ad195232e00b", null ]
    ] ],
    [ "statusCodeToString", "status__codes_8hpp.html#a759360006296896ae7e800c558b26156", null ]
];