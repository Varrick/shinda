var annotated_dup =
[
    [ "shinda", null, [
      [ "exception", null, [
        [ "ShmBlockError", "classshinda_1_1exception_1_1ShmBlockError.html", "classshinda_1_1exception_1_1ShmBlockError" ],
        [ "DataSizeMismatchError", "classshinda_1_1exception_1_1DataSizeMismatchError.html", "classshinda_1_1exception_1_1DataSizeMismatchError" ],
        [ "SocketError", "classshinda_1_1exception_1_1SocketError.html", "classshinda_1_1exception_1_1SocketError" ]
      ] ],
      [ "memory", null, [
        [ "SharedMemoryBlock", "classshinda_1_1memory_1_1SharedMemoryBlock.html", "classshinda_1_1memory_1_1SharedMemoryBlock" ],
        [ "SharedMsgHeader", "structshinda_1_1memory_1_1SharedMsgHeader.html", "structshinda_1_1memory_1_1SharedMsgHeader" ],
        [ "SharedMsg", "structshinda_1_1memory_1_1SharedMsg.html", "structshinda_1_1memory_1_1SharedMsg" ],
        [ "SharedRingBuffer", "classshinda_1_1memory_1_1SharedRingBuffer.html", "classshinda_1_1memory_1_1SharedRingBuffer" ],
        [ "SharedRingBufferMember", "classshinda_1_1memory_1_1SharedRingBufferMember.html", "classshinda_1_1memory_1_1SharedRingBufferMember" ],
        [ "SharedRingBufferWrapper", "classshinda_1_1memory_1_1SharedRingBufferWrapper.html", "classshinda_1_1memory_1_1SharedRingBufferWrapper" ]
      ] ],
      [ "pubsub", null, [
        [ "ShindaBroker", "classshinda_1_1pubsub_1_1ShindaBroker.html", "classshinda_1_1pubsub_1_1ShindaBroker" ],
        [ "ShindaClient", "classshinda_1_1pubsub_1_1ShindaClient.html", "classshinda_1_1pubsub_1_1ShindaClient" ],
        [ "ShindaUser", "structshinda_1_1pubsub_1_1ShindaUser.html", "structshinda_1_1pubsub_1_1ShindaUser" ],
        [ "TopicBlueprint", "classshinda_1_1pubsub_1_1TopicBlueprint.html", "classshinda_1_1pubsub_1_1TopicBlueprint" ],
        [ "Topic", "classshinda_1_1pubsub_1_1Topic.html", "classshinda_1_1pubsub_1_1Topic" ]
      ] ],
      [ "socket", null, [
        [ "client", null, [
          [ "ISocketClientCallbacks", "classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks.html", "classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks" ],
          [ "ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html", "classshinda_1_1socket_1_1client_1_1ISocketClient" ],
          [ "SocketClientFactory", "classshinda_1_1socket_1_1client_1_1SocketClientFactory.html", "classshinda_1_1socket_1_1client_1_1SocketClientFactory" ],
          [ "SocketClient", "classshinda_1_1socket_1_1client_1_1SocketClient.html", "classshinda_1_1socket_1_1client_1_1SocketClient" ]
        ] ],
        [ "server", null, [
          [ "ISocketServerCallbacks", "classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks.html", "classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks" ],
          [ "ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html", "classshinda_1_1socket_1_1server_1_1ISocketServer" ],
          [ "SocketServerFactory", "classshinda_1_1socket_1_1server_1_1SocketServerFactory.html", "classshinda_1_1socket_1_1server_1_1SocketServerFactory" ],
          [ "ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html", "classshinda_1_1socket_1_1server_1_1ISocketSession" ],
          [ "SocketSession", "classshinda_1_1socket_1_1server_1_1SocketSession.html", "classshinda_1_1socket_1_1server_1_1SocketSession" ],
          [ "SocketServer", "classshinda_1_1socket_1_1server_1_1SocketServer.html", "classshinda_1_1socket_1_1server_1_1SocketServer" ]
        ] ],
        [ "seqpacket_protocol", "structshinda_1_1socket_1_1seqpacket__protocol.html", "structshinda_1_1socket_1_1seqpacket__protocol" ],
        [ "SocketNotifier", "classshinda_1_1socket_1_1SocketNotifier.html", "classshinda_1_1socket_1_1SocketNotifier" ]
      ] ],
      [ "utils", null, [
        [ "bench", null, [
          [ "sync", null, [
            [ "IPC_Barrier", "classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier.html", "classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier" ]
          ] ],
          [ "MeasurementDetails", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails" ],
          [ "CSVWriter", "classshinda_1_1utils_1_1bench_1_1CSVWriter.html", "classshinda_1_1utils_1_1bench_1_1CSVWriter" ],
          [ "Measurement", "classshinda_1_1utils_1_1bench_1_1Measurement.html", "classshinda_1_1utils_1_1bench_1_1Measurement" ]
        ] ],
        [ "json", null, [
          [ "parser", null, [
            [ "RSJresource", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJresource.html", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJresource" ],
            [ "RSJparsedData", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJparsedData.html", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJparsedData" ]
          ] ],
          [ "serializer", null, [
            [ "JSON", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON.html", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON" ]
          ] ]
        ] ],
        [ "timing", null, [
          [ "DeadlineTimer", "classshinda_1_1utils_1_1timing_1_1DeadlineTimer.html", "classshinda_1_1utils_1_1timing_1_1DeadlineTimer" ]
        ] ],
        [ "IdManager", "classshinda_1_1utils_1_1IdManager.html", "classshinda_1_1utils_1_1IdManager" ]
      ] ]
    ] ]
];