var hierarchy =
[
    [ "shinda::utils::json::serializer::JSON::BackingData", "unionshinda_1_1utils_1_1json_1_1serializer_1_1JSON_1_1BackingData.html", null ],
    [ "shinda::utils::bench::CSVWriter", "classshinda_1_1utils_1_1bench_1_1CSVWriter.html", null ],
    [ "shinda::utils::timing::DeadlineTimer", "classshinda_1_1utils_1_1timing_1_1DeadlineTimer.html", null ],
    [ "std::exception", null, [
      [ "std::runtime_error", null, [
        [ "shinda::exception::DataSizeMismatchError", "classshinda_1_1exception_1_1DataSizeMismatchError.html", null ],
        [ "shinda::exception::ShmBlockError", "classshinda_1_1exception_1_1ShmBlockError.html", null ],
        [ "shinda::exception::SocketError", "classshinda_1_1exception_1_1SocketError.html", null ]
      ] ]
    ] ],
    [ "shinda::utils::IdManager", "classshinda_1_1utils_1_1IdManager.html", null ],
    [ "shinda::utils::bench::sync::IPC_Barrier", "classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier.html", null ],
    [ "shinda::socket::client::ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html", [
      [ "shinda::socket::client::SocketClient", "classshinda_1_1socket_1_1client_1_1SocketClient.html", null ]
    ] ],
    [ "shinda::socket::client::ISocketClientCallbacks", "classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks.html", [
      [ "shinda::pubsub::ShindaClient< T >", "classshinda_1_1pubsub_1_1ShindaClient.html", null ]
    ] ],
    [ "shinda::socket::server::ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html", [
      [ "shinda::socket::server::SocketServer", "classshinda_1_1socket_1_1server_1_1SocketServer.html", null ]
    ] ],
    [ "shinda::socket::server::ISocketServerCallbacks", "classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks.html", [
      [ "shinda::pubsub::ShindaBroker", "classshinda_1_1pubsub_1_1ShindaBroker.html", null ]
    ] ],
    [ "shinda::socket::server::ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html", [
      [ "shinda::socket::server::SocketSession", "classshinda_1_1socket_1_1server_1_1SocketSession.html", null ]
    ] ],
    [ "shinda::utils::json::serializer::JSON", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON.html", null ],
    [ "shinda::utils::json::serializer::JSON::JSONConstWrapper< Container >", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON_1_1JSONConstWrapper.html", null ],
    [ "shinda::utils::json::serializer::JSON::JSONWrapper< Container >", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON_1_1JSONWrapper.html", null ],
    [ "shinda::utils::bench::Measurement", "classshinda_1_1utils_1_1bench_1_1Measurement.html", null ],
    [ "shinda::utils::bench::MeasurementDetails", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html", null ],
    [ "shinda::utils::json::parser::RSJparsedData", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJparsedData.html", null ],
    [ "shinda::utils::json::parser::RSJresource", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJresource.html", null ],
    [ "shinda::socket::seqpacket_protocol", "structshinda_1_1socket_1_1seqpacket__protocol.html", null ],
    [ "shinda::memory::SharedMemoryBlock< T >", "classshinda_1_1memory_1_1SharedMemoryBlock.html", null ],
    [ "shinda::memory::SharedMsg", "structshinda_1_1memory_1_1SharedMsg.html", null ],
    [ "shinda::memory::SharedMsgHeader", "structshinda_1_1memory_1_1SharedMsgHeader.html", null ],
    [ "shinda::memory::SharedRingBuffer", "classshinda_1_1memory_1_1SharedRingBuffer.html", null ],
    [ "shinda::memory::SharedRingBufferMember", "classshinda_1_1memory_1_1SharedRingBufferMember.html", null ],
    [ "shinda::memory::SharedRingBufferWrapper", "classshinda_1_1memory_1_1SharedRingBufferWrapper.html", null ],
    [ "shinda::pubsub::ShindaUser", "structshinda_1_1pubsub_1_1ShindaUser.html", null ],
    [ "shinda::socket::client::SocketClientFactory", "classshinda_1_1socket_1_1client_1_1SocketClientFactory.html", null ],
    [ "shinda::socket::SocketNotifier", "classshinda_1_1socket_1_1SocketNotifier.html", null ],
    [ "shinda::socket::server::SocketServerFactory", "classshinda_1_1socket_1_1server_1_1SocketServerFactory.html", null ],
    [ "shinda::pubsub::TopicBlueprint", "classshinda_1_1pubsub_1_1TopicBlueprint.html", [
      [ "shinda::pubsub::Topic", "classshinda_1_1pubsub_1_1Topic.html", null ]
    ] ]
];