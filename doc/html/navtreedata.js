/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Shinda", "index.html", [
    [ "Installation", "index.html#autotoc_md1", null ],
    [ "Removal", "index.html#autotoc_md4", null ],
    [ "Usage", "index.html#autotoc_md6", [
      [ "Example Program", "index.html#autotoc_md7", [
        [ "Publisher", "index.html#autotoc_md8", null ],
        [ "Subscriber", "index.html#autotoc_md9", null ]
      ] ]
    ] ],
    [ "Demos", "index.html#autotoc_md11", [
      [ "ClientStringDemo", "index.html#autotoc_md12", [
        [ "Command Line Arguments", "index.html#autotoc_md13", null ]
      ] ],
      [ "ClientNumberDemo", "index.html#autotoc_md14", [
        [ "Command Line Arguments", "index.html#autotoc_md15", null ]
      ] ],
      [ "ClientCamDemo", "index.html#autotoc_md16", [
        [ "Command Line Arguments", "index.html#autotoc_md17", null ]
      ] ]
    ] ],
    [ "Configuration", "index.html#autotoc_md19", [
      [ "shinda.service", "index.html#autotoc_md20", null ],
      [ "config.json", "index.html#autotoc_md21", null ],
      [ "constants.hpp", "index.html#autotoc_md22", null ],
      [ "macros.hpp", "index.html#autotoc_md23", null ]
    ] ],
    [ "Concept", "index.html#autotoc_md25", [
      [ "Motivation", "index.html#autotoc_md26", null ],
      [ "General Idea", "index.html#autotoc_md27", null ],
      [ "Broker", "index.html#autotoc_md28", null ],
      [ "Client", "index.html#autotoc_md29", null ],
      [ "Buffer", "index.html#autotoc_md30", null ],
      [ "Synchronization", "index.html#autotoc_md31", [
        [ "Producer", "index.html#autotoc_md32", null ],
        [ "Consumer", "index.html#autotoc_md33", null ],
        [ "Adaptive Polling", "index.html#autotoc_md34", null ]
      ] ]
    ] ],
    [ "Benchmarks", "index.html#autotoc_md36", null ],
    [ "References", "index.html#autotoc_md38", null ],
    [ "Namespaces", "namespaces.html", [
      [ "Namespace List", "namespaces.html", "namespaces_dup" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ],
        [ "Typedefs", "functions_type.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"ISocketCallbacks_8hpp_source.html",
"classshinda_1_1pubsub_1_1TopicBlueprint.html#a13caa3c9917eee6c595cd2ae09f84242",
"classshinda_1_1utils_1_1json_1_1serializer_1_1JSON.html#a7cfc193afe8a6554b7bc18b671417599",
"timing_8hpp.html#a176e9e58d7b7d028f028503adedc5294"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';