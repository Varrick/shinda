var classshinda_1_1memory_1_1SharedRingBuffer =
[
    [ "SharedRingBuffer", "classshinda_1_1memory_1_1SharedRingBuffer.html#aaadc3f75016791515046b0a95b6605b0", null ],
    [ "create", "classshinda_1_1memory_1_1SharedRingBuffer.html#a78dbad059a6a2fa642a00eaf06b1dec5", null ],
    [ "estimateSize", "classshinda_1_1memory_1_1SharedRingBuffer.html#aed95d00549c160306d2de8905ea62f5e", null ],
    [ "getMsgAddrAt", "classshinda_1_1memory_1_1SharedRingBuffer.html#a000e97e456077911f333467d15a94e2d", null ],
    [ "getMsgAt", "classshinda_1_1memory_1_1SharedRingBuffer.html#a189c3c4ec97cacfdd1e61efd889bd572", null ],
    [ "getNextPos", "classshinda_1_1memory_1_1SharedRingBuffer.html#a0becb7cb991581bf54ab654921b11ce1", null ],
    [ "intToPos", "classshinda_1_1memory_1_1SharedRingBuffer.html#a2a448bbd0c5db308a7fac080fe60e4f5", null ],
    [ "operator<<", "classshinda_1_1memory_1_1SharedRingBuffer.html#af52a0ee7c1a368084a888b96c6124b48", null ],
    [ "consumers", "classshinda_1_1memory_1_1SharedRingBuffer.html#aa8f23fef31013544f4bee5f48b43be0a", null ],
    [ "lastTail", "classshinda_1_1memory_1_1SharedRingBuffer.html#ad2f1863089145e9be6fdc5a8cb4891a7", null ],
    [ "maxMsgSize", "classshinda_1_1memory_1_1SharedRingBuffer.html#a84bae8ac7fbe3ab1f9a0434495374249", null ],
    [ "numSegments", "classshinda_1_1memory_1_1SharedRingBuffer.html#a3cedfc4638e8c9a2525263c947e77911", null ],
    [ "producer", "classshinda_1_1memory_1_1SharedRingBuffer.html#afc449d0236d994b6a74995b37cc018b4", null ],
    [ "rawMsgs", "classshinda_1_1memory_1_1SharedRingBuffer.html#a2ccefc335b347b9292a8d4986ee08592", null ],
    [ "size", "classshinda_1_1memory_1_1SharedRingBuffer.html#a035ba4d97003670319b9258dcad4fe46", null ],
    [ "sizeMask", "classshinda_1_1memory_1_1SharedRingBuffer.html#ad222d626886fd0f80d8d6ee70f248516", null ]
];