var structshinda_1_1utils_1_1bench_1_1MeasurementDetails =
[
    [ "writeToFile", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#a84107f528f8f9621babd9f90e054ae89", null ],
    [ "epochs", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#a12f013bc03fb61668830af485a30638e", null ],
    [ "iters", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#ab033bd730822f130385c087de8f066c5", null ],
    [ "logLevel", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#aa4ec76ac5b9e36d526058ddaf796f4b4", null ],
    [ "maxMsgSizePower", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#a42e52848a2df6cb6c776ff647a62f82d", null ],
    [ "msgSize", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#a7a0573fc4fc6580cd2921646a684d158", null ],
    [ "name", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#aaf90e309f39ac23231755a852f675d4a", null ],
    [ "numSubs", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#aa004903bd69f609f13fc3d79ca71da8e", null ],
    [ "time", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#a8df32ac1f3119cecfc54bc4a489b37f1", null ],
    [ "type", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html#a30162e360fc07cfbc6435527d73b2219", null ]
];