var classshinda_1_1socket_1_1server_1_1ISocketSession =
[
    [ "ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a5e98f558ca25bc681c333e842276e966", null ],
    [ "~ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a39bdd6a977beccb39b357443660e5f11", null ],
    [ "ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a4eb7ad7aa91ab3b1dafe47b5d6a71645", null ],
    [ "ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#ae78af88206b79f3e4f37997d067cd158", null ],
    [ "handleRead", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a4ed8d6ffd576629e8e964f3f023143dc", null ],
    [ "operator new[]", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a27c7b9d7835062b40d4e946fb5116f21", null ],
    [ "operator=", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a7466939ba333637402bc12df4c923609", null ],
    [ "operator=", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a513d0ee8642c62f0009dd8f299ea1366", null ],
    [ "sendMessage", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#a4ee93ab527f4ef7b3e7bd9ecc4e1719a", null ],
    [ "start", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#ac2abd182f650679a43e2c129793e48a7", null ],
    [ "stop", "classshinda_1_1socket_1_1server_1_1ISocketSession.html#ac4d1208f5aaea334061f30bca6496fa9", null ]
];