var classshinda_1_1socket_1_1server_1_1ISocketServer =
[
    [ "ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a8812e28a8e809909878c226b77ec42c6", null ],
    [ "~ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a979faa1b0a8637fb53edead083aa1c0c", null ],
    [ "ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a0a62c635fdf5814c0af1a3ba1faf0fae", null ],
    [ "ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a1cf195e575cf9b92e771273683599e69", null ],
    [ "join", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a851d0aa77c74656df18c32d36049664f", null ],
    [ "operator new[]", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#ac1dc91e4fe031414dd0794f1935c4342", null ],
    [ "operator=", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a3d367400257d68f5f449604ad243703e", null ],
    [ "operator=", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#af3c5b5606fb8696774993875d48a7f7b", null ],
    [ "registerCallbacks", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a3496af1d130bc792f6b0d0d08e8fa149", null ],
    [ "sendMessage", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a825abfa75538b5d2b0d1fbe0ea9e7c38", null ],
    [ "start", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#a5ef8a09d86b8bac83f69aef46f8680be", null ],
    [ "stop", "classshinda_1_1socket_1_1server_1_1ISocketServer.html#ae0d3a1b03468322a76fb15f29daea7d5", null ]
];