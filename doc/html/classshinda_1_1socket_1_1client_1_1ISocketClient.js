var classshinda_1_1socket_1_1client_1_1ISocketClient =
[
    [ "ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#aa45fc80c0ea48be970103759925dcc68", null ],
    [ "~ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#ae2bc1a4e461548ea85f2a77f308fb9e7", null ],
    [ "ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#ab3a7303c7ade1b63002891daf05c2170", null ],
    [ "ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#ac58b489b7da933cc54d7e871a99c3847", null ],
    [ "operator new[]", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#a9589835265ec892144616a3b959927a5", null ],
    [ "operator=", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#acea8dee1e7cc65c58e59a92390fd9a6d", null ],
    [ "operator=", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#a602b59f4e403431704c4098568dc4113", null ],
    [ "registerCallbacks", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#a98afb2a6188f8228863a0308b3b83649", null ],
    [ "sendMessage", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#a77c221d8be96ab1b5ed69a8088ff0226", null ],
    [ "start", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#a55bd99dfb63af694cb6533b03c5318b9", null ],
    [ "stop", "classshinda_1_1socket_1_1client_1_1ISocketClient.html#adef52ca071e36e3fbc297c89da314844", null ]
];