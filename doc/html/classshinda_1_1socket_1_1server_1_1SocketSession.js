var classshinda_1_1socket_1_1server_1_1SocketSession =
[
    [ "SocketSession", "classshinda_1_1socket_1_1server_1_1SocketSession.html#aa8845c2c3b1ce46fdf09750642ed7648", null ],
    [ "~SocketSession", "classshinda_1_1socket_1_1server_1_1SocketSession.html#ae30356b46d7266170721db42129e26bc", null ],
    [ "doClose", "classshinda_1_1socket_1_1server_1_1SocketSession.html#ab956e1e592840d492b2dfa204acb47cd", null ],
    [ "handleRead", "classshinda_1_1socket_1_1server_1_1SocketSession.html#ad83dcbb42e663fb2cee36316a13a8e1c", null ],
    [ "id", "classshinda_1_1socket_1_1server_1_1SocketSession.html#aaf9048750f0cc2a7e39e3caa8786cea8", null ],
    [ "id", "classshinda_1_1socket_1_1server_1_1SocketSession.html#a6944a676a5b0fc7de51725e56d4e0fb4", null ],
    [ "onSend", "classshinda_1_1socket_1_1server_1_1SocketSession.html#aa5bd60bc4697e38bf9cc074d5eee9d3c", null ],
    [ "sendMessage", "classshinda_1_1socket_1_1server_1_1SocketSession.html#af6829ae0870f9076e9eceb8bd73ab2ad", null ],
    [ "socket", "classshinda_1_1socket_1_1server_1_1SocketSession.html#aa5b8073384fbfeb10eab2c2769f5290b", null ],
    [ "socket", "classshinda_1_1socket_1_1server_1_1SocketSession.html#ae50a3236f9ac05df4951b45d24e71b64", null ],
    [ "start", "classshinda_1_1socket_1_1server_1_1SocketSession.html#acc8d02f521b6a0c7103d326aed17df2a", null ],
    [ "stop", "classshinda_1_1socket_1_1server_1_1SocketSession.html#a2d950f93c84c28494a617986b990a8d7", null ],
    [ "buffer_", "classshinda_1_1socket_1_1server_1_1SocketSession.html#ade6ec941052c901d605e4545bb395375", null ],
    [ "callbacks_", "classshinda_1_1socket_1_1server_1_1SocketSession.html#aeafb798c8fd2474d0cff7369a015c337", null ],
    [ "id_", "classshinda_1_1socket_1_1server_1_1SocketSession.html#a03bb784740a2656ee504d299c1d3284b", null ],
    [ "ioContext_", "classshinda_1_1socket_1_1server_1_1SocketSession.html#a4f8e1604c1a5bb52a7546999b81911c0", null ],
    [ "sessionClosedEvent", "classshinda_1_1socket_1_1server_1_1SocketSession.html#abf3a0f776e4d0447b1a91572776082ae", null ],
    [ "socket_", "classshinda_1_1socket_1_1server_1_1SocketSession.html#a4e5450bd472443adabffe83fe26efd81", null ]
];