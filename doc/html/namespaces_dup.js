var namespaces_dup =
[
    [ "constants", null, [
      [ "kDefaultConfigPath", "constants_8hpp.html#a6ca852c99b1699fcea8e13c8744d10e9", null ],
      [ "kDefaultMaxMsgPower", "constants_8hpp.html#afa2b75d0a7858a80ae14d89ac8f36b4e", null ],
      [ "kDefaultMinMsgPower", "constants_8hpp.html#aa7b3c741f108a09d5c885874fb5da555", null ],
      [ "kDefaultMmapPermissions", "constants_8hpp.html#a72381c52f3cea1c3abf7fb5612e02d13", null ],
      [ "kDefaultMsgPowerStep", "constants_8hpp.html#a5cec4f3fcc482b1be93320cbf8ab13f6", null ],
      [ "kDefaultNumNops", "constants_8hpp.html#ab842e64c9da3d52e6c448e9e1badf7e6", null ],
      [ "kDefaultNumScheduleYields", "constants_8hpp.html#ade37628846e55c547285d01ea290d806", null ],
      [ "kDefaultPubTimeoutUs", "constants_8hpp.html#ab040c9d5fe198c250542872d3cd98abe", null ],
      [ "kDefaultRingbufferSegments", "constants_8hpp.html#a6ed43496aaf93dc2c0fab856a202f187", null ],
      [ "kDefaultShmPermissions", "constants_8hpp.html#aa8388f46811500e2da63879ada57dd7f", null ],
      [ "kDefaultSocketTimeoutMs", "constants_8hpp.html#a1123e880aca5b4b124fa1b5faf5e859f", null ],
      [ "kDefaultSocketUpdateMs", "constants_8hpp.html#aca772a9f6e7eab1aa055fbf5c3316b8b", null ],
      [ "kDefaultSubTimeoutUs", "constants_8hpp.html#aa027a6c84cdb4c1d053dbbfc42709222", null ],
      [ "kDefaultTransmitMaxPower", "constants_8hpp.html#a040a74c7e1b2bf105f77cc946cd3f95d", null ],
      [ "kDefaultUmaskPermissions", "constants_8hpp.html#a1efb17019e173038757ffb8df2da5f28", null ],
      [ "kMaxSocketConnections", "constants_8hpp.html#abb35e69cd8e036e31458366ca7380bd9", null ],
      [ "kMaxSubscribersPerTopic", "constants_8hpp.html#ae333175e16c59133d7eb8863687cec3f", null ],
      [ "kMinRingbufferSegments", "constants_8hpp.html#afae144aa37b4ffa308afcacc79d4baa0", null ]
    ] ],
    [ "shinda", null, [
      [ "exception", null, [
        [ "ShmBlockError", "classshinda_1_1exception_1_1ShmBlockError.html", "classshinda_1_1exception_1_1ShmBlockError" ],
        [ "DataSizeMismatchError", "classshinda_1_1exception_1_1DataSizeMismatchError.html", "classshinda_1_1exception_1_1DataSizeMismatchError" ],
        [ "SocketError", "classshinda_1_1exception_1_1SocketError.html", "classshinda_1_1exception_1_1SocketError" ]
      ] ],
      [ "memory", null, [
        [ "SharedMemoryBlock", "classshinda_1_1memory_1_1SharedMemoryBlock.html", "classshinda_1_1memory_1_1SharedMemoryBlock" ],
        [ "SharedMsgHeader", "structshinda_1_1memory_1_1SharedMsgHeader.html", "structshinda_1_1memory_1_1SharedMsgHeader" ],
        [ "SharedMsg", "structshinda_1_1memory_1_1SharedMsg.html", "structshinda_1_1memory_1_1SharedMsg" ],
        [ "SharedRingBuffer", "classshinda_1_1memory_1_1SharedRingBuffer.html", "classshinda_1_1memory_1_1SharedRingBuffer" ],
        [ "SharedRingBufferMember", "classshinda_1_1memory_1_1SharedRingBufferMember.html", "classshinda_1_1memory_1_1SharedRingBufferMember" ],
        [ "SharedRingBufferWrapper", "classshinda_1_1memory_1_1SharedRingBufferWrapper.html", "classshinda_1_1memory_1_1SharedRingBufferWrapper" ],
        [ "MemberType", "SharedRingBufferMember_8hpp.html#a0d2e80cc509af740501d881a522dc600", [
          [ "CONSUMER", "SharedRingBufferMember_8hpp.html#a0d2e80cc509af740501d881a522dc600ad5d95361651d17841dd4fab657a7ed66", null ],
          [ "PRODUCER", "SharedRingBufferMember_8hpp.html#a0d2e80cc509af740501d881a522dc600a523178c529429c02cbf9cce92eb47a14", null ]
        ] ],
        [ "SHM_OPEN_TYPE", "SharedMemoryBlock_8hpp.html#a10149b1d288e701e7608a7d696cd0931", [
          [ "CREATE", "SharedMemoryBlock_8hpp.html#a10149b1d288e701e7608a7d696cd0931a294ce20cdefa29be3be0735cb62e715d", null ],
          [ "JOIN", "SharedMemoryBlock_8hpp.html#a10149b1d288e701e7608a7d696cd0931aa6fa1cdf9c1a71978751b35b88763f8f", null ],
          [ "OPEN", "SharedMemoryBlock_8hpp.html#a10149b1d288e701e7608a7d696cd0931aa38bd5138bf35514df41a1795ebbf5c3", null ]
        ] ],
        [ "operator<<", "SharedMemoryBlock_8hpp.html#a5ab87aa15f95b36e40daea8b973ce24f", null ],
        [ "operator<<", "SharedMessage_8cpp.html#a66a6a0f0f552f719fbe61ef49a93e330", null ],
        [ "operator<<", "SharedRingBuffer_8cpp.html#a05fc733b0b50ca685b541f3783fd69c6", null ],
        [ "operator<<", "SharedRingBufferMember_8cpp.html#ae2e0ac5de766d42611fabe41579db423", null ]
      ] ],
      [ "pubsub", null, [
        [ "ShindaBroker", "classshinda_1_1pubsub_1_1ShindaBroker.html", "classshinda_1_1pubsub_1_1ShindaBroker" ],
        [ "ShindaClient", "classshinda_1_1pubsub_1_1ShindaClient.html", "classshinda_1_1pubsub_1_1ShindaClient" ],
        [ "ShindaUser", "structshinda_1_1pubsub_1_1ShindaUser.html", "structshinda_1_1pubsub_1_1ShindaUser" ],
        [ "TopicBlueprint", "classshinda_1_1pubsub_1_1TopicBlueprint.html", "classshinda_1_1pubsub_1_1TopicBlueprint" ],
        [ "Topic", "classshinda_1_1pubsub_1_1Topic.html", "classshinda_1_1pubsub_1_1Topic" ],
        [ "ShindaUserType", "ShindaUser_8hpp.html#aee632a1e13d8785ff6af5f207e52c34c", [
          [ "SUBSCRIBER", "ShindaUser_8hpp.html#aee632a1e13d8785ff6af5f207e52c34cac34fea6778f48f5678c8458ed0de675e", null ],
          [ "PUBLISHER", "ShindaUser_8hpp.html#aee632a1e13d8785ff6af5f207e52c34ca02c25714ac6e2973dfb530a7520c80ae", null ]
        ] ],
        [ "userTypeToString", "ShindaUser_8hpp.html#a89c3827b6012dfdde71318a1e2ce73eb", null ]
      ] ],
      [ "socket", null, [
        [ "client", null, [
          [ "ISocketClientCallbacks", "classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks.html", "classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks" ],
          [ "ISocketClient", "classshinda_1_1socket_1_1client_1_1ISocketClient.html", "classshinda_1_1socket_1_1client_1_1ISocketClient" ],
          [ "SocketClientFactory", "classshinda_1_1socket_1_1client_1_1SocketClientFactory.html", "classshinda_1_1socket_1_1client_1_1SocketClientFactory" ],
          [ "SocketClient", "classshinda_1_1socket_1_1client_1_1SocketClient.html", "classshinda_1_1socket_1_1client_1_1SocketClient" ]
        ] ],
        [ "server", null, [
          [ "ISocketServerCallbacks", "classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks.html", "classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks" ],
          [ "ISocketServer", "classshinda_1_1socket_1_1server_1_1ISocketServer.html", "classshinda_1_1socket_1_1server_1_1ISocketServer" ],
          [ "SocketServerFactory", "classshinda_1_1socket_1_1server_1_1SocketServerFactory.html", "classshinda_1_1socket_1_1server_1_1SocketServerFactory" ],
          [ "ISocketSession", "classshinda_1_1socket_1_1server_1_1ISocketSession.html", "classshinda_1_1socket_1_1server_1_1ISocketSession" ],
          [ "SocketSession", "classshinda_1_1socket_1_1server_1_1SocketSession.html", "classshinda_1_1socket_1_1server_1_1SocketSession" ],
          [ "SocketServer", "classshinda_1_1socket_1_1server_1_1SocketServer.html", "classshinda_1_1socket_1_1server_1_1SocketServer" ]
        ] ],
        [ "seqpacket_protocol", "structshinda_1_1socket_1_1seqpacket__protocol.html", "structshinda_1_1socket_1_1seqpacket__protocol" ],
        [ "SocketNotifier", "classshinda_1_1socket_1_1SocketNotifier.html", "classshinda_1_1socket_1_1SocketNotifier" ],
        [ "socketStatus", "SocketDefinitions_8hpp.html#a987e8631abb71df1c3f67adc906ab036", [
          [ "CONNECTED", "SocketDefinitions_8hpp.html#a987e8631abb71df1c3f67adc906ab036a7fcada5e859227c77950770db7da3ed0", null ],
          [ "DISCONNECTED", "SocketDefinitions_8hpp.html#a987e8631abb71df1c3f67adc906ab036afb22bc9306725c4987f9926322d2c512", null ],
          [ "SHUTDOWN", "SocketDefinitions_8hpp.html#a987e8631abb71df1c3f67adc906ab036ae31ebdc3d5d9a93756082e79e3ce862f", null ]
        ] ],
        [ "PLATFORMD_SOCKET", "SocketDefinitions_8hpp.html#a61836024adc6566fa03d9fac7a4ff817", null ],
        [ "RECONNECT_TIMEOUT", "SocketDefinitions_8hpp.html#a30fce8f323018a2a44fc36eeee0bc3d5", null ],
        [ "SOCKET_BUFFER_SIZE", "SocketDefinitions_8hpp.html#ac72b06d0c94ebbfc49959c0e91b3199e", null ]
      ] ],
      [ "status", "namespaceshinda_1_1status.html", [
        [ "StatusCode", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cf", [
          [ "SUCCESS", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfad0749aaba8b833466dfcbb0428e4f89c", null ],
          [ "BUFFER_FULL", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfaf46524fa13d0e02e2743d1db7279375d", null ],
          [ "BUFFER_EMPTY", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa0b6a3ccf5a6e64dff6a5acf1337ce340", null ],
          [ "SOCKET_TIMEOUT", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa2a63be70c23867e82534f3abc8251ca8", null ],
          [ "NO_CONNECTION", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa9dafba0f78599e40537aec876c30325b", null ],
          [ "MSG_SIZE_MISMATCH", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfab8287579a59ce970afe47fc93cad6767", null ],
          [ "TOPIC_FULL", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa669cf651826a219955b82aaf2a3cf8c6", null ],
          [ "TOPIC_DOES_NOT_EXIST", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa944dbcf70fdbfd7e15bd9b703d45d68b", null ],
          [ "BROKER_ERROR", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa315e738311a7a9f90773f11a09e95656", null ],
          [ "NOT_ENOUGH_MEMORY", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfaea8a564efbb910c22c45506553bb7469", null ],
          [ "TOPIC_NOT_ALLOWED", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfa7fc6bd6922cfa7b8c8a09e3d366c38a2", null ],
          [ "TOPIC_SIZE_NOT_ALLOWED", "namespaceshinda_1_1status.html#af906d0ad677e6b87696a89969b6984cfab2f0a68407baa16cd515ad195232e00b", null ]
        ] ],
        [ "statusCodeToString", "namespaceshinda_1_1status.html#a759360006296896ae7e800c558b26156", null ]
      ] ],
      [ "utils", null, [
        [ "bench", null, [
          [ "sync", null, [
            [ "IPC_Barrier", "classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier.html", "classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier" ]
          ] ],
          [ "MeasurementDetails", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails.html", "structshinda_1_1utils_1_1bench_1_1MeasurementDetails" ],
          [ "CSVWriter", "classshinda_1_1utils_1_1bench_1_1CSVWriter.html", "classshinda_1_1utils_1_1bench_1_1CSVWriter" ],
          [ "Measurement", "classshinda_1_1utils_1_1bench_1_1Measurement.html", "classshinda_1_1utils_1_1bench_1_1Measurement" ],
          [ "MeasurementType", "benchmarking_8hpp.html#a74b5ae2beaf44e7fb9d1245b6afabff0", [
            [ "UNKNOWN", "benchmarking_8hpp.html#a74b5ae2beaf44e7fb9d1245b6afabff0a24308182a1abe00deab9a636beb46386", null ],
            [ "ITERS", "benchmarking_8hpp.html#a74b5ae2beaf44e7fb9d1245b6afabff0a508539ffda106bf3028cee6375405a28", null ],
            [ "CAP", "benchmarking_8hpp.html#a74b5ae2beaf44e7fb9d1245b6afabff0ac46ce8334859369b51d5b475ae733107", null ],
            [ "BASE_CAP", "benchmarking_8hpp.html#a74b5ae2beaf44e7fb9d1245b6afabff0abf46830dc19bdeb157ef713f38fa5e02", null ]
          ] ]
        ] ],
        [ "json", null, [
          [ "parser", null, [
            [ "RSJresource", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJresource.html", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJresource" ],
            [ "RSJparsedData", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJparsedData.html", "classshinda_1_1utils_1_1json_1_1parser_1_1RSJparsedData" ],
            [ "RSJarray", "json_8hpp.html#ac8127277c227514d39cac5b0a5abf1d1", null ],
            [ "RSJobject", "json_8hpp.html#ac59a59be7feb79421ee03d022668f999", null ],
            [ "RSJresourceType", "json_8hpp.html#a7c093bda95a65d72714b5c26c772ed70", [
              [ "RSJ_UNINITIATED", "json_8hpp.html#a7c093bda95a65d72714b5c26c772ed70a195d415c3d22763e0d9bb6f5b40e0979", null ],
              [ "RSJ_UNKNOWN", "json_8hpp.html#a7c093bda95a65d72714b5c26c772ed70a6daa99efbe5e0a2f0d0e11bcafce5f59", null ],
              [ "RSJ_OBJECT", "json_8hpp.html#a7c093bda95a65d72714b5c26c772ed70adf0dcdcbcc1cde88f39fcd2c581e2e5b", null ],
              [ "RSJ_ARRAY", "json_8hpp.html#a7c093bda95a65d72714b5c26c772ed70aa8402efda88705f8e0dd637461b5aa80", null ],
              [ "RSJ_LEAF", "json_8hpp.html#a7c093bda95a65d72714b5c26c772ed70a4fcfa7b4c4ef6233d5eb35d388076522", null ]
            ] ],
            [ "StrTrimDir", "json_8hpp.html#a7ec96aa5f77f61d2dfb07b2e7761d8a8", [
              [ "STRTRIM_L", "json_8hpp.html#a7ec96aa5f77f61d2dfb07b2e7761d8a8abf86fcbc842619283d2cf03aed220583", null ],
              [ "STRTRIM_R", "json_8hpp.html#a7ec96aa5f77f61d2dfb07b2e7761d8a8a29486b0a4dafda3cc5761bdcaf16a088", null ],
              [ "STRTRIM_LR", "json_8hpp.html#a7ec96aa5f77f61d2dfb07b2e7761d8a8afc1df36ddc0b6c478ec04e3ce2d73f7d", null ]
            ] ],
            [ "insert_tab_after_newlines", "json_8hpp.html#ac198c07b7a3661fff0873b1c9617adf2", null ],
            [ "is_bracket", "json_8hpp.html#ac816b67e544779d816cd4de20f53fab1", null ],
            [ "RSJresource::as< std::string >", "json_8hpp.html#a49ca2c4381e1bff13b52aaa6e8b26f9f", null ],
            [ "split_RSJ_array", "json_8hpp.html#a7d94c6f08ae95cb3bfced1f35dfb8d6f", null ],
            [ "strip_outer_quotes", "json_8hpp.html#adbc2230ce0e5109e0c84832a504df65b", null ],
            [ "strtrim", "json_8hpp.html#adf0f0383966b35a41429cf60a419d9ed", null ],
            [ "to_string", "json_8hpp.html#add924008f8257502e59314d9d2588647", null ],
            [ "RSJarraybrackets", "json_8hpp.html#addfa04340c1844dbdce33385f2e6143a", null ],
            [ "RSJarraydelimiter", "json_8hpp.html#ad3f761b36e083438015eb2d00d797b3f", null ],
            [ "RSJbrackets", "json_8hpp.html#ae9f1625a968ae7f51099b10db44c01c2", null ],
            [ "RSJcharescape", "json_8hpp.html#ae400451a40d6b90cb361d89b632e7ab6", null ],
            [ "RSJlinecommentstart", "json_8hpp.html#a42500d2608d49fa40dddb14e0c25d1f3", null ],
            [ "RSJobjectassignment", "json_8hpp.html#a4b05cf2fd88303bca5c6a4d140826485", null ],
            [ "RSJobjectbrackets", "json_8hpp.html#afc02b65369c647c5675051b2f636ff7a", null ],
            [ "RSJprinttab", "json_8hpp.html#adff8ca6f6dee6a081e730203ffbf4b43", null ],
            [ "RSJstringquotes", "json_8hpp.html#a584171bc863665fbdb74ff7cfe0252fd", null ]
          ] ],
          [ "serializer", null, [
            [ "JSON", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON.html", "classshinda_1_1utils_1_1json_1_1serializer_1_1JSON" ],
            [ "Array", "json_8hpp.html#ab3223f73ed1a0ced27d73795938d8bf3", null ],
            [ "Array", "json_8hpp.html#afaa873989f7cd7113039eec698ee5db1", null ],
            [ "Object", "json_8hpp.html#a66485313762103834114ddf791e596b2", null ],
            [ "operator<<", "json_8hpp.html#aeecfad22b5e85e58bdfc1837cfb81c59", null ]
          ] ]
        ] ],
        [ "logging", null, [
          [ "coloring_formatter", "logging_8hpp.html#a49037adca634499f35a32cbe6c899379", null ]
        ] ],
        [ "math", null, [
          [ "circLead", "math_8hpp.html#a4e815ba0da78374c7515bee87334d9a2", null ],
          [ "fitSegments", "math_8hpp.html#a9cd1638b4c1a1e82ae2c545dc5864b60", null ],
          [ "mean", "math_8hpp.html#a779f70962eaf3414013e41bf63cca5f2", null ],
          [ "stdev", "math_8hpp.html#a083bbcbe810cb8bfe6821d13313a97ac", null ]
        ] ],
        [ "random", null, [
          [ "random_string", "random_8hpp.html#aa1afdacda3b284de3230d1e01465d5b9", null ]
        ] ],
        [ "timing", null, [
          [ "DeadlineTimer", "classshinda_1_1utils_1_1timing_1_1DeadlineTimer.html", "classshinda_1_1utils_1_1timing_1_1DeadlineTimer" ],
          [ "steady_microseconds", "timing_8hpp.html#a45932d1ef722fdac651daaa5a9cbabf3", null ],
          [ "steady_milliseconds", "timing_8hpp.html#a17e1f818ae59959ea36383272f9ea0e2", null ],
          [ "steady_nanoseconds", "timing_8hpp.html#aed81db2ef0e8cfa98368c248694eaa41", null ],
          [ "wallClockTime_microseconds", "timing_8hpp.html#a008cbd2bcfdacca184629eabd9a3668e", null ],
          [ "wallClockTime_milliseconds", "timing_8hpp.html#a176e9e58d7b7d028f028503adedc5294", null ],
          [ "wallClockTime_nanoseconds", "timing_8hpp.html#a4ddb36d0983ab97cdfffceb6a20f36f9", null ]
        ] ],
        [ "IdManager", "classshinda_1_1utils_1_1IdManager.html", "classshinda_1_1utils_1_1IdManager" ]
      ] ]
    ] ]
];