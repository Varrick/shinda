var searchData=
[
  ['idmanager_69',['IdManager',['../classshinda_1_1utils_1_1IdManager.html',1,'shinda::utils']]],
  ['ipc_5fbarrier_70',['IPC_Barrier',['../classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier.html',1,'shinda::utils::bench::sync']]],
  ['isocketclient_71',['ISocketClient',['../classshinda_1_1socket_1_1client_1_1ISocketClient.html',1,'shinda::socket::client']]],
  ['isocketclientcallbacks_72',['ISocketClientCallbacks',['../classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks.html',1,'shinda::socket::client']]],
  ['isocketserver_73',['ISocketServer',['../classshinda_1_1socket_1_1server_1_1ISocketServer.html',1,'shinda::socket::server']]],
  ['isocketservercallbacks_74',['ISocketServerCallbacks',['../classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks.html',1,'shinda::socket::server']]],
  ['isocketsession_75',['ISocketSession',['../classshinda_1_1socket_1_1server_1_1ISocketSession.html',1,'shinda::socket::server']]]
];
