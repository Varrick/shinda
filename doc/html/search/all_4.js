var searchData=
[
  ['idmanager_9',['IdManager',['../classshinda_1_1utils_1_1IdManager.html',1,'shinda::utils']]],
  ['iostream_10',['iostream',['../structshinda_1_1socket_1_1seqpacket__protocol.html#a4dbe5c33b58211a0218e8c47545ef0d6',1,'shinda::socket::seqpacket_protocol']]],
  ['ipc_5fbarrier_11',['IPC_Barrier',['../classshinda_1_1utils_1_1bench_1_1sync_1_1IPC__Barrier.html',1,'shinda::utils::bench::sync']]],
  ['isnull_12',['IsNull',['../classshinda_1_1utils_1_1json_1_1serializer_1_1JSON.html#a5bd2ccf37b37f1f80582e27201901e28',1,'shinda::utils::json::serializer::JSON']]],
  ['isocketclient_13',['ISocketClient',['../classshinda_1_1socket_1_1client_1_1ISocketClient.html',1,'shinda::socket::client']]],
  ['isocketclientcallbacks_14',['ISocketClientCallbacks',['../classshinda_1_1socket_1_1client_1_1ISocketClientCallbacks.html',1,'shinda::socket::client']]],
  ['isocketserver_15',['ISocketServer',['../classshinda_1_1socket_1_1server_1_1ISocketServer.html',1,'shinda::socket::server']]],
  ['isocketservercallbacks_16',['ISocketServerCallbacks',['../classshinda_1_1socket_1_1server_1_1ISocketServerCallbacks.html',1,'shinda::socket::server']]],
  ['isocketsession_17',['ISocketSession',['../classshinda_1_1socket_1_1server_1_1ISocketSession.html',1,'shinda::socket::server']]]
];
