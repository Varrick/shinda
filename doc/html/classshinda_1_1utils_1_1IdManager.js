var classshinda_1_1utils_1_1IdManager =
[
    [ "IdManager", "classshinda_1_1utils_1_1IdManager.html#a0d746fc75cc9aa8e248b0db375cd9801", null ],
    [ "getId", "classshinda_1_1utils_1_1IdManager.html#a66408dac950fd8d7b3e85037d54cc3b4", null ],
    [ "isAnyIdAvailable", "classshinda_1_1utils_1_1IdManager.html#a4c3a8bc50de61d879df3e30f8252d5e0", null ],
    [ "isAnyIdInUse", "classshinda_1_1utils_1_1IdManager.html#aee34784329ce4cfc663004b7dde37d38", null ],
    [ "releaseId", "classshinda_1_1utils_1_1IdManager.html#a5ce1da7139a79ca5324c3bcd413ae618", null ],
    [ "maxId_", "classshinda_1_1utils_1_1IdManager.html#a1837e27183523d7684c369d095e3966f", null ],
    [ "minId_", "classshinda_1_1utils_1_1IdManager.html#a2999f990fd8c48637bb0f437a4dc840f", null ],
    [ "numIds_", "classshinda_1_1utils_1_1IdManager.html#a8253f00a848017fef53cc1bca3721e30", null ],
    [ "unused_", "classshinda_1_1utils_1_1IdManager.html#a2b4afd6321b40218a2f3bdfb9261a949", null ],
    [ "used_", "classshinda_1_1utils_1_1IdManager.html#a5349a16563b8d6107eccfe077689ee8c", null ]
];