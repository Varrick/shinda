#!/bin/bash

BASEDIR=$(dirname "$0")
pathToDoc="$BASEDIR/doc/html/index.html"

if which xdg-open > /dev/null; then
  xdg-open $pathToDoc
  echo "It can take some time to open the documentation..."
elif which python > /dev/null; then
  python -mwebbrowser $pathToDoc
  echo "It can take some time to open the documentation..."_
elif which python3 > /dev/null; then
  python3 -mwebbrowser $pathToDoc
  echo "It can take some time to open the documentation..."
else
  echo "Could not find suitable app to open documentation."
  echo "Open documentation manually from relative path: $pathToDoc"
fi