#!/bin/bash

# Update Documentation
./doxy.sh

# Navigate To Project Root
cd ..

# Delete Outdated Package
rm build/shinda.zip > /dev/null

# Create Tree
mkdir -p tmp/shinda/bin
mkdir -p tmp/shinda/lib
mkdir -p tmp/shinda/doc
mkdir -p tmp/shinda/etc
mkdir -p tmp/shinda/include
mkdir -p tmp/shinda/modules
mkdir -p tmp/shinda/src

# Copy Data Into Tree
rsync -arv --exclude={Makefile,shinda.zip,.cmake,CMakeFiles,*.cmake,*.txt,*.json,*.so} ./build/ ./tmp/shinda/bin 
rsync -arvm --include='*.so' --exclude='*' ./build/ ./tmp/shinda/lib 
rsync -arv ./doc/ ./tmp/shinda/doc 
rsync -av ./conf/shinda.service ./tmp/shinda/etc
rsync -av ./conf/config.json ./tmp/shinda/etc
rsync -av ./conf/install.sh ./tmp/shinda
rsync -av ./conf/spinner.sh ./tmp/shinda
rsync -av ./conf/uninstall.sh ./tmp/shinda
rsync -av ./conf/documentation.sh ./tmp/shinda
rsync -av ./include/ ./tmp/shinda/include
rsync -av ./gen/SocketMessages.pb.h ./tmp/shinda/include/pubsub
rsync -av ./cmake/modules/ShindaConfig.cmake ./tmp/shinda/modules
rsync -av ./src/demo/ ./tmp/shinda/src

# ZIP Installer Package
cd tmp
zip -9 -r ../build/shinda.zip ./
cd ..
rm -R ./tmp