#!/bin/bash

: '
BSD 3-Clause License

Copyright (c) 2017, Joe Cooper
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
'

# shellcheck disable=SC2034 disable=SC2039
# Config variables, set these after sourcing to change behavior.
SPINNER_COLORNUM=256 # What color? Irrelevent if COLORCYCLE=1.
SPINNER_COLORCYCLE=0 # Does the color cycle?
SPINNER_DONEFILE="stopspinning" # Path/name of file to exit on.
SPINNER_SYMBOLS="ASCII_PROPELLER" # Name of the variable containing the symbols.
SPINNER_CLEAR=1 # Blank the line when done.

# Handle signals
cleanup () {
	tput rc
	tput cnorm
	return 1
}
# This tries to catch any exit, to reset cursor
trap cleanup INT QUIT TERM

spinner () {
  # Safest option are one of these. Doesn't need Unicode, at all.
  local ASCII_PROPELLER="/ - \\ |"
  local ASCII_PLUS="x +"
  local ASCII_BLINK="o -"
  local ASCII_V="v < ^ >"
  local ASCII_INFLATE=". o O o"

  # Needs Unicode support in shell and terminal.
  # These are ordered most to least likely to be available, in my limited experience.
  local UNI_DOTS="⠋ ⠙ ⠹ ⠸ ⠼ ⠴ ⠦ ⠧ ⠇ ⠏"
  local UNI_DOTS2="⣾ ⣽ ⣻ ⢿ ⡿ ⣟ ⣯ ⣷"
  local UNI_DOTS3="⣷ ⣯ ⣟ ⡿ ⢿ ⣻ ⣽ ⣾"
  local UNI_DOTS4="⠋ ⠙ ⠚ ⠞ ⠖ ⠦ ⠴ ⠲ ⠳ ⠓"
  local UNI_DOTS5="⠄ ⠆ ⠇ ⠋ ⠙ ⠸ ⠰ ⠠ ⠰ ⠸ ⠙ ⠋ ⠇ ⠆"
  local UNI_DOTS6="⠋ ⠙ ⠚ ⠒ ⠂ ⠂ ⠒ ⠲ ⠴ ⠦ ⠖ ⠒ ⠐ ⠐ ⠒ ⠓ ⠋"
  local UNI_DOTS7="⠁ ⠉ ⠙ ⠚ ⠒ ⠂ ⠂ ⠒ ⠲ ⠴ ⠤ ⠄ ⠄ ⠤ ⠴ ⠲ ⠒ ⠂ ⠂ ⠒ ⠚ ⠙ ⠉ ⠁"
  local UNI_DOTS8="⠈ ⠉ ⠋ ⠓ ⠒ ⠐ ⠐ ⠒ ⠖ ⠦ ⠤ ⠠ ⠠ ⠤ ⠦ ⠖ ⠒ ⠐ ⠐ ⠒ ⠓ ⠋ ⠉ ⠈"
  local UNI_DOTS9="⠁ ⠁ ⠉ ⠙ ⠚ ⠒ ⠂ ⠂ ⠒ ⠲ ⠴ ⠤ ⠄ ⠄ ⠤ ⠠ ⠠ ⠤ ⠦ ⠖ ⠒ ⠐ ⠐ ⠒ ⠓ ⠋ ⠉ ⠈ ⠈"
  local UNI_DOTS10="⢹ ⢺ ⢼ ⣸ ⣇ ⡧ ⡗ ⡏"
  local UNI_DOTS11="⢄ ⢂ ⢁ ⡁ ⡈ ⡐ ⡠"
  local UNI_DOTS12="⠁ ⠂ ⠄ ⡀ ⢀ ⠠ ⠐ ⠈"
  local UNI_BOUNCE="⠁ ⠂ ⠄ ⠂"
  local UNI_PIPES="┤ ┘ ┴ └ ├ ┌ ┬ ┐"
  local UNI_HIPPIE="☮ ✌ ☺ ♥"
  local UNI_HANDS="☜ ☝ ☞ ☟"
  local UNI_ARROW_ROT="➫ ➭ ➬ ➭"
  local UNI_CARDS="♣ ♤ ♥ ♦"
  local UNI_TRIANGLE="◢ ◣ ◤ ◥"
  local UNI_SQUARE="◰ ◳ ◲ ◱"
  local UNI_BOX_BOUNCE="▖ ▘ ▝ ▗"
  local UNI_PIE="◴ ◷ ◶ ◵"
  local UNI_CIRCLE="◐ ◓ ◑ ◒"
  local UNI_QTR_CIRCLE="◜ ◝ ◞ ◟"

  # Bigger spinners and progress type bars; takes more space.
  local WIDE_ASCII_PROG="[>----] [=>---] [==>--] [===>-] [====>] [----<] [---<=] [--<==] [-<===] [<====]"
  local WIDE_ASCII_PROPELLER="[|####] [#/###] [##-##] [###\\#] [####|] [###\\#] [##-##] [#/###]"
  local WIDE_ASCII_SNEK="[>----] [~>---] [~~>--] [~~~>-] [~~~~>] [----<] [---<~] [--<~~] [-<~~~] [<~~~~]"
  local WIDE_UNI_GREYSCALE="░░░░░░░ ▒░░░░░░ ▒▒░░░░░ ▒▒▒░░░░ ▒▒▒▒░░░ ▒▒▒▒▒░░ ▒▒▒▒▒▒░ ▒▒▒▒▒▒▒ ▒▒▒▒▒▒░ ▒▒▒▒▒░░ ▒▒▒▒░░░ ▒▒▒░░░░ ▒▒░░░░░ ▒░░░░░░ ░░░░░░░"
  local WIDE_UNI_GREYSCALE2="░░░░░░░ ▒░░░░░░ ▒▒░░░░░ ▒▒▒░░░░ ▒▒▒▒░░░ ▒▒▒▒▒░░ ▒▒▒▒▒▒░ ▒▒▒▒▒▒▒ ░▒▒▒▒▒▒ ░░▒▒▒▒▒ ░░░▒▒▒▒ ░░░░▒▒▒ ░░░░░▒▒ ░░░░░░▒"

  local SPINNER_NORMAL
  SPINNER_NORMAL=$(tput sgr0)

  eval SYMBOLS=\$${SPINNER_SYMBOLS}

  # Get the parent PID
  SPINNER_PPID=$(ps -p "$$" -o ppid=)
  while :; do
    tput civis
    for c in ${SYMBOLS}; do
      if [ $SPINNER_COLORCYCLE -eq 1 ]; then
        if [ $SPINNER_COLORNUM -eq 7 ]; then
          SPINNER_COLORNUM=1
        else
          SPINNER_COLORNUM=$((SPINNER_COLORNUM+1))
        fi
      fi
      local COLOR
      COLOR=$(tput setaf ${SPINNER_COLORNUM})
      tput sc
      env printf "${COLOR}${c}${SPINNER_NORMAL}"
      tput rc
      if [ -f "${SPINNER_DONEFILE}" ]; then
        if [ ${SPINNER_CLEAR} -eq 1 ]; then
          tput el
        fi
        rm ${SPINNER_DONEFILE}
        break 2
      fi
      # This is questionable. sleep with fractional seconds is not
      # always available, but seems to not break things, when not.
      env sleep .2
      # Check to be sure parent is still going; handles sighup/kill
      if [ ! -z "$SPINNER_PPID" ]; then
        # This is ridiculous. ps prepends a space in the ppid call, which breaks
        # this ps with a "garbage option" error.
        # XXX Potential gotcha if ps produces weird output.
        # shellcheck disable=SC2086
        SPINNER_PARENTUP=$(ps --no-headers $SPINNER_PPID)
        if [ -z "$SPINNER_PARENTUP" ]; then
          break 2
        fi
      fi
    done
  done
  tput cnorm
  return 0
}

spinner &

CYAN='\033[1;36m'
NC='\033[0m' # No Color
echo -e "${CYAN}Starting installation...${NC}"
BASEDIR=$(dirname "$0")

# Install UserGroup
if [ $(getent group shinda) ]; then
  true
else
  groupadd shinda
  echo "Group shinda created."
fi

user_name=$(logname 2>/dev/null || echo ${SUDO_USER:-${USER}})
if getent group shinda | grep -q "\b${user_name}\b"; then
    true
else
    usermod -a -G shinda ${user_name} > /dev/null
    echo "User '${user_name}' added to group shinda."
fi

# Install Shared Library
echo "Installing libraries..."
rm -R /usr/lib/shinda 2> /dev/null
mkdir -p /usr/lib/shinda > /dev/null
if [ ! -e /etc/ld.so.conf.d/shinda.conf ] ; then
    touch /etc/ld.so.conf.d/shinda.conf > /dev/null
fi
grep -q "/usr/lib/shinda" /etc/ld.so.conf.d/shinda.conf || echo "/usr/lib/shinda" >> /etc/ld.so.conf.d/shinda.conf
mv -v $BASEDIR/lib/libShinda.so /usr/lib/shinda > /dev/null
ldconfig > /dev/null

# Install Headers
echo "Installing headers..."
rm -R /usr/include/shinda 2> /dev/null
mkdir -p /usr/include/shinda > /dev/null
mv -v $BASEDIR/include/* /usr/include/shinda > /dev/null

# Install CMake Modules
echo "Installing Cmake modules..."
mkdir -p /usr/lib/shinda/cmake > /dev/null
mv -v $BASEDIR/modules/* /usr/lib/cmake/shinda > /dev/null

# Install Broker systemd Service
echo "Installing Broker systemd service..."
systemctl stop shinda > /dev/null 2>&1
systemctl disable shinda > /dev/null 2>&1
systemctl daemon-reload > /dev/null 2>&1
rm -R /usr/bin/shinda 2> /dev/null
rm -R /etc/systemd/system/shinda.service 2> /dev/null
mkdir -p /usr/bin/shinda > /dev/null
mkdir -p /etc/shinda > /dev/null
mv -v $BASEDIR/bin/ShindaBroker /usr/bin/shinda > /dev/null
mv -v $BASEDIR/etc/config.json /etc/shinda > /dev/null
mv -v $BASEDIR/etc/shinda.service /etc/systemd/system/shinda.service > /dev/null
chmod 664 /etc/systemd/system/shinda.service > /dev/null
systemctl daemon-reload > /dev/null 2>&1
systemctl enable shinda > /dev/null 2>&1
systemctl start shinda > /dev/null 2>&1

# Cleanup
echo "Cleaning up directory..."
rm -R $BASEDIR/include 2> /dev/null
rm -R $BASEDIR/lib 2> /dev/null
rm -R $BASEDIR/etc 2> /dev/null
rm -R $BASEDIR/modules 2> /dev/null
rm $BASEDIR/install.sh 2> /dev/null

# Install Dependencies
echo "Installing dependencies..."
apt-get install cmake -y > /dev/null
apt-get install rsync -y > /dev/null
apt-get install zip -y > /dev/null
apt-get install wget -y > /dev/null
apt-get install libboost-all-dev -y > /dev/null
apt-get install libopencv-dev -y > /dev/null
apt-get install python3-opencv -y > /dev/null
apt-get install mosquitto -y > /dev/null
apt-get install mosquitto-clients -y > /dev/null
apt-get install libmosquitto-dev -y > /dev/null
apt-get install doxygen -y > /dev/null

# Protobuf from source
apt-get install autoconf automake libtool curl make g++ unzip -y > /dev/null
if command protoc --version != "libprotoc 3.15.8" > /dev/null
then
  echo -e "Protobuf satisfied with version 3.15.8." > /dev/null
else
  wget https://github.com/protocolbuffers/protobuf/releases/download/v3.15.8/protobuf-cpp-3.15.8.tar.gz > /dev/null
  tar -xf protobuf-cpp-3.15.8.tar.gz > /dev/null
  cd protobuf-3.15.8 > /dev/null
  ./autogen.sh > /dev/null
  ./configure --prefix=/usr > /dev/null
  make -j$(nproc) > /dev/null
  make install > /dev/null
  ldconfig > /dev/null
  cd .. > /dev/null
  rm -R protobuf-3.15.8 > /dev/null
  rm -R protobuf-cpp-3.15.8.tar.gz > /dev/null
fi

RED='\033[1;31m'
NC='\033[0m' # No Color
echo -e "${RED}Please restart your system in order to use shinda properly!${NC}"
touch stopspinning
rm ./stopspinning