#!/bin/sh

echo "Running doxygen...."
doxygen --version
doxygen Doxyfile
echo "------------------------------------------"
echo "Finished updating documentation." _