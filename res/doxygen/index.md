@anchor hello_there
<span style="font-size:x-large;">**Hello There!**</span>

<span style="font-size:large;">**Shinda** is an easy to use library for transferring data via shared memory SPMC circular buffer between applications on the same target system with highest reachable bandwidth and a minimum overhead publish/subscribe architecture like MQTT.</span>

---

@anchor installation
# Installation 

<span style="font-size:large;">**Option A**</span>: You compiled the project by yourself.
	- Open a terminal in the ./conf folder of the project and execute:

			./createInstaller
	
	- Pick up shinda.zip from the ./build folder and extract it at any destination you like  

<span style="font-size:large;">**Option B**</span>: You got the archive pre-compiled from the [Release Page](https://gitlab.com/Varrick/shinda/-/releases).
	- Extract the archive, enter /build and extract the contained zip file at any target destination you like


Now follow the instructions below:

---

1.  Open a terminal in the root shinda folder and run the install.sh script as follows:
    - Make sure you have super-user rights!

            sudo ./install.sh
        
2.  The library should now be installed on your system
    - Check if the library was installed successfully by typing:

            ldconfig -p | grep libShinda

    - If anything is found, the installation was successfull
    - Check if the system service was installed successfully by typing:
	    
		    systemctl status shinda
		    
	- If installation was successfull you should see "Active: active (running)" with green background
    
3. You can test the library by starting the Demo binaries

    1. Open another terminal in the shinda/bin folder and type 

             sudo ./ClientNumberDemo --pub 1 --logLevel 2 --updateRate 1000

    2. Open another terminal in the shinda/bin folder and type 

             sudo ./ClientNumberDemo --pub 0 --logLevel 2 --updateRate 1000

    3. Look at the data flowing from one process via shared memory to the other
    4. You can quit the processes at any time by using CTRL+C in the respective terminals
    5. You can view the status of the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) running in the background by opening a terminal anywhere:
        
             sudo systemctl status shinda
            
    6. You can view the log of the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) running in the background by opening a terminal anywhere:
           
             sudo journalctl -a -u shinda
        
    5. Congratulations! You are now ready to take advantage of high bandwidth inter-process communication!

<br/>

---

@anchor removal
# Removal
1. For clean uninstallation of **Shinda** from you system open a terminal in your root shinda folder and type:
	- Make sure you have super-user rights!
			
			sudo ./uninstall.sh
			
2. You can check, if uninstalling was successfull by typing in the same terminal:
	- Check if the shared libraries were removed
		
			ldconfig -p | grep libShinda
			
	- You should not get any result from that call
	- Check if the system service was removed
			
			systemctl status shinda
	
	- The OS should inform you, that such a service does not exist

<br/>

---

@anchor usage
# Usage
To use the library in your very own project, you have to link against the library's shared object file as well as include the respective headers. You can do this via the shipped custom CMake Find - Module which will be automatically installed so it can be found by CMake.

1. Create your Client applications by taking a look at the sample code in ClientDemo.cpp and the code documentation. 
	- For basic usage you will need to at least include:
 		
			#include <shinda/pubsub/Client.hpp>
	
2. Link your executable(s) to the **Shinda** library 
	- Shinda ships with a custom CMake Find - Module automatically installed.
	- Use following commands to link via your CMakeLists.txt:
	
			find_package(Shinda REQUIRED)
			if(SHINDA_FOUND)
				include_directories(<your other includes> ${SHINDA_INCLUDE_DIRS})
			    target_link_libraries(<your executable> ${SHINDA_LIBRARIES})
			endif()

	- You can alternatively try to set the respective paths manually in your CMakeLists.txt: 

			set(SHINDA_INCLUDE_DIRS /usr/include/shinda)
			set(SHINDA_LIBRARIES /usr/lib/shinda/libShinda.so)
			include_directories(<your other includes> ${SHINDA_INCLUDE_DIRS})
			target_link_libraries(<your executable> ${SHINDA_LIBRARIES})

3. Start your client application(s)
4. Profit.

@anchor example
## Example Program
Let's build a simple program to see in-depth how to actually use the library! 
So, what do we want to transfer? Let's suppose we want to transmit images captured via webcam with OpenCV between applications!
You can also find this program as source code in the project with additional things like fps information.

@anchor publisher
### Publisher

Include the header:

\code{.cpp}
#include <shinda/pubsub/Client.hpp>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
\endcode

Define properties of our images:

~~~~~~~~~~~~~{.cpp}
#define ROWS		480
#define COLS		640
#define CHANNELS	3
#define TYPE		16
~~~~~~~~~~~~~

Initialize our image container:

~~~~~~~~~~~~~{.cpp}
cv::Mat frame = cv::Mat::zeros(ROWS, COLS, TYPE);
~~~~~~~~~~~~~

Calculate image size:

~~~~~~~~~~~~~{.cpp}
unsigned int maxImgSize = ROWS * COLS * CHANNELS;
~~~~~~~~~~~~~

Initialize the ShindaClient. It must know the datatype to transmit (*uchar* from OpenCV), the name of the [Topic](@ref shinda.pubsub.Topic) to which to publish to & the maximum size the data will have:

~~~~~~~~~~~~~{.cpp}
shinda::pubsub::ShindaClient<cv::uchar> client("WebCamDemo", maxImgSize);
~~~~~~~~~~~~~

Connect to the shinda [ShindaBroker](@ref shinda.pubsub.ShindaBroker) & exit if not reachable:

~~~~~~~~~~~~~{.cpp}
if(client.connect() != status::StatusCode::SUCCESS)
	exit(1);
~~~~~~~~~~~~~

Initialize OpenCV Webcam:

~~~~~~~~~~~~~{.cpp}
cv::VideoCapture cap;
int apiID = cv::CAP_ANY;      // 0 = autodetect cam default API
cap.open(deviceId, apiID);
if (!cap.isOpened()) {
	exit(1);
}
cap.set(cv::CAP_PROP_FOURCC, cv::VideoWriter::fourcc('M','J','P','G'));
cap.set(cv::CAP_PROP_FRAME_WIDTH, cols);
cap.set(cv::CAP_PROP_FRAME_HEIGHT, rows);
~~~~~~~~~~~~~

Write data to shared memory with shinda:

~~~~~~~~~~~~~{.cpp}
for (;;)
{ 
	// Capture frame from the camera
	cap.read(frame);
	if (frame.empty()) {
		break;	// invalid frame grabbed
	}

	// Publish frame as message to shared memory
	if(client.publish(frame.data, imgSize) != status::StatusCode::SUCCESS)
		exit(1);	// Something went wrong on writing
}
~~~~~~~~~~~~~

@anchor subscriber
### Subscriber 

Include the headers:

~~~~~~~~~~~~~{.cpp}
#include <shinda/pubsub/Client.hpp>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
~~~~~~~~~~~~~

Define properties of our images:

~~~~~~~~~~~~~{.cpp}
#define ROWS		480
#define COLS		640
#define CHANNELS	3
#define TYPE		16
~~~~~~~~~~~~~

Initialize our image container:

~~~~~~~~~~~~~{.cpp}
cv::Mat frame = cv::Mat::zeros(ROWS, COLS, TYPE);
~~~~~~~~~~~~~

Calculate image size:

~~~~~~~~~~~~~{.cpp}
unsigned int maxImgSize = ROWS * COLS * CHANNELS;
~~~~~~~~~~~~~
 
Initialize the [ShindaClient](@ref shinda.pubsub.ShindaClient). It must know the datatype to transmit (*uchar* from OpenCV), the name of the [Topic](@ref shinda.pubsub.Topic) to which to subscribe to & the maximum size the data will have:

~~~~~~~~~~~~~{.cpp}
shinda::pubsub::ShindaClient<cv::uchar> client("WebCamDemo", maxImgSize);
~~~~~~~~~~~~~

Connect to the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) & exit if not reachable:

~~~~~~~~~~~~~{.cpp}
if(client.connect() != status::StatusCode::SUCCESS)
	exit(1);
~~~~~~~~~~~~~

Read data from shared memory to our image container and show the image:

~~~~~~~~~~~~~{.cpp}
if(client.spin(frame.data) == status::StatusCode::SUCCESS) 
{
	imshow("Live", frame);
	if (waitKey(1) >= 0) {
		break;
	}
}
~~~~~~~~~~~~~

<br/>

---

@anchor demo_bins
# Demos

@anchor string_demo
## ClientStringDemo
This script launches a Client, which wants to write/read data to/from a certain preset [Topic](@ref shinda.pubsub.Topic) via shared memory. Writing or reading as well as other options can be set via the command line args. The [ShindaBroker](@ref shinda.pubsub.ShindaBroker) process must be in active execution for the clients to establish a connection via shared memory. This should always be the case after installation. You can check if the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) is running by typing in any terminal:

	systemctl status shinda

@anchor string_cla
### Command Line Arguments
| Parameter | Explanation  |
|---|---|
| --pub | 1 = Process will be writing to [Topic](@ref shinda.pubsub.Topic), 0 = Process will read from [Topic](@ref shinda.pubsub.Topic). |
| --logLevel | Amount of logging from ClientDemo execution to be shown ranging from 0 - 5 with 0 showing the most details. |
| --iters | Number of reads/writes to perform before process shuts down. |
| --updateRate | Rate in milliseconds at which messages will be written/read to/from shared memory/[Topic](@ref shinda.pubsub.Topic). |
| --maxDataSize | Maximum number of bytes one plans to transmit through shared memory at a time. |


@anchor num_demo
## ClientNumberDemo
This script launches a Client, which wants to write/read increasing numbers to/from a certain preset [Topic](@ref shinda.pubsub.Topic) via shared memory. Writing or reading as well as other options can be set via the command line args. The [ShindaBroker](@ref shinda.pubsub.ShindaBroker) process must be in active execution for the clients to establish a connection via shared memory. This should always be the case after installation. You can check if the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) is running by typing in any terminal:

	systemctl status shinda

@anchor num_cla
### Command Line Arguments
| Parameter | Explanation  |
|---|---|
| --pub | 1 = Process will be writing to [Topic](@ref shinda.pubsub.Topic), 0 = Process will read from [Topic](@ref shinda.pubsub.Topic). |
| --updateRate | Rate in milliseconds at which messages will be written/read to/from 
| --logLevel | Amount of logging from ClientDemo execution to be shown ranging from 0 - 5 with 0 showing the most details. |

@anchor cam_demo
## ClientCamDemo
This script launches a Client, which writes/reads images from the system default camera to/from a certain preset Topic via shared memory and is able to visualize them in a GUI. Writing or reading as well as other options can be set via the command line args. The [ShindaBroker](@ref shinda.pubsub.ShindaBroker) process must be in active execution for the clients to establish a connection via shared memory. This should always be the case after installation. You can check if the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) is running by typing in any terminal:

	systemctl status shinda

@anchor cam_cla
### Command Line Arguments
| Parameter | Explanation  |
|---|---|
| --pub | 1 = Process will be writing to [Topic](@ref shinda.pubsub.Topic), 0 = Process will read from [Topic](@ref shinda.pubsub.Topic). |
| --logLevel | Amount of logging from ClientDemo execution to be shown ranging from 0 - 5 with 0 showing the most details. |
| --shinda | 1 = Activates shinda shared memory transfer, 0 = Not shared memory transfer |
| --show | 1 = Shows current camera dataframes in a window, 0 = Does not show any image data while execution. |
| --deviceId | Selects camera. Default is zero which selects the system default camera. |
| --rows | Height of the images to capture, transfer and show. |
| --cols | Width of the images to capute, transfer and show. |


<br/>

---

@anchor config_params
# Configuration

@anchor service
## shinda.service 
| Parameter | Explanation  |
|---|---|
| User | User which runs the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) at boot. Default is 'root'. |
| Group | UserGroup which is allowed to connect to the [ShindaBroker](@ref shinda.pubsub.ShindaBroker). Every process running [ShindaClient](@ref shinda.pubsub.ShindaClient) must be in this group. Default is 'shinda'. **BEWARE**: The user executing the installation script is automatically added to UserGroup 'shinda' on installation. If you change the user group you have to take care to adjust the installation process. |

@anchor config
## config.json 
| Parameter | Explanation  |
|---|---|
| max_memory_usage | Maximum usable RAM by Shinda for creation of [Topic](@ref shinda.pubsub.Topic). |
| max_topic_size | Maximum usable RAM by Shinda for every individual [Topic](@ref shinda.pubsub.Topic). |
| secure_mode | If activated, only allows Clients to create Topics pre-defined in topics array. If number of segments is not possible because of memory limitations, the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) will choose segments according to available memory or fail. This behaviour is only true for Topics which don't have the essential flag. If essential flag is set and not enough memory is available for specified number of segments, creating the [Topic](@ref shinda.pubsub.Topic) will fail. |
| topics | Array of pre-defined Topics which are the only allowed topics if secure mode is active. Array elements are to be defined as follows:<br/>{<br/>&nbsp;&nbsp;&nbsp;"name": <[Topic](@ref shinda.pubsub.Topic) name as string>, <br/>&nbsp;&nbsp;&nbsp;"maxDataSize": <Maximum size of data to be transmitted as unsigned int>,<br/>&nbsp;&nbsp;&nbsp;"numSegments": <Number of ringbuffer segments as unsigned int>,<br/>&nbsp;&nbsp;&nbsp;"essential": <If memory shall be reserved for this Topic to guarantee that it is creatable as defined as boolean><br/>} |

@anchor constants
## constants.hpp
| Parameter | Explanation  |
|---|---|
| kMinRingbufferSegments| Minimum number of segments of a circular buffer the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) will consider at creation of a [Topic](@ref shinda.pubsub.Topic). |
| kMaxSubscribersPerTopic | Maximum number of Clients the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) will allow to register at every [Topic](@ref shinda.pubsub.Topic) as subscriber. |
| kMaxSocketConnections | Maximum number of allowed connections at the same time on the Unix Domain Socket. |
| kDefaultPubTimeoutUs | Default timeout in microseconds for publishing a message as Client to a certain [Topic](@ref shinda.pubsub.Topic). |
| kDefaultSubTimeoutUs | Default timeout in microseconds for reading a message as Client from a certain [Topic](@ref shinda.pubsub.Topic). |
| kDefaultSocketTimeoutMs | Default timeout in milliseconds after which a Client will classify a request to the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) via the Unix Domain Socket as failed. |
| kDefaultSocketUpdateMs | Default update rate in milliseconds at which the client will check if the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) has sent a reply to previously sent request.  |
| kDefaultRingbufferSegments | Default number of ringbuffer segments the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) will try to create Topics with. |
| kDefaultUmaskPermissions | Permission mask for every file, the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) creates on the file system (Socket, Shared Memory). Controls, which users are allowed to use Shinda. |
| kDefaultShmPermissions | Permissions for the Shared Memory files created on [Topic](@ref shinda.pubsub.Topic) creation by the [ShindaBroker](@ref shinda.pubsub.ShindaBroker). Gets masked by DEFAULT_UMASK_PERMISSIONS. |
| kDefaultMmapPermissions | Permissions for the  mapped Shared Memory files created on [Topic](@ref shinda.pubsub.Topic) creation by the [ShindaBroker](@ref shinda.pubsub.ShindaBroker). Gets masked by DEFAULT_UMASK_PERMISSIONS |
| kDefaultConfigPath | Default path to the config.json file the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) reads at startup. |
| kDefaultNumNops | Default number of NOP operations in adaptive polling scheme before scheduler gets called. |
| kDefaultNumScheduleYields | Default number of scheduler calls to be done in adaptive polling scheme before waiting of conditional variable is started. |
| kDefaultTransmitMaxPower | Total amount of data as power of 2 to transfer for benchmarks. |
| kDefaultMaxMsgPower | Ending message size power of 2 for benchmarks. |
| kDefaultMinMsgPower | Starting message size power of 2 for benchmarks. |
| kDefaultMsgPowerStep | Default step width of power of 2 for message sizes for benchmarks. |

@anchor macros
## macros.hpp
| Parameter | Explanation  |
|---|---|
| LOG_DEBUGGING| Allows for configuring if debug level information from within the library shall be kept on compilation. **If activated, expect severe slowdown of library because boost logging has a huge performance impact** |
| LOG_APPLICATION | Allows for configuring if application level logs e.g. publisher joining topic events shall be kept on compilation. |

<br/>

---

@anchor concept
# Concept

@anchor motivation
## Motivation

Recent developments in cloud computing have already led to a revolution in how people use and store their data. However, there are areas not applicable for this approach due to its natural limitations: Bandwidth, latency, security and the lack of offline access. 
As one solution, edge computing can be proposed. Edge computing means storing data and executing computations close to the user and/or the device, such that information can be shared quickly, securely and without further latency. [[1]](#1) 

With those advantages in mind, the edge computing approach is trending towards a microservice architecture where applications are decomposed into smaller pieces of software running in their own containers and communicating with one another based on inter-process communication schemes [[2]](#2). 
Problems arise as soon as the hardware resources in the edge computing scenario are limited. Special attention must therefore be granted to inefficiencies in communication on the same target platform with classical approaches such as pipes or libraries like MQTT relying on large overhead TCP/IP protocols. 

At this point, the Shinda library steps in; using shared-memory which is the fastest possible inter-process communication tool with proper synchronization mechanisms and a publish/subscribe architecture like MQTT or ZeroMQ but with minimum overhead. The main advantage of shared memory is that maximum throughput is easily achieved because data has not to be copyied multiple times like in classical approaches.
As of that, Shinda aims to be an as easy as MQTT to be used library for transferring data through shared memory between applications on the same target system with the highest reachable bandwidth.

@anchor idea
## General Idea
As mentioned before, the general idea is to implement a publish/subscribe architecture based on data exchange via shared memory. To achieve this, Shinda is split into two major components: [ShindaClient](@ref shinda.pubsub.ShindaClient) & [ShindaBroker](@ref shinda.pubsub.ShindaBroker). As common in such an architecture, data exchange is organized in so called 'Topics'. A [Topic](@ref shinda.pubsub.Topic), represented by a system-wide uniquely defined name, represents a certain type of data and can be used to publish or receive that data by Clients. Each Topics lifetime is managed by the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) and has a single [SharedMemoryBlock](@ref shinda.memory.SharedMemoryBlock) which contains a [SharedRingBuffer](@ref shinda.memory.SharedRingBuffer) through which [SharedMessage](@ref shinda.memory.SharedMessage) objects containing the data to be exchanged are transferred. The synchronization scheme used for transferring the data is explained in detail at [Synchronization](@ref synchronization). For Clients to able to get access to the shared memory, they must connect to the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) and register themself to the [Topic](@ref shinda.pubsub.Topic) they want to be part of. This communication is done by utilizing Protobuf protocol and Unix Domain Sockets. The [SharedRingBuffer](@ref shinda.memory.SharedRingBuffer), through which data is transmitted, is a SPMC [Buffer](@ref buffer); only one publisher but multiple consumers are allowed per [Topic](@ref shinda.pubsub.Topic). Messages which were published to a certain [Topic](@ref shinda.pubsub.Topic) are guaranteed to be delivered for any currently subscribed process. See the following image for an overview of the architecture of Shinda:

@image html ../res/GeneralConcept.png width=75%

@anchor broker
## Broker
The [ShindaBroker](@ref shinda.pubsub.ShindaBroker) is running as a systemd service in the background and starts itself at bootup. Every Client that wants to exchange data, must reach out to the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) via Unix Domain Sockets and request a [Topic](@ref shinda.pubsub.Topic) to publish/subscribe to.  

For a [Topic](@ref shinda.pubsub.Topic) to be created, there are multiple necessary conditions to be met:
- [Topic](@ref shinda.pubsub.Topic) with similiar name must not exist
- Enough memory must be available to create the [SharedMemoryBlock](@ref shinda.memory.SharedMemoryBlock) (can be configured at [Config](@ref config))

For registering at an existing [Topic](@ref shinda.pubsub.Topic), there are also multiple necessary conditions to be met:
- [Topic](@ref shinda.pubsub.Topic) with that name must exist
- For registering as publisher, there must be no other publisher already for this [Topic](@ref shinda.pubsub.Topic)
- For registering as subscriber, there must be enough slots for Subscribers left (can be changed at [Constants](@ref constants))
- Client must expect the same maximum data size of a message to be transferred

Any Client can also [unregister](@ref shinda.pubsub.ShindaClient.unregister) itself from any [Topic](@ref shinda.pubsub.Topic) it has previously joined. If no Client is registered for a certain [Topic](@ref shinda.pubsub.Topic) anymore, the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) will automatically destroy that [Topic](@ref shinda.pubsub.Topic) and release all memory associated with it. If any Client exits ungracefully, it is automatically removed from all previously associated Topics.

At startup, the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) will read the config file at "/etc/shinda/config.json" (can be changed at [Constants](@ref constants)).
This file contains configuration parameters as defined in [Config](@ref config). In there, one can set the total amount of memory from RAM which shall be dedicated for creating Topics and the total amount of memory allowed for each individual [Topic](@ref shinda.pubsub.Topic). Also one can pre-define an array of allowed Topics with the option to turn on secure mode, which will only allow Clients to create those pre-defined Topics. If secure mode is turned off, any [Topic](@ref shinda.pubsub.Topic) can be created as long as enough memory is made available. The number of elements of a circular buffer is dynamically determined by the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) by available memory. Only pre-defined Topics can be configured to have a certain amount of elements in the circular buffer. Only pre-defined Topics with the essential flag are guaranteed to have that specific number of elements as long as memory is available.

@anchor client
## Client
A [ShindaClient](@ref shinda.pubsub.ShindaClient) always represents one [Topic](@ref shinda.pubsub.Topic) and can be publisher and subscriber to that [Topic](@ref shinda.pubsub.Topic) at the same time. For its creation, certain necessary parameters are needed:
- System-wide unique name of the [Topic](@ref shinda.pubsub.Topic)
- Maximum size of data to be transmitted through shared memory
- Data type of the data to be transmitted

To be able to publish or subscribe to a [Topic](@ref shinda.pubsub.Topic), it must register itself for that specific [Topic](@ref shinda.pubsub.Topic) at the [ShindaBroker](@ref shinda.pubsub.ShindaBroker) via Unix Domain Socket communication. This is done automatically if the [publish](@ref shinda.pubsub.ShindaClient.publish) or [spin](@ref shinda.pubsub.ShindaClient.spin) method of [ShindaClient](@ref shinda.pubsub.ShindaClient) is called.  
The user only has to do the following to get started:
1. Create the [ShindaClient](@ref shinda.pubsub.ShindaClient) object
2. Call [connect](@ref shinda.pubsub.ShindaClient.connect) to establish a connection to the [ShindaBroker](@ref shinda.pubsub.ShindaBroker)
3. Call [publish](@ref shinda.pubsub.ShindaClient.publish)/[spin](@ref shinda.pubsub.ShindaClient.spin) for publishing/subscribing

For publishing and subscribing, there is the option to independently specify a timeout after which reading/writing a message to shared memory is aborted. If no timeout is set, the Client will block forever until it can finish its task.  
A Client can also [unregister](@ref shinda.pubsub.ShindaClient.unregister) from any [Topic](@ref shinda.pubsub.Topic) to make room for other processes or if it does not longer want to take that role.

@anchor buffer
## Buffer
For exchange of data, the fastest possible inter-process mechanism is chosen: shared memory. To utilize this memory in an efficient way for data transmission, a single circular buffer is used. As of that, every [Topic](@ref shinda.pubsub.Topic) creates one shared memory region in which such a circular buffer ([SharedRingBuffer](@ref shinda.memory.SharedRingBuffer)) is put into. The circular buffer can be described as follows:
- Latch-free
- SPMC - Single Producer Multiple Consumer
- Messages are guaranteed to be delivered
- Producer can't overtake consumers
- Consumers can't overtake producer
- Number of Segments: &ensp;&ensp; \f$ N \in \{2^x : x \in \mathbb{N},\; \log_2(\text{constants::kMinRingbufferSegments }) \leq x \leq \log_2(\text{DEFAULT_RINGBUFFER_SEGMENTS}) \}\f$ (can be changed at [Constants](@ref constants))
- Number of Consumers: &ensp; \f$ C \in \{x : x \in \mathbb{N},\; 0 \leq x \leq \text{MAX_SUBSCRIBERS_PER_TOPIC}\} \f$ (can be changed at [Constants](@ref constants))
- Buffer content: &emsp;&emsp;&emsp;&emsp;&nbsp;\f$ \text{buffer[i]},\; i \in \{x : x \in \mathbb{N},\; 0 \leq x \leq N \} \f$
- Consumer positions: &emsp;&ensp;&nbsp; \f$ \text{tails[i]},\; i \in \{x : x \in \mathbb{N},\; 0 \leq x \leq C \} \f$ 
- Producer position: &emsp;&emsp;&ensp;&nbsp; head

The semantics of this data structure includes C logical queues [[4]](#4):
- Queues:&emsp;&emsp;&emsp;&ensp;&ensp;&ensp;&ensp;\f$ \text{Q[i]},\; i \in \{x : x \in \mathbb{N},\; 0 \leq x \leq C \} \f$ 
- Q[i] is empty, if:&emsp;&nbsp;&nbsp;&nbsp;\f$\text{head == tail[i]}\f$
- Q[i] is full, if:&emsp;&emsp;&ensp;&ensp;&nbsp;\f$\text{head+1 mod N == tail[i]}\f$
- Q[i] has data:&emsp;&emsp;&nbsp;&nbsp;&nbsp;\f$\text{[buffer[tails[i]], buffer[tails[i]+1 mod N],..., buffer[head-1 mod N]]}\f$

The following image illustrates the architecture of the circular buffer:

@image html ../res/CircularBuffer.png width=50%

@anchor synchronization
## Synchronization
Synchronization is done by utilizing atomics and conditional variables stored in shared memory combined with an algorithm called 'Adaptive Polling' [[3]](#3). With this approach, consumers can read circular buffer segments concurrently. Both, producer and consumers, are not allowed to overtake each other. Consumers themselves are allowed to. The synchronization is aimed to avoid busy waiting and is not wait-free.      
The following section only applies to older versions of **Shinda**:    
To be able to sustain ungracefull failures of clients, each producer and every consumer gets an own conditional variable to wait on. If one would use one conditional variable to wait for multiple processes at the same time and one of those processes exits ungracefully while waiting on that conditional, it would be left in an unconsistent state and every other process using it would also fail. This behaviour is due to the implementation of POSIX conditional variables. An alternative approach would also be to use SYSTEM V Semaphores, because SYSTEM V implementation is not subject to mentioned inconsistent states.

@anchor producer
### Producer
The producer will check on every [publish](@ref shinda.pubsub.ShindaClient.publish) call, if a consumer is in front of him. If that's the case, it will wait according to [Adaptive Polling](@ref adaptive_polling) or return immediatly with or without publishing data if [try_publish](@ref shinda.pubsub.ShindaClient.try_publish) is used.
If no one is in front of the producer, it is guaranteed that no process can get there in the future, because consumers can't overtake the producer. As of that, the producer will then write its data to its current position and then move on to the next segments which is guaranteed to be free of any other process [[4]](#4). Lastly, the producer checks, if any consumer is right behind him and/or waiting. If that's the case, the producer would wake up any waiting consumer, because new data is now available to read for the them.  
See the following pseudo code on how a producer synchronizes:

@image html ../res/EnqueuePseudo.png width=50%

@anchor consumer
### Consumer
The consumer will check on every [spin](@ref shinda.pubsub.ShindaClient.spin) call, if a producer is in front of him. If that's the case, it will wait according to [Adaptive Polling](@ref adaptive_polling) or return immediatly with or without reading data if [try_spin](@ref shinda.pubsub.ShindaClient.try_spin) is used.
If no one is in front of the consumer, it is guaranteed that no producer can get there in the future, because the producer can't overtake any consumer. As of that, the consumer will then move to the next segment which is guaranteed to be free and to contain new data and then read the data from this segment [[4]](#4). Lastly, the consumer checks, if the producer is right behind him and/or waiting. If that's the case ands the consumer is the last consumer in front of the producer, it will wake up the waiting producer because there is new space available for publishing a message.  
See the following pseudo code on how a consumer synchronizes:

@image html ../res/DequeuePseudo.png width=50%

@anchor adaptive_polling
### Adaptive Polling
<blockquote>
**⚠️ DEPRECATION WARNING** <br/>
In the current version 2.1.0, adaptive polling is not used. Instead, only sched_yield loops are in place for synchronization between publishers and subscribers. 
</blockquote>

 
This algorithm has the purpose of saving CPU resources while waiting on a given condition to be fullfilled [[3]](#3). In the current version of **Shinda**, only the sched_yield command is used to call the scheduler repeatadly for polling in a pseudo-non-busy manner for the [publish](@ref shinda.pubsub.ShindaClient.publish) and [spin](@ref shinda.pubsub.ShindaClient.spin) methods due to performance advantages compared to using synchronization via interprocess conditional variables.    
The following section only applies to older versions of **Shinda**:    
It combines classical busy polling with non-busy waiting and has three stages:
1. In the first stage, the algorithm checks the given condition a predefined number of times and executes NOP operations in-between to have a high probability that the condition has changed whilst doing this.
2. If the condition is still not fullfilled, the algorithm checks the given condition for another predefined number of times and calls the scheduler in-between. This allows other processes to run which increases the probability, that the condition has changed whilst doing this.
3. If the condition is still not met, the process is put to sleep until another process notifies that the condition has been fullfilled. Therefore the waiting in this stage has no impact on CPU load.

See the following UML activity diagram on how the algorithm operates:

@image html ../res/AdaptivePolling.png width=50%

<br/>

---

@anchor benchmarks
# Benchmarks
Results on throughput are highly dependent on the target platform. However, to give an impression, let's compare the throughput of Shinda to MQTT as well as to a bare minimum reference measurement consisting of pure memcpy commands to shared memory.
In this case, the measurement was done as follows:
- Read-Write(RW)/Write-only(W) \f$2^\text{27}\f$ Bytes to/from shared memory (MQTT(RW, QOS=1): 5, Shinda(RW) & Reference(W): 100) times in a row, measure the time for each transmission, calculate throughput and average
- Repeat this transmission for different message sizes: \f$\;\text{msg_size} \in \{2^x : x \in \mathbb{N},\; 5 \leq x \leq 25 \} \f$

In the following image, the green curve represents the reference for pure memcpy calls to shared memory without synchronization or reading back the data from shared memory. Notice, that this represents no transmission but only shows how fast data can actually be written to shared memory on this target system. The data clearly illustrates the impact of cache utilization in data transmission.  
The blue curve represents the described measurement for the **Shinda** library. Full transmissions were done and the throughput was measured for each message size. Again, the impact of the cache on throughput is not be underestimated. The drop in throughput in the section up to L1-Cache is explained by the overhead while transmitting many small messages instead of a few larger ones.  
The orange curve represents the described measurement for the mosquitto MQTT library. Because **Shinda** does not lose any messages, QOS=1 was selected for establishing a similiar behaviour. One can clearly see, that throughput for any message size is severly limited with MQTT's usage of TCP/IP compared to **Shinda** utilizing shared memory while granting the same quality of service.

@image html ../res/BandwidthComparison.png width=50%

<br/>

In the following image one can observe how the bandwidth changes with increasing number of subscribers on the same [Topic](@ref shinda.pubsub.Topic):

@image html ../res/SubscriberSweep.png width=50%

<br/>

---

@anchor references
# References
<a id="1">[1]</a> 
https://www.scc.com/insights/news/industry-news/what-is-edge-computing-and-why-does-it-matter/

<a id="2">[2]</a> 
Abranches et al. 
Shimmy: Shared Memory Channels for High Performance Inter-Container
Communication.
University of Colorado, Boulder. 

<a id="3">[3]</a> 
Fent et al. (2020).
Low-Latency Communication for Fast DBMS
Using RDMA and Shared Memory.
Technische Universität München.

<a id="4">[4]</a> 
Wong et al. (2009).
Oracle Streams: A High Performance Implementation for Near Real Time Asynchronous Replication
Oracle USA 500 Oracle Parkway, M/S 4op10, Redwood Shores, CA, U.S.A.