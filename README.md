# What is this all about?

**Shinda** is an easy to use library for transferring data via shared memory SPMC circular buffers between applications with highest reachable bandwidth and a minimum overhead publish/subscribe architecture.
<br/>
<br/>

# How do I use it?

For the documentation on how to install, use and understand **Shinda** please look here:\
<span style="font-size:larger;"><a href="https://varrick.gitlab.io/shinda/" target="_blank">**Documentation**</a></span>
<br/>
<br/>

# How can I collaborate?

Create a new issue if you encounter any inconveniences, have any ideas on how to improve **Shinda** or if you have any feature requests:\
<span style="font-size:larger;"><a href="https://gitlab.com/Varrick/shinda/-/issues" target="_blank">**Issues**</a></span>